package it.unito.lmf.android.ui.lists;

import it.unito.lmf.android.R;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class ShopsListAdapter extends BaseAdapter {

	class ShopListViewCache {

		private View baseView;
		private TextView textName;
		private TextView textDescription;
		private TextView textAddress;
		private TextView textOwner;
		private ImageView imagePicture;

		public ShopListViewCache(View baseView) {
			this.baseView = baseView;
		}

		public View getViewBase() {
			return baseView;
		}

		public TextView getTextName(int resource) {
			if (textName == null) {
				textName = (TextView) baseView.findViewById(R.id.name);
			}
			return textName;
		}

		public TextView getTextDescription(int resource) {
			if (textDescription == null) {
				textDescription = (TextView) baseView
						.findViewById(R.id.description);
			}
			return textDescription;
		}
		
		public TextView getTextAddress(int resource) {
			if (textAddress == null) {
				textAddress = (TextView) baseView.findViewById(R.id.address);
			}
			return textAddress;
		}
		
		public TextView getTextOwner(int resource) {
			if (textOwner == null) {
				textOwner = (TextView) baseView.findViewById(R.id.ownerName);
			}
			return textOwner;
		}

		public ImageView getImageView(int resource) {
			if (imagePicture == null) {
				imagePicture = (ImageView) baseView.findViewById(R.id.picture);
			}
			return imagePicture;
		}
	}

	private static final String TAG = "ShopListAdapter";

	private List<ShopRepresentation> results;
	private final LayoutInflater layoutInflater;
	private DisplayImageOptions options;

	public ShopsListAdapter(Context context, List<ShopRepresentation> results) {
		this.results = results;
		this.layoutInflater = LayoutInflater.from(context);
		this.options = new DisplayImageOptions.Builder().imageScaleType(
				ImageScaleType.EXACTLY).build();
	}

	public int getCount() {
		return this.results != null ? this.results.size() : 0;
	}

	public ShopRepresentation getItem(int position) {
		return this.results.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	private int resourceId = R.layout.shops_list_item;

	public View getView(int position, View convertView, ViewGroup parent) {
		ShopListViewCache viewCache;

		boolean init = true;

		if (convertView == null) {
			convertView = this.layoutInflater
					.inflate(resourceId, parent, false);
			viewCache = new ShopListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			init = false;
			convertView = (LinearLayout) convertView;
			viewCache = (ShopListViewCache) convertView.getTag();
		}

		ShopRepresentation result = getItem(position);
		if (result != null) {
			TextView t = viewCache.getTextName(resourceId);
			t.setText(result.getName());
			
			t = viewCache.getTextDescription(resourceId);
			t.setText(result.getDescription());
			
			viewCache.getTextAddress(resourceId).setText(result.getAddress());
			viewCache.getTextOwner(resourceId).setText(result.getOwner().getUsername() + " ("
					+ result.getOwner().getFirstName() + " "
					+ result.getOwner().getLastName() + ")");

			ImageView i = viewCache.getImageView(resourceId);

			if (init)
				ImageLoader.getInstance().displayImage(result.getImage(), i,
						options);
		}

		return convertView;
	}

}
