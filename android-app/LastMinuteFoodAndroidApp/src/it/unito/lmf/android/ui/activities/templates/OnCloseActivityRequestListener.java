package it.unito.lmf.android.ui.activities.templates;

public interface OnCloseActivityRequestListener {

	/**
	 * Called when a subcomponent of the activity wants to close the hosting
	 * activity (for example a fragment).
	 * 
	 * @param message an optional message describing the reason (for the user).
	 * @param statusCode an optional statusCode scribing the reason.
	 */
	public void onCloseActivityRequest(String message, long statusCode);
}
