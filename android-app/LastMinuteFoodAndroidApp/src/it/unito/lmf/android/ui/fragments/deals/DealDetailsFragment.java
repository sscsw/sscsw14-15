package it.unito.lmf.android.ui.fragments.deals;

import it.unito.lmf.android.R;
import it.unito.lmf.android.rest.LMFAPIClient;
import it.unito.lmf.android.ui.activities.templates.OnCloseActivityRequestListener;
import it.unito.lmf.android.ui.misc.ProgressDialogUtils;
import it.unito.lmf.android.utils.DataConverter;
import it.unito.lmf.android.utils.RestResponseBundle;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.values.PriceUnit;
import it.unito.lmf.domain.dsl.values.QuantityUnit;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

public class DealDetailsFragment extends Fragment {

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_DEAL_ID = "it.unito.lmf.dealId";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private DealRepresentation deal;

	private OnCloseActivityRequestListener closeActivityListener;

	TextView tvTitle;
	TextView tvDescription;
	TextView tvPrice;
	TextView tvDiscountPrc;
	TextView tvStartingPrice;
	TextView tvPublishedDate;
	TextView tvExpires;
	TextView tvQuantity;
	TextView tvDiscountReason;
	ImageView ivPicture;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DealDetailsFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_DEAL_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			new GetDealTask().execute(buildGetDealsParams(getArguments()
					.getLong(ARG_DEAL_ID)));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			closeActivityListener = (OnCloseActivityRequestListener) activity;
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"The hosting activit must implement OnCloseActivityRequestListener interface.");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.deal_details_fragment, container,
				false);

		tvTitle = (TextView) view.findViewById(R.id.title);
		tvDescription = (TextView) view.findViewById(R.id.description);
		tvPrice = (TextView) view.findViewById(R.id.price);
		tvDiscountPrc = (TextView) view.findViewById(R.id.discountPercentage);
		tvStartingPrice = (TextView) view.findViewById(R.id.startingPrice);
		tvPublishedDate = (TextView) view.findViewById(R.id.publishedDate);
		tvExpires = (TextView) view.findViewById(R.id.expireDate);
		tvQuantity = (TextView) view.findViewById(R.id.quantity);
		tvDiscountReason = (TextView) view.findViewById(R.id.discountReason);
		ivPicture = (ImageView) view.findViewById(R.id.picture);
		((Button) view.findViewById(R.id.showOnMapButton))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						showDealOnMap(deal);
					}
				});

		return view;
	}

	public void showDealOnMap(DealRepresentation deal) {
		String baseUri = "geo:";

		String location="";
		if (deal.getShop().getLatitude() != null)
			location += deal.getShop().getLatitude() + ","
					+ deal.getShop().getLongitude();
		else
			location += deal.getShop().getAddress();
		
		baseUri+=location;
		baseUri += "?q="+location+"(" + deal.getShop().getName() + ")";

		baseUri += "&z=11";
		
		Uri uri = Uri.parse(baseUri).buildUpon().build();

		showMap(uri);
	}

	public void showMap(Uri geoLocation) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(geoLocation);
		if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
			startActivity(intent);
		}
	}

	protected void refreshResults(DealRepresentation deal) {
		if (deal == null) {

			return;
		}

		this.deal = deal;

		String priceUnit = "/" + deal.getPriceUnit();
		if (deal.getPriceUnit().equals(PriceUnit.PIECE.toString()))
			priceUnit = "";

		String fullPriceUnit = " " + deal.getCurrencySymbol() + priceUnit;

		tvTitle.setText(deal.getName());
		tvDescription.setText(deal.getDescription());
		tvPrice.setText(deal.getPrice() + fullPriceUnit);
		tvDiscountPrc.setText("-" + deal.getDiscountPercentage().intValue()
				+ "%");
		tvStartingPrice.setText(deal.getStartingPrice() + fullPriceUnit);
		tvPublishedDate.setText(DataConverter.getElapsedTime(
				deal.getCreatedAt(), getActivity()));
		tvExpires.setText((deal.getExpireDate() != null ? deal.getExpireDate()
				.toLocaleString() : "Never"));

		String quantityUnit = " " + deal.getQuantityUnit();
		if (deal.getQuantityUnit().equals(QuantityUnit.PIECE.toString()))
			quantityUnit = "";
		tvQuantity.setText(deal.getQuantity() + quantityUnit);
		tvDiscountReason.setText(deal.getDiscountReason());
		ImageLoader.getInstance().displayImage(deal.getImage(), ivPicture);
	}

	// ***************************************
	// Private classes
	// ***************************************
	private class GetDealTask
			extends
			AsyncTask<Map<String, Object>, Void, RestResponseBundle<DealRepresentation, DealRepresentation>> {

		public static final String TAG = "GetDealTask";
		private Map<String, Object> params;

		@Override
		protected void onPreExecute() {
			ProgressDialogUtils.get(getActivity()).showLoadingProgressDialog();

		}

		@Override
		protected RestResponseBundle<DealRepresentation, DealRepresentation> doInBackground(
				Map<String, Object>... params) {
			try {
				this.params = params[0];

				return LMFAPIClient.getInstance().getDeal(
						(Long) this.params.get("DealId"));
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(
				RestResponseBundle<DealRepresentation, DealRepresentation> responseBundle) {
			ProgressDialogUtils.get(getActivity()).dismissProgressDialog();
			if (responseBundle != null)
				refreshResults(responseBundle.getResult());
			else {
				Toast.makeText(getActivity().getApplicationContext(),
						"Impossibile caricare l'offerta.", Toast.LENGTH_LONG)
						.show();

				closeActivityListener.onCloseActivityRequest(
						"Impossibile caricare l'offerta", -1);
			}
		}

	}

	public static Map<String, Object> buildGetDealsParams(long dealId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("DealId", dealId);
		return params;
	}
}
