package it.unito.lmf.android.ui.misc;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;

/**
 * Default {@link TabListener} that handles attach/detach actions of a
 * {@link Fragment} within the given layout id inside the activity.
 * 
 * @author Biava Lorenzo
 *
 * @param <T>
 */
@SuppressWarnings("deprecation")
public class DefaultTabListener<T extends Fragment> implements
		ActionBar.TabListener {
	private Fragment fragment;
	private final Activity activity;
	private final String tag;
	private final Class<T> fragmentClass;
	private final int layoutId;

	/**
	 * Constructor used each time a new tab is created. <br/>
	 * This uses the default {@code android.R.id.content} layoutId.
	 * 
	 * @param activity
	 *            The host Activity, used to instantiate the fragment
	 * @param tag
	 *            The identifier tag for the fragment
	 * @param clz
	 *            The fragment's Class, used to instantiate the fragment
	 */
	public DefaultTabListener(Activity activity, String tag, Class<T> clz) {
		this(activity, tag, clz, android.R.id.content);
	}
	
	public DefaultTabListener(Activity activity, String tag, Class<T> clz, Fragment fragment) {
		this(activity, tag, clz, android.R.id.content, fragment);
	}

	/**
	 * Constructor used each time a new tab is created.
	 * 
	 * @param activity
	 * @param tag
	 * @param clz
	 * @param layoutId
	 *            The id of the layout object to put the fragment into.
	 */
	public DefaultTabListener(Activity activity, String tag, Class<T> clz,
			int layoutId) {
		this.activity = activity;
		this.tag = tag;
		this.fragmentClass = clz;
		this.layoutId = layoutId;
	}
	
	public DefaultTabListener(Activity activity, String tag, Class<T> clz,
			int layoutId, Fragment fragment) {
		this.activity = activity;
		this.tag = tag;
		this.fragmentClass = clz;
		this.layoutId = layoutId;
		this.fragment=fragment;
	}

	/* The following are each of the ActionBar.TabListener callbacks */

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// Check if the fragment is already initialized
		if (fragment == null) {
			// If not, instantiate and add it to the activity
			fragment = Fragment.instantiate(activity, fragmentClass.getName());
			ft.add(layoutId, fragment, tag);
		} else {
			// If it exists, simply attach it in order to show it
			ft.attach(fragment);
		}
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		if (fragment != null) {
			// Detach the fragment, because another one is being attached
			ft.detach(fragment);
		}
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// User selected the already selected tab. Usually do nothing.
	}
}
