package it.unito.lmf.android.ui.lists;

import android.database.DataSetObserver;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListAdapter;

public class EndlessScrollListener implements OnScrollListener {

	public interface EndlessScrollListenerActions {

		/**
		 * Put here the code for the update of the list.
		 * 
		 * @param view
		 * @param firstVisibleItem
		 * @param visibleItemCount
		 * @param totalItemCount
		 * @param currentPage
		 *            zero based.
		 * @param pageSize
		 */
		public void onGetNewItems(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount, int currentPage,
				int pageSize);

		// /**
		// * Put here the code for the refresh of the whole list.
		// *
		// * @param view
		// * @param firstVisibleItem
		// * @param visibleItemCount
		// * @param totalItemCount
		// * @param currentPage
		// * zero based.
		// * @param pageSize
		// */
		// public void onRefreshItems(AbsListView view, int currentPage,
		// int pageSize);
	}

	public enum ScrollingDirection {
		UP, DOWN, NONE
	}

	public static final String TAG = "EndlessScrollListener";
	private static final int DEFAULT_VISIBLE_THRESHOLD = 2;

	private int visibleThreshold = 2;
	private int currentPage = 0;
	private int previousTotal = 0;
	private boolean loading = true;
	private int pageSize = 10;
	private int lastFirstVisibleItem = 0;
	// private ScrollingDirection scrollingDirection = ScrollingDirection.NONE;
	private EndlessScrollListenerActions actions;
	private ListAdapter listAdapter;

	public EndlessScrollListener(ListAdapter listAdapter,
			EndlessScrollListenerActions actions) {
		this(listAdapter, actions, DEFAULT_VISIBLE_THRESHOLD);
	}

	public EndlessScrollListener(ListAdapter listAdapter,
			EndlessScrollListenerActions actions, int visibleThreshold) {
		this.actions = actions;
		this.visibleThreshold = visibleThreshold;
		this.listAdapter = listAdapter;
		//this.listAdapter.registerDataSetObserver(new MyDataSetObserver());
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	private class MyDataSetObserver extends DataSetObserver {

		private int previousCount = 0;

		@Override
		public void onChanged() {
			if (listAdapter.getCount() < previousCount)
				currentPage = 0;
			previousCount = listAdapter.getCount();
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// if (lastFirstVisibleItem < firstVisibleItem) {
		// scrollingDirection = ScrollingDirection.DOWN;
		// }
		// if (lastFirstVisibleItem > firstVisibleItem) {
		// scrollingDirection = ScrollingDirection.UP;
		// }
		// lastFirstVisibleItem = firstVisibleItem;

		// List reset
		if (totalItemCount != previousTotal) {
			loading=true;
		}
		
		if (loading) {
			if (totalItemCount != previousTotal) {
				loading = false;

				// List reset
				if (totalItemCount < previousTotal) {
					currentPage = 1;
					Log.d(TAG, "List reset");
				}
				else
					currentPage++;
				previousTotal = totalItemCount;

			}
		}
		if (!loading
				&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {

			// Check whether the last loaded page was full, otherwise
			// there's noting more to load
			// TODO: Improve with refresh of the whole list
			if (totalItemCount >= pageSize * currentPage) {

				Log.d(TAG, "Loading next page, number " + currentPage
						+ " size " + pageSize);

				// Load the next items page
				actions.onGetNewItems(view, firstVisibleItem, visibleItemCount,
						totalItemCount, currentPage, pageSize);

				loading = true;
			}
//			else
//				Log.d(TAG, "No more items to load.");
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// if (scrollState == SCROLL_STATE_FLING
		// && view.getFirstVisiblePosition() == 0
		// && scrollingDirection == ScrollingDirection.UP) {
		// Log.d(TAG, "Refreshing list");
		//
		// actions.onRefreshItems(view, scrollState, scrollState);
		// }
	}

}
