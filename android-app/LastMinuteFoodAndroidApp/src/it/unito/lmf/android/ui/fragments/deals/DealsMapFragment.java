/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unito.lmf.android.ui.fragments.deals;

import it.unito.lmf.android.R;
import it.unito.lmf.android.rest.LMFAPIClient;
import it.unito.lmf.android.ui.activities.deal.DealDetailsActivity;
import it.unito.lmf.android.ui.activities.experimental.swipefragmentlist.SwipeRefreshListFragment;
import it.unito.lmf.android.ui.lists.EndlessScrollListener.EndlessScrollListenerActions;
import it.unito.lmf.android.ui.misc.ProgressDialogUtils;
import it.unito.lmf.android.ui.misc.map.cluster.DealClusterItem;
import it.unito.lmf.android.utils.RestResponseBundle;
import it.unito.lmf.android.utils.pagination.Page;
import it.unito.lmf.android.utils.pagination.PageRequest;
import it.unito.lmf.android.utils.pagination.Pageable;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * A sample which shows how to use
 * {@link android.support.v4.widget.SwipeRefreshLayout} within a
 * {@link android.support.v4.app.ListFragment} to add the 'swipe-to-refresh'
 * gesture to a {@link android.widget.ListView}. This is provided through the
 * provided re-usable {@link SwipeRefreshListFragment} class.
 *
 * <p>
 * To provide an accessible way to trigger the refresh, this app also provides a
 * refresh action item. This item should be displayed in the Action Bar's
 * overflow item.
 *
 * <p>
 * In this sample app, the refresh updates the ListView with a random set of new
 * items.
 *
 * <p>
 * This sample also provides the functionality to change the colors displayed in
 * the {@link android.support.v4.widget.SwipeRefreshLayout} through the options
 * menu. This is meant to showcase the use of color rather than being something
 * that should be integrated into apps.
 */
public class DealsMapFragment extends SupportMapFragment implements
		ClusterManager.OnClusterClickListener<DealClusterItem>,
		ClusterManager.OnClusterInfoWindowClickListener<DealClusterItem>,
		ClusterManager.OnClusterItemClickListener<DealClusterItem>,
		ClusterManager.OnClusterItemInfoWindowClickListener<DealClusterItem> {

	private static final String LOG_TAG = DealsMapFragment.class
			.getSimpleName();

	private int pageSize = 10;
	private List<DealRepresentation> deals = new ArrayList<DealRepresentation>();
	// Declare a variable for the cluster manager.
	ClusterManager<DealClusterItem> clusterManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Notify the system to allow an options menu for this fragment.
		// setHasOptionsMenu(true);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		getMap().setMyLocationEnabled(true);
		getMap().moveCamera(CameraUpdateFactory.zoomTo(8));
		getMap().getUiSettings().setZoomControlsEnabled(true);

		setUpClusterer();
		
		getMap().setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker marker) {

//			    // Getting view from the layout file
//			    View v = getActivity().getLayoutInflater().inflate(R.layout.map_popup, null);
//
//			    TextView title = (TextView) v.findViewById(R.id.title);
//			    title.setText(marker.getTitle());
//
//			    TextView address = (TextView) v.findViewById(R.id.distance);
//			    address.setText(marker.getSnippet());
//
//			    return v;
				return null;
			}

			@Override
			public View getInfoContents(Marker arg0) {
			    // TODO Auto-generated method stub
			    return null;
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		new DealsGetTask().execute(buildGetDealsParams(new PageRequest(0,
				pageSize), true));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.deals_list_fragment_menu, menu);
	}

	/**
	 * Respond to the user's selection of the Refresh action item. Start the
	 * SwipeRefreshLayout progress bar, then initiate the background task that
	 * refreshes the content.
	 *
	 * <p>
	 * A color scheme menu item used for demonstrating the use of
	 * SwipeRefreshLayout's color scheme functionality. This kind of menu item
	 * should not be incorporated into your app, it just to demonstrate the use
	 * of color. Instead you should choose a color scheme based off of your
	 * application's branding.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			Log.i(LOG_TAG, "Refresh menu item selected");

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void refreshResults(List<DealRepresentation> response,
			boolean refreshWholeList) {
		if (response == null) {
			return;
		}

		if (refreshWholeList) {
			deals.clear();
			// getMap().clear();
			clusterManager.clearItems();
		}
		deals.addAll(response);

		for (DealRepresentation deal : response) {
			try {
				// getMap().addMarker(
				// new MarkerOptions().title(deal.getName()).position(
				// new LatLng(deal.getShop().getLatitude(), deal
				// .getShop().getLongitude())));
				clusterManager.addItem(new DealClusterItem(deal));
			} catch (Exception ex) {
				Log.d(LOG_TAG,
						"Cannot add deal " + deal.getId() + ": "
								+ ex.getMessage());
			}
		}

		clusterManager.cluster();
	}

	// ***************************************
	// Private classes
	// ***************************************
	private class DealsGetTask
			extends
			AsyncTask<Map<String, Object>, Void, RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]>> {

		public static final String TAG = "DealsGetTask";
		private Map<String, Object> params;

		@Override
		protected void onPreExecute() {
			ProgressDialogUtils.get(getActivity()).showLoadingProgressDialog();
		}

		@Override
		protected RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]> doInBackground(
				Map<String, Object>... params) {
			try {
				this.params = params[0];

				return LMFAPIClient.getInstance().getDeals(
						(Pageable) this.params.get("Pageable"));
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(
				RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]> responseBundle) {
			ProgressDialogUtils.get(getActivity()).dismissProgressDialog();
			if (responseBundle != null)
				refreshResults(responseBundle.getResult().getContent(),
						(Boolean) params.get("Refresh"));
			else
				Toast.makeText(getActivity().getApplicationContext(),
						"Impossibile caricare le offerte.", Toast.LENGTH_LONG)
						.show();
		}

	}

	public static Map<String, Object> buildGetDealsParams(Pageable pageable,
			boolean refresh) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("Refresh", refresh);
		params.put("Pageable", pageable);
		return params;
	}

	public class DealsActions implements EndlessScrollListenerActions {

		@SuppressWarnings("unchecked")
		@Override
		public void onGetNewItems(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount, int currentPage,
				int pageSize) {
			// Load the next deals page
			new DealsGetTask().execute(buildGetDealsParams(new PageRequest(
					currentPage, pageSize), false));
		}
	}

	/*
	 * MAPS CLUSTER
	 */
	private void setUpClusterer() {

		// Initialize the manager with the context and the map.
		// (Activity extends context, so we can pass 'this' in the constructor.)
		clusterManager = new ClusterManager<DealClusterItem>(getActivity(),
				getMap());

		// Point the map's listeners at the listeners implemented by the cluster
		// manager.

		clusterManager.setRenderer(new PersonRenderer());
		getMap().setOnCameraChangeListener(clusterManager);
		getMap().setOnMarkerClickListener(clusterManager);
		getMap().setOnInfoWindowClickListener(clusterManager);
		getMap().setInfoWindowAdapter(clusterManager.getMarkerManager());
		clusterManager.setOnClusterClickListener(this);
		clusterManager.setOnClusterInfoWindowClickListener(this);
		clusterManager.setOnClusterItemClickListener(this);
		clusterManager.setOnClusterItemInfoWindowClickListener(this);

	}

	/**
	 * Draws profile photos inside markers (using IconGenerator). When there are
	 * multiple people in the cluster, draw multiple photos (using
	 * MultiDrawable).
	 */
	private class PersonRenderer extends
			DefaultClusterRenderer<DealClusterItem> {
		private final IconGenerator mIconGenerator = new IconGenerator(
				getActivity().getApplicationContext());
		private final IconGenerator mClusterIconGenerator = new IconGenerator(
				getActivity().getApplicationContext());
		private final ImageView mImageView;
		private final ImageView mClusterImageView;
		private final int mDimension;

		public PersonRenderer() {
			super(getActivity().getApplicationContext(), getMap(),
					clusterManager);

			View multiProfile = getActivity().getLayoutInflater().inflate(
					R.layout.multi_profile, null);
			mClusterIconGenerator.setContentView(multiProfile);
			mClusterImageView = (ImageView) multiProfile
					.findViewById(R.id.image);

			mImageView = new ImageView(getActivity().getApplicationContext());
			mDimension = (int) getResources().getDimension(
					R.dimen.custom_profile_image);
			mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension,
					mDimension));
			int padding = (int) getResources().getDimension(
					R.dimen.custom_profile_padding);
			mImageView.setPadding(padding, padding, padding, padding);
			mIconGenerator.setContentView(mImageView);
		}

		@Override
		protected void onBeforeClusterItemRendered(DealClusterItem deal,
				MarkerOptions markerOptions) {
			// Draw a single person.
			// Set the info window to show their name.
			 mImageView.setImageResource(
			 R.drawable.spinner_black_48);
			// mImageView.setImageBitmap(ImageLoader.getInstance().loadImageSync(
			// deal.item().getImage()));

			Bitmap icon = mIconGenerator.makeIcon();
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(
					deal.item().getName());
		}

		@Override
		protected void onBeforeClusterRendered(
				Cluster<DealClusterItem> cluster, MarkerOptions markerOptions) {
			// Draw multiple people.
			// Note: this method runs on the UI thread. Don't spend too much
			// time in here (like in this example).
			List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4,
					cluster.getSize()));
			int width = mDimension;
			int height = mDimension;

			for (DealClusterItem p : cluster.getItems()) {
				// Draw 4 at most.
				if (profilePhotos.size() == 4)
					break;
				Drawable drawable = getResources().getDrawable(
						R.drawable.spinner_black_48);
				drawable.setBounds(0, 0, width, height);
				profilePhotos.add(drawable);
			}
			MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
			multiDrawable.setBounds(0, 0, width, height);

			mClusterImageView.setImageDrawable(multiDrawable);
			Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster
					.getSize()));
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
		}

		@Override
		protected boolean shouldRenderAsCluster(Cluster<DealClusterItem> cluster) {
			// Always render clusters.
			return cluster.getSize() > 1;
		}
	}

	@Override
	public boolean onClusterClick(Cluster<DealClusterItem> cluster) {
		// Show a toast with some info when the cluster is clicked.
		String firstName = cluster.getItems().iterator().next().item()
				.getName();
		Toast.makeText(getActivity(),
				cluster.getSize() + " (including " + firstName + ")",
				Toast.LENGTH_SHORT).show();
		return true;
	}

	@Override
	public void onClusterInfoWindowClick(Cluster<DealClusterItem> cluster) {
		// Does nothing, but you could go to a list of the users.
	}

	@Override
	public boolean onClusterItemClick(DealClusterItem item) {
		return false;
	}

	@Override
	public void onClusterItemInfoWindowClick(DealClusterItem item) {
		DealDetailsActivity.launch(getActivity(), item.item().getId(), item.item().getShop().getId());
	}

}
