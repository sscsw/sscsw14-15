package it.unito.lmf.android.ui.misc.map.cluster;

import com.google.android.gms.maps.model.LatLng;

import it.unito.lmf.domain.dsl.deal.DealRepresentation;

public class DealClusterItem extends GenericClusterItem<DealRepresentation> {

	public DealClusterItem(DealRepresentation item) {
		super(item);
	}

	@Override
	public LatLng getPosition() {
		return new LatLng(this.item().getShop().getLatitude(), this.item().getShop().getLongitude());
	}
	
}
