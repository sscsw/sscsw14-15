package it.unito.lmf.android.ui.fragments.shops;

import it.unito.lmf.android.R;
import it.unito.lmf.android.rest.LMFAPIClient;
import it.unito.lmf.android.ui.activities.experimental.swipefragmentlist.SwipeRefreshListFragment;
import it.unito.lmf.android.ui.activities.shop.ShopDetailsActivity;
import it.unito.lmf.android.ui.lists.EndlessScrollListener;
import it.unito.lmf.android.ui.lists.EndlessScrollListener.EndlessScrollListenerActions;
import it.unito.lmf.android.ui.lists.ShopsListAdapter;
import it.unito.lmf.android.utils.RestResponseBundle;
import it.unito.lmf.android.utils.pagination.Page;
import it.unito.lmf.android.utils.pagination.PageRequest;
import it.unito.lmf.android.utils.pagination.Pageable;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * A sample which shows how to use
 * {@link android.support.v4.widget.SwipeRefreshLayout} within a
 * {@link android.support.v4.app.ListFragment} to add the 'swipe-to-refresh'
 * gesture to a {@link android.widget.ListView}. This is provided through the
 * provided re-usable {@link SwipeRefreshListFragment} class.
 *
 * <p>
 * To provide an accessible way to trigger the refresh, this app also provides a
 * refresh action item. This item should be displayed in the Action Bar's
 * overflow item.
 *
 * <p>
 * In this sample app, the refresh updates the ListView with a random set of new
 * items.
 *
 * <p>
 * This sample also provides the functionality to change the colors displayed in
 * the {@link android.support.v4.widget.SwipeRefreshLayout} through the options
 * menu. This is meant to showcase the use of color rather than being something
 * that should be integrated into apps.
 */
public class ShopsListFragment extends SwipeRefreshListFragment implements
		OnItemClickListener {

	private static final String LOG_TAG = ShopsListFragment.class
			.getSimpleName();

	private int pageSize = 10;
	private ShopsListAdapter listAdapter;
	private ArrayList<ShopRepresentation> itemList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Notify the system to allow an options menu for this fragment.
		setHasOptionsMenu(true);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		/**
		 * Create an ArrayAdapter to contain the data for the ListView. Each
		 * item in the ListView uses the system-defined simple_list_item_1
		 * layout that contains one TextView.
		 */
		itemList = new ArrayList<ShopRepresentation>();
		listAdapter = new ShopsListAdapter(getActivity(), itemList);

		// Set the adapter between the ListView and its backing data.
		setListAdapter(listAdapter);

		// Set the EndlessScrollListener to auto-load overflowing items
		ListView lw = getListView();
		EndlessScrollListener esl = new EndlessScrollListener(listAdapter,
				new ShopsActions());
		esl.setPageSize(pageSize);
		lw.setOnScrollListener(esl);
		lw.setOnItemClickListener(this);

		/**
		 * Implement {@link SwipeRefreshLayout.OnRefreshListener}. When users do
		 * the "swipe to refresh" gesture, SwipeRefreshLayout invokes
		 * {@link SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}.
		 * In {@link SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}
		 * , call a method that refreshes the content. Call the same method in
		 * response to the Refresh action from the action bar.
		 */
		setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

				initiateRefresh();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		// when this activity starts, load the shops async
		initiateRefresh();

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ShopRepresentation shop = listAdapter.getItem(position);		
		ShopDetailsActivity.launch(getActivity(), shop.getId());
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.shops_list_fragment_menu, menu);
	}

	/**
	 * Respond to the user's selection of the Refresh action item. Start the
	 * SwipeRefreshLayout progress bar, then initiate the background task that
	 * refreshes the content.
	 *
	 * <p>
	 * A color scheme menu item used for demonstrating the use of
	 * SwipeRefreshLayout's color scheme functionality. This kind of menu item
	 * should not be incorporated into your app, it just to demonstrate the use
	 * of color. Instead you should choose a color scheme based off of your
	 * application's branding.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			Log.i(LOG_TAG, "Refresh menu item selected");

			// We make sure that the SwipeRefreshLayout is displaying it's
			// refreshing indicator
			if (!isRefreshing()) {
				setRefreshing(true);
			}

			// Start our refresh background task
			initiateRefresh();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * By abstracting the refresh process to a single method, the app allows
	 * both the SwipeGestureLayout onRefresh() method and the Refresh action
	 * item to refresh the content.
	 */
	@SuppressWarnings("unchecked")
	private void initiateRefresh() {
		Log.i(LOG_TAG, "initiateRefresh");

		/**
		 * Execute the background task, which uses {@link android.os.AsyncTask}
		 * to load the data.
		 */
		new ShopsGetTask().execute(buildGetShopsParams(new PageRequest(0,
				pageSize), true));
	}

	// ***************************************
	// Private methods
	// ***************************************
	private void refreshResults(List<ShopRepresentation> response,
			boolean refreshWholeList) {
		if (response == null) {
			return;
		}

		if (refreshWholeList) {
			itemList.clear();
		}
		itemList.addAll(response);
		listAdapter.notifyDataSetChanged();

		if (refreshWholeList)
			// Stop the refreshing indicator
			setRefreshing(false);
	}

	// ***************************************
	// Private classes
	// ***************************************
	private class ShopsGetTask
			extends
			AsyncTask<Map<String, Object>, Void, RestResponseBundle<Page<ShopRepresentation>, ShopRepresentation[]>> {

		public static final String TAG = "ShopsGetTask";
		private Map<String, Object> params;

		@Override
		protected void onPreExecute() {
			// showLoadingProgressDialog();
			setRefreshing(true);
		}

		@Override
		protected RestResponseBundle<Page<ShopRepresentation>, ShopRepresentation[]> doInBackground(
				Map<String, Object>... params) {
			try {
				this.params = params[0];

				return LMFAPIClient.getInstance().getShops(
						(Pageable) this.params.get("Pageable"));
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(
				RestResponseBundle<Page<ShopRepresentation>, ShopRepresentation[]> responseBundle) {
			dismissProgressDialog();
			if (responseBundle != null)
				refreshResults(responseBundle.getResult().getContent(),
						(Boolean) params.get("Refresh"));
			else
				Toast.makeText(getActivity().getApplicationContext(),
						"Impossibile caricare i negozi.", Toast.LENGTH_LONG)
						.show();

			setRefreshing(false);
		}

	}

	public static Map<String, Object> buildGetShopsParams(Pageable pageable,
			boolean refresh) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("Refresh", refresh);
		params.put("Pageable", pageable);
		return params;
	}

	public class ShopsActions implements EndlessScrollListenerActions {

		@SuppressWarnings("unchecked")
		@Override
		public void onGetNewItems(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount, int currentPage,
				int pageSize) {
			// Load the next shops page
			new ShopsGetTask().execute(buildGetShopsParams(new PageRequest(
					currentPage, pageSize), false));
		}
	}

}
