package it.unito.lmf.android.ui.misc.map.cluster;

import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import com.google.android.gms.maps.model.LatLng;

public class ShopClusterItem extends GenericClusterItem<ShopRepresentation> {

	public ShopClusterItem(ShopRepresentation item) {
		super(item);
	}

	@Override
	public LatLng getPosition() {
		return new LatLng(this.item().getLatitude(), this.item().getLongitude());
	}
	
}
