/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unito.lmf.android.ui.activities.shop;

import it.unito.lmf.android.R;
import it.unito.lmf.android.ui.activities.templates.OnCloseActivityRequestListener;
import it.unito.lmf.android.ui.fragments.shops.ShopDetailsFragment;
import it.unito.lmf.android.utils.ActivityUtils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * A simple launcher activity containing a summary sample description, sample
 * log and a custom {@link android.support.v4.app.Fragment} which can display a
 * view.
 * <p>
 * For devices with displays with a width of 720dp or greater, the sample log is
 * always visible, on other devices it's visibility is controlled by an item on
 * the Action Bar.
 */
public class ShopDetailsActivity extends ActionBarActivity implements OnCloseActivityRequestListener {

	public static final String TAG = "ShopsDetailsActivity";

	/**
	 * Launch this activity to display the given shop.
	 * 
	 * @param ctx
	 * @param shopId
	 */
	public static <P extends Activity> void launch(Context ctx, Class<P> parent, long shopId) {
		Bundle extras=new Bundle();
		extras.putLong(ShopDetailsFragment.ARG_SHOP_ID,  shopId);		
		ActivityUtils.launchWithParent(ctx, parent, ShopDetailsActivity.class, extras);
	}
	
	public static <P extends Activity> void launch(P parentAndContext, long shopId) {
		launch(parentAndContext, parentAndContext.getClass(), shopId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Notice that setContentView() is not used, because we use the root
		// android.R.id.content as the container for each fragment

		// setup action bar for tabs
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);

		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putLong(ShopDetailsFragment.ARG_SHOP_ID, getIntent()
					.getLongExtra(ShopDetailsFragment.ARG_SHOP_ID,0));
			ShopDetailsFragment fragment = new ShopDetailsFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, fragment).commit();
		}
	}

	@Override
	public Intent getSupportParentActivityIntent() {
		try {
			String className = getIntent().getStringExtra(Intent.EXTRA_INTENT);
			return new Intent(this, Class.forName(className));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return super.getSupportParentActivityIntent();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.shops_activity_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
		// logToggle.setVisible(findViewById(R.id.sample_output) instanceof
		// ViewAnimator);
		// logToggle.setTitle(mLogShown ? R.string.sample_hide_log :
		// R.string.sample_show_log);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case R.id.menu_toggle_log:
		// mLogShown = !mLogShown;
		// ViewAnimator output = (ViewAnimator)
		// findViewById(R.id.sample_output);
		// if (mLogShown) {
		// output.setDisplayedChild(1);
		// } else {
		// output.setDisplayedChild(0);
		// }
		// supportInvalidateOptionsMenu();
		// return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCloseActivityRequest(String message, long statusCode) {
		finish();
	}

}
