package it.unito.lmf.android.ui.activities;

import it.unito.lmf.android.R;
import it.unito.lmf.android.ui.activities.deal.DealsActivity;
import it.unito.lmf.android.ui.activities.login.UserSessionManager;
import it.unito.lmf.android.ui.activities.shop.ShopsActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	private static final String TAG = "MainActivity";

	// TODO: Save to bundle ?
	UserSessionManager session;

	Button btShowDeals;
	Button btShowShops;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Debug.waitForDebugger();

		setContentView(R.layout.activity_main);

		// Intent i = new Intent(this, LoginActivity.class);
		// startActivityForResult(i, 1);

		// Session class instance
		session = UserSessionManager.getInstance();

		session.checkLogin();

		btShowShops = (Button) findViewById(R.id.shopsListButton);
		btShowDeals = (Button) findViewById(R.id.dealsListButton);

		btShowShops.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(),
						ShopsActivity.class));
			}
		});

		btShowDeals.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(),
						DealsActivity.class));
			}
		});
	}

	long lastBackPressed = 0;

	@Override
	public void onBackPressed() {

		if (System.currentTimeMillis() - lastBackPressed < 1000) {
			super.onBackPressed();
		} else {
			lastBackPressed = System.currentTimeMillis();
			Toast.makeText(getApplicationContext(), "Press again to quit",
					Toast.LENGTH_SHORT).show();
		}
	}

}
