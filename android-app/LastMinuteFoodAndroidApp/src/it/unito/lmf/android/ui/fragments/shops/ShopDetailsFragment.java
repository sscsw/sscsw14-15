package it.unito.lmf.android.ui.fragments.shops;

import it.unito.lmf.android.R;
import it.unito.lmf.android.rest.LMFAPIClient;
import it.unito.lmf.android.ui.activities.templates.OnCloseActivityRequestListener;
import it.unito.lmf.android.ui.misc.ProgressDialogUtils;
import it.unito.lmf.android.utils.RestResponseBundle;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ShopDetailsFragment extends Fragment {

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_SHOP_ID = "it.unito.lmf.shopId";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private ShopRepresentation shop;

	private OnCloseActivityRequestListener closeActivityListener;

	TextView tvName;
	TextView tvDescription;
	TextView tvPrice;
	TextView categories;
	TextView address;
	TextView partitaIVA;
	TextView phoneNumber;
	TextView owner;
	ImageView ivPicture;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ShopDetailsFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_SHOP_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			new GetShopTask().execute(buildGetShopsParams(getArguments()
					.getLong(ARG_SHOP_ID)));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			closeActivityListener = (OnCloseActivityRequestListener) activity;
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"The hosting activit must implement OnCloseActivityRequestListener interface.");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.shop_details_fragment, container,
				false);

		tvName = (TextView) view.findViewById(R.id.name);
		tvDescription = (TextView) view.findViewById(R.id.description);
		categories = (TextView) view.findViewById(R.id.categories);
		address = (TextView) view.findViewById(R.id.address);
		phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
		owner = (TextView) view.findViewById(R.id.ownerName);
		ivPicture = (ImageView) view.findViewById(R.id.picture);
		((Button) view.findViewById(R.id.showOnMapButton))
		.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showShopOnMap(shop);
			}
		});
		return view;
	}

	public void showShopOnMap(ShopRepresentation shop) {
		String baseUri = "geo:";

		String location="";
		if (shop.getLatitude() != null)
			location += shop.getLatitude() + ","
					+ shop.getLongitude();
		else
			location += shop.getAddress();
		baseUri+=location;
		baseUri += "?q="+location+"(" + shop.getName() + ")";

		baseUri += "&z=11";
		Uri uri = Uri.parse(baseUri).buildUpon().build();

		showMap(uri);
	}

	public void showMap(Uri geoLocation) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(geoLocation);
		if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
			startActivity(intent);
		}
	}
	
	protected void refreshResults(ShopRepresentation shop) {
		if (shop == null) {

			return;
		}

		this.shop = shop;

		tvName.setText(shop.getName());
		tvDescription.setText(shop.getDescription());

		categories.setText(shop.getCategories().toString());
		address.setText(shop.getAddress());
		phoneNumber.setText(shop.getPhoneNumber());
		owner.setText(shop.getOwner().getUsername() + " ("
				+ shop.getOwner().getFirstName() + " "
				+ shop.getOwner().getLastName() + ")");
		ImageLoader.getInstance().displayImage(shop.getImage(), ivPicture);
	}

	// ***************************************
	// Private classes
	// ***************************************
	private class GetShopTask
			extends
			AsyncTask<Map<String, Object>, Void, RestResponseBundle<ShopRepresentation, ShopRepresentation>> {

		public static final String TAG = "GetShopTask";
		private Map<String, Object> params;

		@Override
		protected void onPreExecute() {
			ProgressDialogUtils.get(getActivity()).showLoadingProgressDialog();

		}

		@Override
		protected RestResponseBundle<ShopRepresentation, ShopRepresentation> doInBackground(
				Map<String, Object>... params) {
			try {
				this.params = params[0];

				return LMFAPIClient.getInstance().getShop(
						(Long) this.params.get("ShopId"));
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(
				RestResponseBundle<ShopRepresentation, ShopRepresentation> responseBundle) {
			ProgressDialogUtils.get(getActivity()).dismissProgressDialog();
			if (responseBundle != null)
				refreshResults(responseBundle.getResult());
			else {
				Toast.makeText(getActivity().getApplicationContext(),
						"Impossibile caricare il negozio.", Toast.LENGTH_LONG)
						.show();

				closeActivityListener.onCloseActivityRequest(
						"Impossibile caricare il negozio", -1);
			}
		}

	}

	public static Map<String, Object> buildGetShopsParams(long shopId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ShopId", shopId);
		return params;
	}
}
