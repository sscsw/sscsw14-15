package it.unito.lmf.android.ui.misc;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogUtils {
	private ProgressDialog progressDialog;

	private boolean destroyed = false;
	private Context context;
	private static Map<Context, ProgressDialogUtils> map=new HashMap<Context, ProgressDialogUtils>();

	// ***************************************
	// Activity methods
	// ***************************************

	private ProgressDialogUtils(Context context) {
		this.context=context;
	}
	
	public static ProgressDialogUtils get(Context context) {
		if(!map.containsKey(context))
			map.put(context, new ProgressDialogUtils(context));
		
		return map.get(context);					 
	}
	
	public void onDestroy() {
		this.destroyed = true;
	}

	// ***************************************
	// Public methods
	// ***************************************
	public void showLoadingProgressDialog() {
		this.showProgressDialog("Loading. Please wait...");
	}

	public void showProgressDialog(CharSequence message) {
		if (this.progressDialog == null) {
			this.progressDialog = new ProgressDialog(this.context);
			this.progressDialog.setIndeterminate(true);
		}

		this.progressDialog.setMessage(message);
		this.progressDialog.show();
	}

	public void dismissProgressDialog() {
		if (this.progressDialog != null && !this.destroyed) {
			try {
				this.progressDialog.dismiss();	
			} catch (Exception e) {
			}
		}
	}
}
