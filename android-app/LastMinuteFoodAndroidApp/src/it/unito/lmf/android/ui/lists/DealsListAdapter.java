package it.unito.lmf.android.ui.lists;

import it.unito.lmf.android.R;
import it.unito.lmf.android.utils.DataConverter;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.values.PriceUnit;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class DealsListAdapter extends BaseAdapter {

	class DealListViewCache {

		private View baseView;
		private TextView textTitle;
		private TextView textDescription;
		private TextView textPrice;
		private TextView textPublishedDate;
		private TextView textDiscountPrc;
		private ImageView imagePicture;

		public DealListViewCache(View baseView) {
			this.baseView = baseView;
		}

		public View getViewBase() {
			return baseView;
		}

		public TextView getTextTitle(int resource) {
			if (textTitle == null) {
				textTitle = (TextView) baseView.findViewById(R.id.title);
			}
			return textTitle;
		}

		public TextView getTextPrice(int resource) {
			if (textPrice == null) {
				textPrice = (TextView) baseView.findViewById(R.id.price);
			}
			return textPrice;
		}

		public TextView getTextDiscountPercentage(int resource) {
			if (textDiscountPrc == null) {
				textDiscountPrc = (TextView) baseView
						.findViewById(R.id.discountPercentage);
			}
			return textDiscountPrc;
		}

		public TextView getTextPublishedDate(int resource) {
			if (textPublishedDate == null) {
				textPublishedDate = (TextView) baseView
						.findViewById(R.id.publishedDate);
			}
			return textPublishedDate;
		}

		public TextView getTextDescription(int resource) {
			if (textDescription == null) {
				textDescription = (TextView) baseView
						.findViewById(R.id.description);
			}
			return textDescription;
		}

		public ImageView getImageView(int resource) {
			if (imagePicture == null) {
				imagePicture = (ImageView) baseView.findViewById(R.id.picture);
			}
			return imagePicture;
		}
	}

	private static final String TAG = "DealListAdapter";

	private List<DealRepresentation> results;
	private final LayoutInflater layoutInflater;
	private DisplayImageOptions options;
	private Context context;

	public DealsListAdapter(Context context, List<DealRepresentation> results) {
		this.context = context;
		this.results = results;
		this.layoutInflater = LayoutInflater.from(context);
		this.options = new DisplayImageOptions.Builder().imageScaleType(
				ImageScaleType.EXACTLY).build();
	}

	public int getCount() {
		return this.results != null ? this.results.size() : 0;
	}

	public DealRepresentation getItem(int position) {
		return this.results.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	private int resourceId = R.layout.deals_list_item;

	public View getView(int position, View convertView, ViewGroup parent) {
		DealListViewCache viewCache;

		boolean init = true;

		if (convertView == null) {
			convertView = this.layoutInflater
					.inflate(resourceId, parent, false);
			viewCache = new DealListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			init = false;
			convertView = (LinearLayout) convertView;
			viewCache = (DealListViewCache) convertView.getTag();
		}

		DealRepresentation result = getItem(position);
		if (result != null) {
			TextView t = viewCache.getTextTitle(resourceId);
			t.setText(result.getName());

			t = viewCache.getTextDescription(resourceId);
			t.setText(result.getDescription());
			String priceUnit = "/" + result.getPriceUnit();
			if (result.getPriceUnit().equals(PriceUnit.PIECE.toString()))
				priceUnit = "";
			viewCache.getTextPrice(resourceId).setText(
					result.getPrice() + " " + result.getCurrencySymbol()
							+ priceUnit);
			viewCache.getTextDiscountPercentage(resourceId).setText(
					"-" + result.getDiscountPercentage().intValue() + "%");
			viewCache.getTextPublishedDate(resourceId)
					.setText(
							DataConverter.getElapsedTime(result.getCreatedAt(),
									context));

			ImageView i = viewCache.getImageView(resourceId);

			if (init)
				ImageLoader.getInstance().displayImage(result.getImage(), i,
						options);
		}

		return convertView;
	}

}
