package it.unito.lmf.android.ui.activities.deal;

import it.unito.lmf.android.R;
import it.unito.lmf.android.ui.fragments.deals.DealsListFragment;
import it.unito.lmf.android.ui.fragments.deals.DealsMapFragment;
import it.unito.lmf.android.ui.misc.DefaultTabListener;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * A simple launcher activity containing a summary sample description, sample
 * log and a custom {@link android.support.v4.app.Fragment} which can display a
 * view.
 * <p>
 * For devices with displays with a width of 720dp or greater, the sample log is
 * always visible, on other devices it's visibility is controlled by an item on
 * the Action Bar.
 */
@SuppressWarnings("deprecation")
public class DealsActivity extends ActionBarActivity {

	public static final String TAG = "DealsActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Notice that setContentView() is not used, because we use the root
		// android.R.id.content as the container for each fragment

		// setup action bar for tabs
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);

		Tab tab = actionBar
				.newTab()
				.setText(R.string.deals_activity_nav_list_title)
				.setTabListener(
						new DefaultTabListener<DealsListFragment>(this,
								"dealsList", DealsListFragment.class));
		actionBar.addTab(tab);

		// MapFragment mMapFragment = MapFragment.newInstance();
		// FragmentTransaction fragmentTransaction =
		// getFragmentManager().beginTransaction();
		// fragmentTransaction.add(R.id.my_container, mMapFragment);
		// fragmentTransaction.commit();

		tab = actionBar
				.newTab()
				.setText(R.string.deals_activity_nav_map_title)
				.setTabListener(
						new DefaultTabListener<DealsMapFragment>(this,
								"dealsMap", DealsMapFragment.class));
		actionBar.addTab(tab);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.deals_activity_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
		// logToggle.setVisible(findViewById(R.id.sample_output) instanceof
		// ViewAnimator);
		// logToggle.setTitle(mLogShown ? R.string.sample_hide_log :
		// R.string.sample_show_log);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case R.id.menu_toggle_log:
		// mLogShown = !mLogShown;
		// ViewAnimator output = (ViewAnimator)
		// findViewById(R.id.sample_output);
		// if (mLogShown) {
		// output.setDisplayedChild(1);
		// } else {
		// output.setDisplayedChild(0);
		// }
		// supportInvalidateOptionsMenu();
		// return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
