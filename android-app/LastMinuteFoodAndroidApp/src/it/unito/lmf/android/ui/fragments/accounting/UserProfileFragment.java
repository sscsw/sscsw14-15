package it.unito.lmf.android.ui.fragments.accounting;

import it.unito.lmf.android.R;
import it.unito.lmf.android.ui.activities.login.UserSessionManager;
import it.unito.lmf.android.ui.activities.login.UserSessionManager.UserDetails;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class UserProfileFragment extends Fragment {

	TextView tvUsername;
	ImageView ivPicture;
	Button btLogout;

	UserSessionManager session;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public UserProfileFragment() {
		session = UserSessionManager.getInstance();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.user_profile_fragment, container,
				false);

		tvUsername = (TextView) view.findViewById(R.id.username);
		ivPicture = (ImageView) view.findViewById(R.id.picture);
		btLogout = (Button) view.findViewById(R.id.logout_button);

		btLogout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				session.logoutUser();
				session.checkLogin();
			}
		});

		if (session.isLoggedIn()) {
			UserDetails user = session.getUserDetails();
			tvUsername.setText(user.getUsername());
		}

		return view;
	}

}
