package it.unito.lmf.android.ui.activities.login;

import it.unito.lmf.android.LMFApplication;

import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.facebook.Session;

/**
 * http://www.androidhive.info/2012/08/android-session-management-using-shared
 * -preferences/
 * 
 *
 */
public class UserSessionManager {

	public enum SocialNetworkProvider {
		FACEBOOK(Facebook.class), TWITTER(Object.class), GOOGLE_PLUS(Object.class);
		
		Class<?> providerClass;
		
		private SocialNetworkProvider(Class<?> providerClass) {
			this.providerClass=providerClass;
		}

		public Class<?> getProviderClass() {
			return providerClass;
		}
		
	}

	public static class UserDetails {
		private String id;
		private String username;
		private String email;
		private String token;
		private SocialNetworkProvider socialNetwork;

		public UserDetails(String id, String username, String email,
				String token, SocialNetworkProvider socialNetwork) {
			super();
			this.id = id;
			this.username = username;
			this.email = email;
			this.token = token;
			this.socialNetwork = socialNetwork;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public SocialNetworkProvider getSocialNetwork() {
			return socialNetwork;
		}

		public void setSocialNetwork(SocialNetworkProvider socialNetwork) {
			this.socialNetwork = socialNetwork;
		}

	}

	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "LMFPref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	public static final String KEY_ID = "id";
	public static final String KEY_TOKEN = "token";
	// User name (make variable public to access from outside)
	public static final String KEY_NAME = "username";

	// Email address (make variable public to access from outside)
	public static final String KEY_EMAIL = "email";

	public static final String KEY_SOCIAL_NETWORK = "socialNetwork";

	private static UserSessionManager INSTANCE = null;

	private ConnectionRepository connectionRepository;

	private FacebookConnectionFactory fbConnectionFactory;
	
	// Constructor
	private UserSessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		
		this.connectionRepository = ((LMFApplication) context)
				.getConnectionRepository();
		this.fbConnectionFactory = ((LMFApplication) context)
				.getFacebookConnectionFactory();
	}

	public static UserSessionManager getInstance() {
		if (INSTANCE == null)
			INSTANCE = new UserSessionManager(LMFApplication.getContext());

		return INSTANCE;
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(UserDetails user) {
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);

		// Storing id in pref
		editor.putString(KEY_ID, user.getId());

		// Storing token in pref
		editor.putString(KEY_TOKEN, user.getToken());

		// Storing name in pref
		editor.putString(KEY_NAME, user.getUsername());

		// Storing email in pref
		editor.putString(KEY_EMAIL, user.getEmail());

		// Storing SN in pref
		if(user.getSocialNetwork()!=null)
			editor.putString(KEY_SOCIAL_NETWORK, user.getSocialNetwork().name());

		// commit changes
		editor.commit();
	}

	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public void checkLogin() {
		// Check login status
		if (!this.isLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			launchLoginActivity();
		}

	}

	/**
	 * Get stored session data
	 * */
	public UserDetails getUserDetails() {
		SocialNetworkProvider sn = null;
		try {
			sn = SocialNetworkProvider.valueOf(pref.getString(
					KEY_SOCIAL_NETWORK, null));
		} catch (Exception e) {
		}

		UserDetails user = new UserDetails(pref.getString(KEY_ID, null),
				pref.getString(KEY_NAME, null),
				pref.getString(KEY_EMAIL, null),
				pref.getString(KEY_TOKEN, null), sn);

		return user;
	}

	public boolean isConnectedWithSocial(Class<?> socialNetwork) {
		return connectionRepository.findPrimaryConnection(socialNetwork) != null;
	}

	public void disconnectSocial(Class<?> socialNetwork) {
		if(socialNetwork.equals(Facebook.class)){
			this.connectionRepository.removeConnections(this.fbConnectionFactory.getProviderId());
			try {
				Session.getActiveSession().close();	
			} catch (Exception e) {
			}
		}
	}
	
	public void disconnectAllSocial() {
		this.connectionRepository.removeConnections(this.fbConnectionFactory
				.getProviderId());
	}
	
	/**
	 * Clear session details
	 * */
	public void logoutUser() {
		// Disconnect social connection if available
		UserDetails user = getUserDetails();
		if(user.socialNetwork!=null)
			disconnectSocial(user.socialNetwork.getProviderClass());
		
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		
		// After logout redirect user to Login Activity
		launchLoginActivity();
	}

	protected void launchLoginActivity() {

		Intent i = new Intent(_context, LoginActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
		_context.startActivity(i);
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn() {
		if (pref.getBoolean(IS_LOGIN, false)==false)
			return false;

		UserDetails user = getUserDetails();
		if (user.socialNetwork == null)
			return true;

		// TODO: TEST SN Connection
		return true;
	}
}