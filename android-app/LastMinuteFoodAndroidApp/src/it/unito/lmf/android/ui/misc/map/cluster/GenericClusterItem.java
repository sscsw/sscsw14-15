package it.unito.lmf.android.ui.misc.map.cluster;

import com.google.maps.android.clustering.ClusterItem;

public abstract class GenericClusterItem<T> implements ClusterItem {

	private T item;

	public GenericClusterItem(T item) {
		this.item = item;
	}

	public T item() {
		return item;
	}
	
}
