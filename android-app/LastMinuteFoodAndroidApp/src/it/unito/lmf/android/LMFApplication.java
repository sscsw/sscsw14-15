package it.unito.lmf.android;

import org.springframework.security.crypto.encrypt.AndroidEncryptors;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.sqlite.SQLiteConnectionRepository;
import org.springframework.social.connect.sqlite.support.SQLiteConnectionRepositoryHelper;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class LMFApplication extends android.app.Application {
	private ConnectionFactoryRegistry connectionFactoryRegistry;
	private SQLiteOpenHelper repositoryHelper;
	private ConnectionRepository connectionRepository;

	private static Context mContext;

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this;

		// Create global configuration and initialize ImageLoader with this
		// config
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).diskCacheSize(50 * 1024 * 1024).diskCacheFileCount(1000)
				.diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
				.build();
		try {
			ImageLoader.getInstance().destroy();
		} catch (Exception e) {
		}
		ImageLoader.getInstance().init(config);

		initFacebook();
	}

	public static Context getContext() {
		return mContext;
	}

	// ***************************************
	// Application Methods
	// ***************************************
	protected void initFacebook() {
		// create a new ConnectionFactoryLocator and populate it with Facebook
		// ConnectionFactory
		this.connectionFactoryRegistry = new ConnectionFactoryRegistry();
		this.connectionFactoryRegistry
				.addConnectionFactory(new FacebookConnectionFactory(
						getFacebookAppId(), getFacebookAppSecret()));

		// set up the database and encryption
		this.repositoryHelper = new SQLiteConnectionRepositoryHelper(this);
		this.connectionRepository = new SQLiteConnectionRepository(
				this.repositoryHelper, this.connectionFactoryRegistry,
				AndroidEncryptors.text("password", "5c0744940b5c369b"));
	}

	// ***************************************
	// Private methods
	// ***************************************
	private String getFacebookAppId() {
		return getString(R.string.facebook_app_id);
	}

	private String getFacebookAppSecret() {
		return getString(R.string.facebook_app_secret);
	}

	// ***************************************
	// Public methods
	// ***************************************
	public ConnectionRepository getConnectionRepository() {
		return this.connectionRepository;
	}

	public FacebookConnectionFactory getFacebookConnectionFactory() {
		return (FacebookConnectionFactory) this.connectionFactoryRegistry
				.getConnectionFactory(Facebook.class);
	}
}