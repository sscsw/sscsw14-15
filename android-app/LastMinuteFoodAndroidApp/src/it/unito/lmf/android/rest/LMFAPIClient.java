package it.unito.lmf.android.rest;

import it.unito.lmf.android.LMFApplication;
import it.unito.lmf.android.R;
import it.unito.lmf.android.utils.RestResponseBundle;
import it.unito.lmf.android.utils.pagination.Page;
import it.unito.lmf.android.utils.pagination.PageImpl;
import it.unito.lmf.android.utils.pagination.PageRequest;
import it.unito.lmf.android.utils.pagination.Pageable;
import it.unito.lmf.android.utils.pagination.Sort.Order;
import it.unito.lmf.domain.dsl.accounting.TokenRepresentation;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.deal.DealSearchData;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;
import it.unito.lmf.domain.dsl.misc.ErrorRepresentation;
import it.unito.lmf.domain.dsl.misc.Pagination;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LMFAPIClient {

	private static final String TAG = "LMFAPIClient";
	private String baseUrl;
	private static LMFAPIClient instance;

	public LMFAPIClient(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public static LMFAPIClient getInstance() {
		if (instance == null)
			instance = new LMFAPIClient(LMFApplication.getContext()
					.getResources().getString(R.string.base_uri_rest));
		return instance;
	}

	/*--- AUTHENTICATION ---*/

	public RestResponseBundle<TokenRepresentation, TokenRepresentation> getToken(
			String username, String password) throws LMFAPIClientException {
		try {
			// The URL for making the GET request
			String url = baseUrl + "auth/token";

			// Perform the HTTP GET request to the Deal API
			HttpHeaders headers = acceptJSON();
			headers.setAuthorization(new HttpBasicAuthentication(username,
					password));
			ResponseEntity<TokenRepresentation> response = getDefaultRestTemplate()
					.exchange(url, HttpMethod.GET, headers, null,
							TokenRepresentation.class);

			return new RestResponseBundle<TokenRepresentation, TokenRepresentation>(
					response.getBody(), response);
		} catch (Exception e) {
			throw handlerError("Unable to get token for user " + username, e);
		}
	}

	/*--- DEALs---*/

	public RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]> getDeals(
			Pageable pageable) throws LMFAPIClientException {
		return getDeals(pageable, null);
	}

	public RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]> getDeals(
			Pageable pageable, DealSearchData searchParams)
			throws LMFAPIClientException {
		try {
			// The URL for making the GET request
			Uri.Builder ub = Uri.parse(baseUrl).buildUpon();
			ub.appendPath("deals");
			ub = appendPaginationParams(ub, pageable);

			// Append SearchParams
			if (searchParams != null) {
				for (Field field : DealSearchData.class.getDeclaredFields()) {
					ub.appendQueryParameter(field.getName(),
							field.get(searchParams).toString());
				}
			}

			String url = ub.build().toString();

			if (pageable == null)
				pageable = new PageRequest(0, 20);

			Log.d(TAG, "Getting deals, page size " + pageable.getPageSize()
					+ " number " + pageable.getPageNumber());

			// Perform the HTTP GET request to the Google search API
			HttpHeaders headers = acceptJSON();

			ResponseEntity<DealRepresentation[]> response = getDefaultRestTemplate()
					.exchange(url, HttpMethod.GET, headers, null,
							DealRepresentation[].class);

			Page<DealRepresentation> page = getPage(response, pageable);
			return new RestResponseBundle<Page<DealRepresentation>, DealRepresentation[]>(
					page, response);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			throw handlerError("Unable to retrieve deal list", e);
		}
	}

	public RestResponseBundle<DealRepresentation, DealRepresentation> getDeal(
			long id) throws LMFAPIClientException {
		try {
			// The URL for making the GET request
			String url = baseUrl + "deals/{dealId}";

			Log.d(TAG, "Getting deal " + id);

			// Perform the HTTP GET request to the Deal API
			ResponseEntity<DealRepresentation> response = getDefaultRestTemplate()
					.exchange(url, HttpMethod.GET, acceptJSON(), null,
							DealRepresentation.class, id);

			return new RestResponseBundle<DealRepresentation, DealRepresentation>(
					response.getBody(), response);
		} catch (Exception e) {
			throw handlerError("Unable to retrieve deal " + id, e);
		}
	}

	/*--- SHOPs---*/

	public RestResponseBundle<Page<ShopRepresentation>, ShopRepresentation[]> getShops(
			Pageable pageable) throws LMFAPIClientException {
		try {
			// The URL for making the GET request
			String url = baseUrl + "shops" + "?"
					+ Pagination.QUERY_PAG_PAGE_NUMBER + "={pageNumber}&"
					+ Pagination.QUERY_PAG_PAGE_SIZE + "={pageSize}";

			if (pageable == null)
				pageable = new PageRequest(0, 20);

			Log.d(TAG, "Getting shops, page size " + pageable.getPageSize()
					+ " number " + pageable.getPageNumber());

			// Perform the HTTP GET request to the Google search API
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			ResponseEntity<ShopRepresentation[]> response = getDefaultRestTemplate()
					.exchange(url, HttpMethod.GET, headers, null,
							ShopRepresentation[].class,
							pageable.getPageNumber(), pageable.getPageSize());

			Page<ShopRepresentation> page = getPage(response, pageable);
			return new RestResponseBundle<Page<ShopRepresentation>, ShopRepresentation[]>(
					page, response);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			throw handlerError("Unable to retrieve shop list", e);
		}
	}

	public RestResponseBundle<ShopRepresentation, ShopRepresentation> getShop(
			long id) throws LMFAPIClientException {
		try {
			// The URL for making the GET request
			String url = baseUrl + "shops/{shopId}";

			Log.d(TAG, "Getting shop " + id);

			// Perform the HTTP GET request to the Deal API
			ResponseEntity<ShopRepresentation> response = getDefaultRestTemplate()
					.exchange(url, HttpMethod.GET, acceptJSON(), null,
							ShopRepresentation.class, id);

			return new RestResponseBundle<ShopRepresentation, ShopRepresentation>(
					response.getBody(), response);
		} catch (Exception e) {
			throw handlerError("Unable to retrieve shop " + id, e);
		}
	}

	/*---	UTILITY METHODS ---*/

	protected class HttpClientErrorExceptionInputMessage implements
			HttpInputMessage {

		HttpHeaders headers;
		InputStream body;

		public HttpClientErrorExceptionInputMessage(HttpClientErrorException e) {
			this.headers = new HttpHeaders();
			this.body = new ByteArrayInputStream(e.getResponseBodyAsByteArray());
		}

		@Override
		public HttpHeaders getHeaders() {
			return headers;
		}

		@Override
		public InputStream getBody() throws IOException {
			return body;
		}

	}

	protected static LMFAPIClientException handlerError(Exception e) {
		return handlerError("", e);
	}

	protected static LMFAPIClientException handlerError(String msg, Exception e) {
		Log.e(TAG, e.getMessage(), e);

		if (e instanceof HttpClientErrorException) {
			HttpClientErrorException hce = (HttpClientErrorException) e;
			try {
				ErrorRepresentation apiError = extractError(hce);
				return new LMFAPIClientException(msg, apiError);
			} catch (Exception e2) {
				return new LMFAPIClientException(msg, new ServerErrorException(
						hce.getStatusCode(), hce.getStatusText(), hce));
			}
		} else
			return new LMFAPIClientException(msg, e);
	}

	protected static ErrorRepresentation extractError(HttpClientErrorException e)
			throws Exception {
		try {
			return new ObjectMapper().readValue(e.getResponseBodyAsString(),
					ErrorRepresentation.class);
			// new
			// MappingJackson2HttpMessageConverter().read(ErrorRepresentation.class,
			// new HttpClientErrorExceptionInputMessage(e));
		} catch (Exception e2) {
			throw e2;
		}
	}

	protected static <T, V> Page<T> getPage(ResponseEntity<T[]> response,
			Pageable pageable) {
		// Update pageable with response data
		int total = Integer.parseInt(response.getHeaders().getFirst(
				Pagination.HEADER_PAG_TOTAL_COUNT));
		int pageNumber = Integer.parseInt(response.getHeaders().getFirst(
				Pagination.HEADER_PAG_PAGE_NUMBER));
		int pageSize = Integer.parseInt(response.getHeaders().getFirst(
				Pagination.HEADER_PAG_PAGE_SIZE));
		pageable = new PageRequest(pageNumber, pageSize, pageable.getSort());

		return new PageImpl<T>(Arrays.asList(response.getBody()), pageable,
				total);
	}

	protected static HttpHeaders acceptJSON() {
		return acceptJSON(null);
	}

	protected static HttpHeaders acceptJSON(HttpHeaders headers) {
		if (headers == null)
			headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		return headers;
	}

	protected static LMFRestTemplate getDefaultRestTemplate() {
		// Create a new RestTemplate instance
		LMFRestTemplate restTemplate = new LMFRestTemplate(10 * 1000);

		// Set a custom MappingJacksonHttpMessageConverter that supports
		// the application/json media type
		MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
		restTemplate.getMessageConverters().add(messageConverter);

		return restTemplate;
	}

	protected static Uri.Builder appendPaginationParams(Uri.Builder ub,
			Pageable pageable) {
		ub.appendQueryParameter(Pagination.QUERY_PAG_PAGE_NUMBER,
				Integer.toString(pageable.getPageNumber()));
		ub.appendQueryParameter(Pagination.QUERY_PAG_PAGE_SIZE,
				Integer.toString(pageable.getPageSize()));

		if (pageable.getSort() != null) {
			for (Order order : pageable.getSort())
				ub.appendQueryParameter("sort", order.getProperty() + ","
						+ order.getDirection().name());
		}

		return ub;
	}
	/*---	UTILITY METHODS ---*/

}
