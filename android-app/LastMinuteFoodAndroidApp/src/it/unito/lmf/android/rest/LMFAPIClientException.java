package it.unito.lmf.android.rest;

import it.unito.lmf.domain.dsl.misc.ErrorRepresentation;

import org.springframework.core.NestedRuntimeException;

public class LMFAPIClientException extends NestedRuntimeException {

	private static final long serialVersionUID = 1L;

	private ErrorRepresentation apiError;

	/**
	 * Construct a new instance of {@code HttpClientException} with the given
	 * message.
	 * 
	 * @param msg
	 *            the message
	 */
	public LMFAPIClientException(String msg) {
		super(msg);
	}
	
	/**
	 * Construct a new instance of {@code HttpClientException} with the given
	 * message.
	 * 
	 * @param msg
	 *            the message
	 */
	public LMFAPIClientException(String msg, ErrorRepresentation apiError) {
		super(msg);
		this.apiError=apiError;
	}

	/**
	 * Construct a new instance of {@code HttpClientException} with the given
	 * message and exception.
	 * 
	 * @param msg
	 *            the message
	 * @param ex
	 *            the exception
	 */
	public LMFAPIClientException(String msg, Throwable ex) {
		super(msg, ex);
	}
	

	
	public ErrorRepresentation getApiError() {
		return apiError;
	}

}
