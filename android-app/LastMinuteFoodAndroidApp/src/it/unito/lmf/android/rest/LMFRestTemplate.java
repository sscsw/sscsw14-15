package it.unito.lmf.android.rest;

import java.net.URLDecoder;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.util.Log;

public class LMFRestTemplate extends RestTemplate {

	public LMFRestTemplate(int timeout) {
		if (getRequestFactory() instanceof SimpleClientHttpRequestFactory) {
			Log.d("HTTP", "HttpUrlConnection is used");
			((SimpleClientHttpRequestFactory) getRequestFactory())
					.setConnectTimeout(timeout);
			((SimpleClientHttpRequestFactory) getRequestFactory())
					.setReadTimeout(timeout);
		} else if (getRequestFactory() instanceof HttpComponentsClientHttpRequestFactory) {
			Log.d("HTTP", "HttpClient is used");
			((HttpComponentsClientHttpRequestFactory) getRequestFactory())
					.setReadTimeout(timeout);
			((HttpComponentsClientHttpRequestFactory) getRequestFactory())
					.setConnectTimeout(timeout);
		}
	}

	public <T, E> ResponseEntity<T> exchange(String url, HttpMethod method,
			HttpHeaders headers, E body, Class<T> responseType,
			Object... uriVariables) throws RestClientException {

		HttpEntity<E> entity = new HttpEntity<E>(body, headers);
		url = URLDecoder.decode(url);
		return exchange(url, HttpMethod.GET, entity, responseType, uriVariables);
	}
}