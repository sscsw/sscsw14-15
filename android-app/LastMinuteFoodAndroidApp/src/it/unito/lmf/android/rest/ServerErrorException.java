package it.unito.lmf.android.rest;

import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public class ServerErrorException extends NestedRuntimeException {

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatus;
	private String statusMessage;
	private HttpClientErrorException httpClientErrorException;

	/**
	 * Construct a new instance of {@code HttpClientException} with the given
	 * message.
	 * 
	 * @param msg
	 *            the message
	 */
	public ServerErrorException(HttpStatus httpStatus, String statusMessage,
			HttpClientErrorException httpClientErrorException) {
		super("Server reported " + httpStatus + " "+ statusMessage);
		this.httpStatus = httpStatus;
		this.statusMessage = statusMessage;
		this.httpClientErrorException = httpClientErrorException;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public HttpClientErrorException getHttpClientErrorException() {
		return httpClientErrorException;
	}

}
