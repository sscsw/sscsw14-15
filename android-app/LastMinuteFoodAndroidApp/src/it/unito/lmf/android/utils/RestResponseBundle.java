package it.unito.lmf.android.utils;

import org.springframework.http.ResponseEntity;

public class RestResponseBundle<RESULT_TYPE, RESPONSE_TYPE> {
	private RESULT_TYPE result;
	private ResponseEntity<RESPONSE_TYPE> responseEntity;

	public RestResponseBundle(RESULT_TYPE result,
			ResponseEntity<RESPONSE_TYPE> responseEntity) {
		super();
		this.result = result;
		this.responseEntity = responseEntity;
	}

	public RESULT_TYPE getResult() {
		return result;
	}

	public void setResult(RESULT_TYPE result) {
		this.result = result;
	}

	public ResponseEntity<RESPONSE_TYPE> getResponseEntity() {
		return responseEntity;
	}

	public void setResponseEntity(ResponseEntity<RESPONSE_TYPE> responseEntity) {
		this.responseEntity = responseEntity;
	}

}
