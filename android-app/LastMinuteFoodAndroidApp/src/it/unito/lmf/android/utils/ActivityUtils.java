package it.unito.lmf.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivityUtils {
	public static <P extends Activity, T extends Activity> void launchWithParent(
			Context ctx, Class<P> parent, Class<T> target, Bundle extras) {
		if (extras == null)
			extras = new Bundle();

		Intent intent = new Intent(ctx, target);
		extras.putString(Intent.EXTRA_INTENT, parent.getName());
		intent.putExtras(extras);
		ctx.startActivity(intent);
	}

	public static <P extends Activity, T extends Activity> void launchWithParent(
			Context ctx, Class<P> parent, Class<T> target) {
		launchWithParent(ctx, parent, target, null);
	}

	public static <P extends Activity, T extends Activity> void launchWithParent(
			P parentAndContext, Class<T> target) {
		launchWithParent(parentAndContext, parentAndContext.getClass(), target);
	}

	public static <P extends Activity, T extends Activity> void launchWithParent(
			P parentAndContext, Class<T> target, Bundle extras) {
		launchWithParent(parentAndContext, parentAndContext.getClass(), target,
				extras);
	}
}
