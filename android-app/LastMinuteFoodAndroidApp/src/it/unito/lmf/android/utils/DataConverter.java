package it.unito.lmf.android.utils;

import java.util.Date;

import android.content.Context;

public class DataConverter {
	public static String getElapsedTime(Date date, Context context) {

		// TODO Load strings from resources depending on locale
		// context.getResources().getString(R.string)

		String days = "%s days ago";
		String hours = "%s hours ago";
		String minutes = "%s minutes ago";

		// in milliseconds
		long diff = new Date().getTime() - date.getTime();

		// long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays > 0)
			return String.format(days, diffDays);

		if (diffHours > 0)
			return String.format(hours, diffHours);

		return String.format(minutes, diffMinutes);
	}
}
