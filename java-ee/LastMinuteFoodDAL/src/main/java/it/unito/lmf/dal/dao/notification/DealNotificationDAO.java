package it.unito.lmf.dal.dao.notification;

import static it.unito.lmf.dal.entities.notification.QDealNotification.dealNotification;
import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.dal.entities.notification.DealNotification.VisibilityType;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class DealNotificationDAO extends QueryDSLAbstractGenericDAO<DealNotification> {

	@PersistenceContext
	protected EntityManager entityManager;

	public DealNotificationDAO() {
		super(DealNotification.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	/**
	 * 
	 * @return a collection of {@link AdministratorNotification}, which are still undone.
	 */
	public Collection<DealNotification> findAllPending(long userId, VisibilityType visibility) {
		JPAQuery q = super.prepareJPAQuery().from(dealNotification)
				.where(dealNotification.read.eq(false))
				.where(dealNotification.user.id.eq(userId))
				.where(dealNotification.visibility.contains(visibility.name()));
		return q.list(dealNotification);
	}

}
