package it.unito.lmf.dal.dao.notification;

import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class NotificationSettingsDAO extends QueryDSLAbstractGenericDAO<NotificationSettingsDAO> {

	@PersistenceContext
	protected EntityManager entityManager;

	public NotificationSettingsDAO() {
		super(NotificationSettingsDAO.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
