package it.unito.lmf.dal.entities.accounting;

import it.unito.lmf.dal.entities.accounting.exceptions.EmailAlreadyInUseException;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.dal.entities.deal.FavoriteDeal;
import it.unito.lmf.dal.entities.feedback.Feedback;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.dal.entities.notification.NotificationSettings;
import it.unito.lmf.dal.entities.shop.FavoriteShop;
import it.unito.lmf.dal.entities.utils.UniqueConstraintExceptionMapping;
import it.unito.lmf.dal.entities.utils.UniqueConstraintMappings;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;
@Entity
@Table(name = "User", uniqueConstraints = {
		@UniqueConstraint(name = User.UniqueConstraints.Constants.UNIQUE_email, columnNames = { "email" }),
		@UniqueConstraint(name = User.UniqueConstraints.Constants.UNIQUE_username, columnNames = { "username" }) })
// @objid("d37c13a0-1561-4fd3-8bb3-d24df220255a")
@UniqueConstraintMappings({
		@UniqueConstraintExceptionMapping(constraintName = User.UniqueConstraints.Constants.UNIQUE_email, exception = EmailAlreadyInUseException.class),
		@UniqueConstraintExceptionMapping(constraintName = User.UniqueConstraints.Constants.UNIQUE_username, exception = UsernameAlreadyInUseException.class) })
public class User implements Serializable, Cloneable {

	private static final long serialVersionUID = -3953951384430882603L;

	public enum UniqueConstraints {
		UNIQUE_email(Constants.UNIQUE_email), UNIQUE_username(
				Constants.UNIQUE_username);

		UniqueConstraints(String genderString) {
		}

		public static class Constants {
			public static final String UNIQUE_email = "UNIQUE_email";
			public static final String UNIQUE_username = "UNIQUE_username";
		}
	}

	// @objid("61104140-a042-4cc5-9095-787bc8b97ef9")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userID", unique = true, nullable = false)
	private long id;

	// @objid("d5766bb7-47b4-4386-9c40-3b7ab929ea10")
	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String email;

	private String password;

	private String signInProvider;

	private String image;
	
	// @objid("d685e87b-b95c-41e3-bd40-b953012e1590")
	private String address;
	
	private double latitude;
	private double longitude;

	// @objid("8c194b82-4f16-4199-9b36-d930e5220428")
	@Column(nullable = false)
	private boolean isAdmin = false;

	// @objid("bc6c1959-4e2c-4105-a2ed-c83e15d664ff")
	@Column(nullable = false)
	private boolean isEnabled = false;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdAt", nullable = false, length = 19)
	// @objid ("9841509a-8ff6-4aa2-a0ce-aace78a92509")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedAt", nullable = false, length = 19)
	// @objid ("bf0a09eb-9492-4495-ab87-ee85baabf7a5")
	private Date modifiedAt;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	// @objid("136d9993-9f57-4f1b-aa0c-c15c9a37592f")
	private Set<Feedback> feedbacks = new HashSet<Feedback>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	// @objid("3d2264b8-8fc8-4f70-81b0-4fb33882975f")
	private Set<DealNotification> dealNotifications = new HashSet<DealNotification>();

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
	// @objid("32685268-4c58-498e-8d8e-8c2f602b5139")
	private NotificationSettings notificationSettings;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
	// @objid("21a75c60-e42c-41a8-a0ec-2ae4fc3d02c6")
	private Seller seller;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<FavoriteDeal> favoriteDeals = new HashSet<FavoriteDeal>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<FavoriteShop> favoriteShops = new HashSet<FavoriteShop>();

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
		this.modifiedAt = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		this.modifiedAt = new Date();
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public boolean hasSocialSignIn() {
		return signInProvider != null;
	}

	public String getSignInProvider() {
		return signInProvider;
	}

	public void setSignInProvider(String signInProvider) {
		this.signInProvider = signInProvider;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Set<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(Set<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public Set<DealNotification> getDealNotifications() {
		return dealNotifications;
	}

	public void setDealNotifications(Set<DealNotification> dealNotifications) {
		this.dealNotifications = dealNotifications;
	}

	public NotificationSettings getNotificationSettings() {
		return notificationSettings;
	}

	public void setNotificationSettings(
			NotificationSettings notificationSettings) {
		this.notificationSettings = notificationSettings;
	}

	public boolean isSeller() {
		return seller != null;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<FavoriteDeal> getFavoriteDeals() {
		return favoriteDeals;
	}

	public void setFavoriteDeals(Set<FavoriteDeal> favoriteDeals) {
		this.favoriteDeals = favoriteDeals;
	}

	public Set<FavoriteShop> getFavoriteShops() {
		return favoriteShops;
	}

	public void setFavoriteShops(Set<FavoriteShop> favoriteShops) {
		this.favoriteShops = favoriteShops;
	}

	@Transient
	public String getName() {
		return firstName + " " + lastName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email
				+ ", signInProvider=" + signInProvider + ", address=" + address
				+ ", isAdmin=" + isAdmin + ", isEnabled=" + isEnabled
				+ ", feedbacks=" + feedbacks + ", dealNotifications="
				+ dealNotifications + ", notificationSettings="
				+ notificationSettings + ", seller=" + seller + "]";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
