package it.unito.lmf.dal.entities.shop;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="ShopCategory")
//@objid("9308cbe9-ffc0-4d12-85e3-80cbb7188223")
public class ShopCategory implements Serializable {

	private static final long serialVersionUID = -8455810220145843752L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "shopCategoryID", unique = true, nullable = false)
	//@objid("2b0cc3c7-3563-46b5-a145-41752e25362f")
	private long id;

	//@objid("5994ad76-d488-48f4-9460-86b3627731b0")
	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ShopCategory [id=" + id + ", name=" + name + "]";
	}
	
}
