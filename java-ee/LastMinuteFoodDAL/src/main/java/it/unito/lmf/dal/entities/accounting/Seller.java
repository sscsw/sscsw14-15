package it.unito.lmf.dal.entities.accounting;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="Seller")
//@objid ("286fd80b-985e-4c44-85e5-33cb7cac8084")
public class Seller  implements Serializable {
	
	private static final long serialVersionUID = 4851420019087766304L;

	//@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "user"))
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sellerID", unique = true, nullable = false)
	private long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private User user;
	
    //@objid ("c6add0d5-5c8e-4eaf-8a15-356a1a4a35a9")
    private boolean isAuthorized;

    //@objid ("4a07d459-aa97-4738-9fff-6ec6211a87e1")
    private String ragioneSociale;

    //@objid ("a9fd0e4b-2c65-4087-b2c8-342d63fb556a")
    private String codiceFiscale;

    //@objid ("517f4a0a-9061-447b-8cf0-f2486dc2cc70")
    private String phoneNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		this.id=user.getId();
	}

	public boolean isAuthorized() {
		return isAuthorized;
	}

	public void setAuthorized(boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Transient
	public String getName() {
		return user.getName();
	}
	
	@Override
	public String toString() {
		return "Seller [id=" + id + ", user=" + user + ", isAuthorized="
				+ isAuthorized + ", ragioneSociale=" + ragioneSociale
				+ ", codiceFiscale=" + codiceFiscale + ", phoneNumber="
				+ phoneNumber + "]";
	}

}
