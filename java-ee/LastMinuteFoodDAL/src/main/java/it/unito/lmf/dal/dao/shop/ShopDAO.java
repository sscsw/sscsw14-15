package it.unito.lmf.dal.dao.shop;

import static it.unito.lmf.dal.entities.shop.QShop.shop;
import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.shop.Shop;

import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.util.CollectionUtils;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class ShopDAO extends QueryDSLAbstractGenericDAO<Shop> {

	public ShopDAO() {
		super(Shop.class);
	}

	/**
	 * 
	 * @param ownerId
	 * @return a collection of {@link Shop}, whose Owner has the given Id.
	 */
	public Collection<Shop> findByOwner(Long ownerId) {
		if (ownerId == null)
			throw new IllegalArgumentException("Owner Id cannot be null.");

		JPAQuery q = super.prepareJPAQuery().from(shop)
				.where(shop.owner.id.eq(ownerId));
		return q.list(shop);
	}

	/**
	 * 
	 * @param ownerId
	 * @return a collection of {@link Shop}, whose Owner has the given Id.
	 */
	public Page<Shop> findByOwner(Long ownerId, Pageable pageable) {
		return findAll(shop.owner.id.eq(ownerId), pageable);
	}
	
	public Page<Shop> findAllAuthorized(Pageable pageable) {
		return findAll(shop.isAuthorized.eq(true), pageable);
	}
	
	public Collection<Shop> findAllPending() {
		Iterable<Shop> shops = findAll(shop.isAuthorized.eq(false));
		
		if (shops instanceof ArrayList<?>)
			return (ArrayList<Shop>)shops;
			
		ArrayList<Shop> retShops = new ArrayList<Shop>();
		for (Shop s : shops) {
			retShops.add(s);
		}
		
		return retShops;
	}

}
