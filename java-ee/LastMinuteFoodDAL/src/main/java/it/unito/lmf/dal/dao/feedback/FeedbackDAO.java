package it.unito.lmf.dal.dao.feedback;

import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.feedback.Feedback;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class FeedbackDAO extends QueryDSLAbstractGenericDAO<Feedback> {

	@PersistenceContext
	protected EntityManager entityManager;

	public FeedbackDAO() {
		super(Feedback.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
