package it.unito.lmf.dal.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Basic implementation of the {@link GenericDAO} with CRUD operations. Must
 * pass the entity type and the {@link getEntityManager()}.
 * 
 * @author Biava Lorenzo
 *
 * @param <T>
 *            the type of the entity.
 */
@Stateless
public abstract class AbstractGenericDAO<T> implements GenericDAO<T> {

	// public static final PersistenceContextType
	// PERSISTENCE_CONTEXT_TYPE=PersistenceContextType.EXTENDED;
	@PersistenceContext
	// (type=PersistenceContextType.EXTENDED)
	protected EntityManager entityManager;

	/**
	 * This method MUST be overriden to use a custom {@link EntityManager}.
	 */
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	protected Class<T> entityClass;

	public AbstractGenericDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public T create(final T entity) {
		assert (getEntityManager() != null);
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public boolean delete(final Object id) {
		assert (getEntityManager() != null);
		Object entity = find(id);

		if (entity != null) {
			getEntityManager().remove(entity);
			return true;
		}

		return false;
	}

	@Override
	public T find(final Object id) {
		assert (getEntityManager() != null);
		T entity = getEntityManager().find(entityClass, id);
		return entity;
	}

	@Override
	public boolean exists(final Object id) {
		assert (getEntityManager() != null);
		return this.find(id) != null;
	}

	@Override
	public T update(final T entity) {
		assert (getEntityManager() != null);
		return getEntityManager().merge(entity);
	}

	protected Class<T> getEntityClass() {
		assert (getEntityManager() != null);
		return entityClass;
	}

	protected String getEntityClassName() {
		assert (entityClass != null);
		return entityClass.getName().toString();
	}

	protected String getEntityClassSimpleName() {
		assert (entityClass != null);
		return entityClass.getSimpleName().toString();
	}

	@Override
	public void forceFlush() {
		getEntityManager().flush();
	}
}
