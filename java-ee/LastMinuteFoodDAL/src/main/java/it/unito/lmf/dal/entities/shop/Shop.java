package it.unito.lmf.dal.entities.shop;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.feedback.Feedback;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name = "Shop")
//@objid("5c02b7a6-d7ca-42b7-af27-68ac85a4a836")
public class Shop implements Serializable {

	private static final long serialVersionUID = 1094994688353638962L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "shopID", unique = true, nullable = false)
	//@objid("efd2d125-b62c-4f56-829b-5771cc0cad0a")
	private long id;

	//@objid("b4060158-c988-4d71-b753-d368b67ee201")
	private String partitaIVA;

	//@objid("8e2dcae6-c374-4cb0-b191-e61e81655e76")
	private String address;
	
	private Double latitude;
	
	private Double longitude;

	//@objid("3fd19c0c-3412-4cd4-b3d2-be1fee6ce9f2")
	private String name;

	//@objid("57322e18-477b-4875-ac33-64bd0fcb6835")
	private String phoneNumber;

	//@objid("ea85d9c5-dc6b-4c19-b1c6-366a691426f9")
	private String description;
	
	private String image;

	//@objid("ac6fb709-4b19-47c1-a76a-be9d587ab868")
	private boolean isAuthorized;

	//@objid("2b7b64ff-dca6-48bc-b1e3-e4c44b36a1cf")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdAt", nullable = false, length = 19)
	private Date createdAt;

	@OneToOne(fetch = FetchType.EAGER)
	private Seller owner;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
	//@objid("7fd54e7d-6eb3-41c5-8ef2-5162f579ee29")
	private Set<Deal> deal = new HashSet<Deal>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
	//@objid("ba190b86-12ea-4edc-bd9d-68bf03a77265")
	private Set<Feedback> feedback = new HashSet<Feedback>();

	@ManyToMany(fetch = FetchType.EAGER)
	//@objid("d62ab772-c515-40ce-a2a7-840561b21d60")
	private Set<ShopCategory> categories = new HashSet<ShopCategory>();

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPartitaIVA() {
		return partitaIVA;
	}

	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAuthorized() {
		return isAuthorized;
	}

	public void setAuthorized(boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Set<Deal> getDeal() {
		return deal;
	}

	public void setDeal(Set<Deal> deal) {
		this.deal = deal;
	}

	public Set<Feedback> getFeedback() {
		return feedback;
	}

	public void setFeedback(Set<Feedback> feedback) {
		this.feedback = feedback;
	}

	public Set<ShopCategory> getCategories() {
		return categories;
	}

	public void setCategories(Set<ShopCategory> categories) {
		this.categories = categories;
	}

	public Seller getOwner() {
		return owner;
	}

	public void setOwner(Seller owner) {
		this.owner = owner;
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Shop [id=" + id + ", partitaIVA=" + partitaIVA + ", address="
				+ address + ", name=" + name + ", phoneNumber=" + phoneNumber
				+ ", description=" + description + ", isAuthorized="
				+ isAuthorized + ", createdAt=" + createdAt + ", owner="
				+ owner + ", deal=" + deal + ", feedback=" + feedback
				+ ", categories=" + categories + "]";
	}
	
}
