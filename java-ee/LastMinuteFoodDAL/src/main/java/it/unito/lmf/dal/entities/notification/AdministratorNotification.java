package it.unito.lmf.dal.entities.notification;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.shop.Shop;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="AdministratorNotification")
//@objid ("5a0874c2-8550-47f1-9d6c-92dc2befcd4d")
public class AdministratorNotification  implements Serializable {

	public enum NotificationType {
		SELLER_AUTHORIZATION_REQUEST, NEW_SHOP_REQUEST
	}
	
	private static final long serialVersionUID = 344416870581830058L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "administratorNotificationID", unique = true, nullable = false)
    //@objid ("117b9cc3-840d-4778-96d3-0a28d4ed3593")
    private long id;

    //@objid ("2d437b5c-3c53-441f-8108-f2ab025316d9")
    private String type;

    //@objid ("dd11c0f4-e75f-4fdc-b1d4-62f06f104ea0")
    private String message;

    //@objid ("ae5d2178-4aed-4147-b5ca-e8d60133bb5e")
    private boolean done;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true/*, cascade={CascadeType.ALL}*/)
    @Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "sellerID", nullable = true)
	private Seller seller;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true/*, cascade={CascadeType.ALL}*/)
    @Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "shopID", nullable = true)
	private Shop shop;
    
    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name = "createdAt", nullable = false, length = 19)
    private Date createdAt;

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
	}
	
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "AdministratorNotification [id=" + id + ", type=" + type
				+ ", message=" + message + ", done=" + done + ", seller="
				+ seller + ", shop=" + shop + ", createdAt=" + createdAt + "]";
	}

}
