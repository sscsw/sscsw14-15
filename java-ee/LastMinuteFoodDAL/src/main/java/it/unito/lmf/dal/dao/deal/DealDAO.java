package it.unito.lmf.dal.dao.deal;

import static it.unito.lmf.dal.entities.deal.QDeal.deal;
import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.deal.Deal;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mysema.query.jpa.impl.JPAQuery;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class DealDAO extends QueryDSLAbstractGenericDAO<Deal> {

	public DealDAO() {
		super(Deal.class);
	}

	public Collection<Deal> findByShop(long shopId) {
		if (shopId < 1)
			throw new IllegalArgumentException("ShopId must be greater than 0.");

		JPAQuery q = super.prepareJPAQuery().from(deal)
				.where(deal.shop.id.eq(shopId));

		return q.list(deal);
	}

	public Page<Deal> findByShop(long shopId, Pageable pageable) {
		if (shopId < 1)
			throw new IllegalArgumentException("ShopId must be greater than 0.");

		return findAll(deal.shop.id.eq(shopId), pageable);
	}

	// public Page<Deal> findAllPaginated(Pageable pageable) {
	//
	// JPAQuery q = super.prepareJPAQuery().from(deal)
	// .limit(pageable.getPageSize()).offset(pageable.getOffset());
	// // .orderBy(pageable.getSort());
	//
	// SearchResults<Deal> results = q.listResults(deal);
	//
	// return new PageImpl<Deal>(results.getResults(), pageable,
	// results.getTotal());
	// }

	// public Page<Deal> findAllPaginated(Pageable pageable) {
	//
	// return findAll(null, pageable);
	// }

}
