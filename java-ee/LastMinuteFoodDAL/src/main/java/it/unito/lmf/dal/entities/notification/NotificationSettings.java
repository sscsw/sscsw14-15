package it.unito.lmf.dal.entities.notification;

import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.deal.DealCategory;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="NotificationSettings")
//@objid ("60bb97f9-0855-47cb-8a79-b0ca3b949cd2")
public class NotificationSettings implements Serializable {

	private static final long serialVersionUID = -5358377735009587648L;

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "user"))
	@Id
	//@GeneratedValue(generator = "generator")
	@Column(name = "notificationSettingsID", unique = true, nullable = false)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private User user;
	
	//@objid ("3e107b18-58e4-4f8a-bffb-b2f64685a23f")
    private boolean mailEnabled;

    //@objid ("2c86af37-6795-4704-9865-6d07a7c9cdc1")
    private boolean webEnabled;

    //@objid ("7ec652f2-864c-4b0e-93be-81da5628ee90")
    private boolean mobileEnabled;

    @ManyToMany(fetch = FetchType.LAZY)	
    //@objid ("523c9f3b-763f-4360-b396-be8b02988ade")
    private Set<DealCategory> dealCategories = new HashSet<DealCategory> ();

	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isMailEnabled() {
		return mailEnabled;
	}

	public void setMailEnabled(boolean mailEnabled) {
		this.mailEnabled = mailEnabled;
	}

	public boolean isWebEnabled() {
		return webEnabled;
	}

	public void setWebEnabled(boolean webEnabled) {
		this.webEnabled = webEnabled;
	}

	public boolean isMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(boolean mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}

	public Set<DealCategory> getDealCategories() {
		return dealCategories;
	}

	public void setDealCategories(Set<DealCategory> dealCategories) {
		this.dealCategories = dealCategories;
	}

	@Override
	public String toString() {
		return "NotificationSettings [id=" + id + ", user=" + user
				+ ", mailEnabled=" + mailEnabled + ", webEnabled=" + webEnabled
				+ ", mobileEnabled=" + mobileEnabled + ", dealCategories="
				+ dealCategories + "]";
	}

}
