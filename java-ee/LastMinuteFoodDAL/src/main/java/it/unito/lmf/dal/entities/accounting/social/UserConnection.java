package it.unito.lmf.dal.entities.accounting.social;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Automatic JPA Table creation for Spring Social UserConnection Repository.<br/>
 * SQL:
 * 
 * <pre>
 * create table UserConnection (userId varchar(255) not null,
 *     providerId varchar(255) not null,
 *     providerUserId varchar(255),
 *     rank int not null,
 *     displayName varchar(255),
 *     profileUrl varchar(512),
 *     imageUrl varchar(512),
 *     accessToken varchar(255) not null,
 *     secret varchar(255),
 *     refreshToken varchar(255),
 *     expireTime bigint,
 *     primary key (userId, providerId, providerUserId));
 * create unique index UserConnectionRank on UserConnection(userId, providerId, rank);
 * </pre>
 * 
 * @author federico
 *
 */

@Entity
@Table(name = "UserConnection", uniqueConstraints = @UniqueConstraint(name = "UserConnectionRank", columnNames = {
		"userId", "providerId", "rank" }))
public class UserConnection implements Serializable {

	private static final long serialVersionUID = -591357577710239705L;

	@EmbeddedId
	private UserConnectionId id;

	@Column(nullable = false)
	private int rank;

	@Column(nullable = true, length = 255)
	private String displayName;

	@Column(name = "profileUrl", nullable = true, length = 512)
	private String profileUrl;

	@Column(name = "imageUrl", nullable = true, length = 512)
	private String imageUrl;

	@Column(nullable = false, length = 255)
	private String accessToken;

	@Column(nullable = true, length = 255)
	private String secret;

	@Column(nullable = true, length = 255)
	private String refreshToken;

	@Column(nullable = true)
	private long expireTime;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accessToken == null) ? 0 : accessToken.hashCode());
		result = prime * result
				+ ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + (int) (expireTime ^ (expireTime >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((imageUrl == null) ? 0 : imageUrl.hashCode());
		result = prime * result
				+ ((profileUrl == null) ? 0 : profileUrl.hashCode());
		result = prime * result + rank;
		result = prime * result
				+ ((refreshToken == null) ? 0 : refreshToken.hashCode());
		result = prime * result + ((secret == null) ? 0 : secret.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserConnection other = (UserConnection) obj;
		if (accessToken == null) {
			if (other.accessToken != null)
				return false;
		} else if (!accessToken.equals(other.accessToken))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (expireTime != other.expireTime)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		if (profileUrl == null) {
			if (other.profileUrl != null)
				return false;
		} else if (!profileUrl.equals(other.profileUrl))
			return false;
		if (rank != other.rank)
			return false;
		if (refreshToken == null) {
			if (other.refreshToken != null)
				return false;
		} else if (!refreshToken.equals(other.refreshToken))
			return false;
		if (secret == null) {
			if (other.secret != null)
				return false;
		} else if (!secret.equals(other.secret))
			return false;
		return true;
	}

	public UserConnectionId getId() {
		return id;
	}

	public void setId(UserConnectionId id) {
		this.id = id;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
