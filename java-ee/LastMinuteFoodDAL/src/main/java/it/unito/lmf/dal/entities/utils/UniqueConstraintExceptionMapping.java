package it.unito.lmf.dal.entities.utils;

import it.unito.lmf.dal.entities.ConstraintViolationException;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Can be used only in entity classes, used to map a UniqueConstraint to the
 * related Exception in case of violation.
 * 
 * @author Biava Lorenzo
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface UniqueConstraintExceptionMapping {
	public Class<? extends ConstraintViolationException> exception() default ConstraintViolationException.class;

	public String constraintName();
}
