package it.unito.lmf.dal.entities.feedback;

import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.shop.Shop;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="Feedback")
//@objid ("9d2d91cd-e45e-478f-9614-db0ca4dbd415")
public class Feedback  implements Serializable {

	private static final long serialVersionUID = -8543225009244942206L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "feedbackID", unique = true, nullable = false)
    //@objid ("a79cf6e6-6828-4128-b175-5beebc633793")
    private long id;

    //@objid ("ad876ccd-df64-45fd-afa4-e7fd1fdbe63a")
    private String message;

    //@objid ("03d62743-1fb3-4a75-bcfd-bb7138acf572")
    private int value;

    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name = "createdAt", nullable = false, length = 19)
    //@objid ("4c3c651e-842f-44ba-84c6-5b657d9a2820")
    private Date createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", nullable = false)
    //@objid ("3380e563-1b3c-48e4-a727-7e75aa1f3f2f")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopID", nullable = false)
    //@objid ("86773f6b-5b07-48b1-987c-fb9b6cf6c300")
    private Shop shop;

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
	}
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@Override
	public String toString() {
		return "Feedback [id=" + id + ", message=" + message + ", value="
				+ value + ", createdAt=" + createdAt + ", user=" + user
				+ ", shop=" + shop + "]";
	}
  
}
