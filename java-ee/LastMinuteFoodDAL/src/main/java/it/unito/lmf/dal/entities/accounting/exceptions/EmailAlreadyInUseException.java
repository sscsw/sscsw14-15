package it.unito.lmf.dal.entities.accounting.exceptions;

import it.unito.lmf.dal.entities.ConstraintViolationException;
import it.unito.lmf.dal.entities.utils.SQLConstraintViolationMeta;

public class EmailAlreadyInUseException extends ConstraintViolationException {

	private static final long serialVersionUID = -4358962393252053322L;

	private String email;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EmailAlreadyInUseException(String email) {
		this("", new SQLConstraintViolationMeta("",email), null);
	}

	public EmailAlreadyInUseException(String message,
			SQLConstraintViolationMeta sqlConstraintViolationMeta,
			Throwable cause) {
		super("The email '" + sqlConstraintViolationMeta.getConstraintValue() + "' is already in use.", sqlConstraintViolationMeta, cause);
		
		this.email = sqlConstraintViolationMeta.getConstraintValue();
	}	
	
}
