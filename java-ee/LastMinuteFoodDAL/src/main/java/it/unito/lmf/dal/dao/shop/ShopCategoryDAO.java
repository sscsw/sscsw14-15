package it.unito.lmf.dal.dao.shop;

import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.shop.ShopCategory;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class ShopCategoryDAO extends QueryDSLAbstractGenericDAO<ShopCategory> {

	public ShopCategoryDAO() {
		super(ShopCategory.class);
	}

}
