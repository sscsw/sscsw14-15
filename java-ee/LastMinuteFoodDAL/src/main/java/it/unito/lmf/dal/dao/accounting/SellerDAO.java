package it.unito.lmf.dal.dao.accounting;

import static it.unito.lmf.dal.entities.accounting.QSeller.seller;

import java.util.ArrayList;
import java.util.Collection;

import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.shop.Shop;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class SellerDAO extends QueryDSLAbstractGenericDAO<Seller> {

	@PersistenceContext
	protected EntityManager entityManager;

	public SellerDAO() {
		super(Seller.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public Collection<Seller> findAllPending() {
		Iterable<Seller> sellers = findAll(seller.isAuthorized.eq(false));
		
		if (sellers instanceof ArrayList<?>)
			return (ArrayList<Seller>)sellers;
		
		ArrayList<Seller> retSellers = new ArrayList<Seller>();
		for (Seller s : sellers) {
			retSellers.add(s);
		}
		
		return retSellers;
	}

}
