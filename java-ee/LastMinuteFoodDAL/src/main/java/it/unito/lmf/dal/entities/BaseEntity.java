package it.unito.lmf.dal.entities;

import java.io.Serializable;

/**
 * Basic entity implementation that MUST be implement by every entity. This
 * supplies somo utility functionalities.
 * 
 * @author Biava Lorenzo
 *
 */
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = -7966141699332491768L;

}
