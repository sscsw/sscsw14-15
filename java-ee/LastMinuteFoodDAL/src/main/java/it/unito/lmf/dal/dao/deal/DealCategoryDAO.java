package it.unito.lmf.dal.dao.deal;

import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.deal.DealCategory;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class DealCategoryDAO extends QueryDSLAbstractGenericDAO<DealCategory> {

	@PersistenceContext
	protected EntityManager entityManager;

	public DealCategoryDAO() {
		super(DealCategory.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
