package it.unito.lmf.dal.entities.notification;

import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.deal.Deal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="DealNotification")
//@objid ("04acca7e-d88d-4dca-a790-38f8dba5b1d2")
public class DealNotification implements Serializable {

	public enum VisibilityType {
		WEB, MOBILE, EMAIL
	}
	
	private static final long serialVersionUID = -957523242024510606L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dealNotificationID", unique = true, nullable = false)
	//@objid ("c9c57eb7-b324-4a9c-80a4-39c7d225e3a1")
    private Long id;

    //@objid ("df78fe3d-6a85-47ed-bdae-224e9cf9dc75")
    private String event;

    //@objid ("1cd876ec-389f-4782-95f5-effee048acd9")
    private boolean read;

    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name = "createdAt", nullable = false, length = 19)
    //@objid ("75ed5091-8f86-48d8-93ff-fcf10d612725")
    private Date createdAt;

    //TODO: Check if correct
    /**
     * Represents the visibility of the notification.
     * It MUST be a CSV containing the {@link VisibilityType} strings.
     */
    private String visibility;
    
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dealID", nullable = false)
    private Deal deal;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", nullable = false)
    private User user;
    
	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
	}
	
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Deal getDeal() {
		return deal;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}
	
	/**
     * Represents the visibility of the notification.
     * It MUST be a CSV containing the {@link VisibilityType} strings.
     */
	public String getVisibility() {
		return visibility;
	}

	/**
     * Represents the visibility of the notification.
     * It MUST be a CSV containing the {@link VisibilityType} strings.
     */
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	@Override
	public String toString() {
		return "DealNotification [id=" + id + ", event=" + event + ", read="
				+ read + ", createdAt=" + createdAt + ", deal=" + deal
				+ ", user=" + user + "]";
	}

}
