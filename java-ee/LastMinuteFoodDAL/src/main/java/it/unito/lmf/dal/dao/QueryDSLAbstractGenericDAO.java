package it.unito.lmf.dal.dao;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.querydsl.QSort;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

public abstract class QueryDSLAbstractGenericDAO<EntityClass> extends
		AbstractGenericDAO<EntityClass> implements GenericDAO<EntityClass>,
		QueryDslPredicateExecutor<EntityClass> {

	private PathBuilder<EntityClass> qObject;
	private PathBuilder<EntityClass> builder;
	private EntityPath<EntityClass> path;

	/**
	 * Constructor which assign the passed EntityClass to the GenericDAO.
	 * 
	 * @param entityClass
	 *            The assigned Class<EntityClass>.
	 */
	public QueryDSLAbstractGenericDAO(Class<EntityClass> entityClass) {
		super(entityClass);
		// this.builder = new PathBuilder<EntityClass>(qObject.getType(),
		// qObject.getMetadata());
	}

	@PostConstruct
	private void initQueryDSL() {
		qObject = new PathBuilder<EntityClass>(super.getEntityClass(), /*"q"
				+*/ super.getEntityClassSimpleName());
		//path = qObject;
		path = org.springframework.data.querydsl.SimpleEntityPathResolver.INSTANCE.createPath(entityClass);
		builder=new PathBuilder<EntityClass>(path.getType(), path.getMetadata());
	}

	@Override
	public Collection<EntityClass> findAll() {
		assert (qObject != null);
		List<EntityClass> result = prepareJPAQuery().from(qObject)
				.list(qObject);
		return result;
	}

	public long count() {
		assert (qObject != null);
		return prepareJPAQuery().from(qObject).count();
	}

	protected JPAQuery prepareJPAQuery() {
		return new JPAQuery(getEntityManager());
	}

	protected PathBuilder<EntityClass> getQ() {
		assert (qObject != null);
		return qObject;
	}

	/*
	 * {@link QueryDslPredicateExecutor} implementation
	 * 
	 * Adapted from:
	 * https://github.com/spring-projects/spring-data-jpa/blob/master/src/main
	 * /java/org/springframework/data/jpa/repository/support/
	 * QueryDslJpaRepository.java
	 * 
	 * TODO: Check if we could just integrate Spring Data JPA
	 */

	// public Page<EntityClass> findAll(Predicate predicate, Pageable pageable)
	// {
	//
	// JPQLQuery countQuery = createQuery(predicate);
	// JPQLQuery query = applyPagination(createQuery(predicate), pageable);
	//
	// return new PageImpl<EntityClass>(query.list(path), pageable,
	// countQuery.count());
	// }

	@Override
	public Page<EntityClass> findAll(Predicate predicate, Pageable pageable) {

		// JPAQuery countQuery = createQuery(predicate);
		JPAQuery query = applyPagination(createQuery(predicate), pageable);

		// Long total = countQuery.count();
		// List<EntityClass> content = total > pageable.getOffset() ?
		// query.list(path) : Collections.<EntityClass> emptyList();
		// return new PageImpl<EntityClass>(content, pageable, total);

		SearchResults<EntityClass> results = query.listResults(path);

		return new PageImpl<EntityClass>(results.getResults(), pageable,
				results.getTotal());
	}

	@Override
	public EntityClass findOne(Predicate predicate) {
		return createQuery(predicate).uniqueResult(path);
	}

	@Override
	public Iterable<EntityClass> findAll(Predicate predicate) {
		return createQuery(predicate).list(path);
	}

	@Override
	public Iterable<EntityClass> findAll(Predicate predicate,
			OrderSpecifier<?>... orders) {
		return executeSorted(createQuery(predicate), orders);
	}

	@Override
	public long count(Predicate predicate) {
		return createQuery(predicate).count();
	}

	/** QueryDSL helper methods **/

	protected JPAQuery createQuery(Predicate... predicate) {
		return prepareJPAQuery().from(path).where(predicate);
	}

	/**
	 * Executes the given {@link JPQLQuery} after applying the given
	 * {@link OrderSpecifier}s.
	 * 
	 * @param query
	 *            must not be {@literal null}.
	 * @param orders
	 *            must not be {@literal null}.
	 * @return
	 */
	private List<EntityClass> executeSorted(JPAQuery query,
			OrderSpecifier<?>... orders) {
		return applySorting(query, new QSort(orders)).list(path);
	}

	protected JPAQuery applyPagination(JPAQuery query, Pageable pageable) {

		if (pageable == null) {
			return query;
		}

		query.offset(pageable.getOffset());
		query.limit(pageable.getPageSize());

		return applySorting(query, pageable.getSort());
	}

	protected JPAQuery applySorting(JPAQuery query, Sort sort) {

		if (sort == null) {
			return query;
		}

		for (Order order : sort) {
			query.orderBy(toOrder(order));
		}

		return query;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected OrderSpecifier<?> toOrder(Order order) {

		Expression<Object> property = builder.get(order.getProperty());

		return new OrderSpecifier(
				order.isAscending() ? com.mysema.query.types.Order.ASC
						: com.mysema.query.types.Order.DESC, property);
	}

}
