package it.unito.lmf.dal.entities.accounting.exceptions;

import it.unito.lmf.dal.entities.ConstraintViolationException;
import it.unito.lmf.dal.entities.utils.SQLConstraintViolationMeta;

public class UsernameAlreadyInUseException extends ConstraintViolationException {

	private static final long serialVersionUID = -2356583871114270811L;

	private String username;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UsernameAlreadyInUseException(String username) {
		this("", new SQLConstraintViolationMeta("", username), null);
	}

	public UsernameAlreadyInUseException(String message,
			SQLConstraintViolationMeta sqlConstraintViolationMeta,
			Throwable cause) {
		super("The username '"
				+ sqlConstraintViolationMeta.getConstraintValue()
				+ "' is already in use.", sqlConstraintViolationMeta, cause);
		
		this.username = sqlConstraintViolationMeta.getConstraintValue();

	}

}