package it.unito.lmf.dal.dao.notification;

import static it.unito.lmf.dal.entities.notification.QAdministratorNotification.administratorNotification;
import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.notification.AdministratorNotification.NotificationType;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class AdministratorNotificationDAO extends QueryDSLAbstractGenericDAO<AdministratorNotification> {

	@PersistenceContext
	protected EntityManager entityManager;

	public AdministratorNotificationDAO() {
		super(AdministratorNotification.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	/**
	 * 
	 * @return a collection of {@link AdministratorNotification}, which are still undone.
	 */
	public Collection<AdministratorNotification> findAllPending() {
		JPAQuery q = super.prepareJPAQuery().from(administratorNotification)
				.where(administratorNotification.done.eq(false));
		return q.list(administratorNotification);
	}
	
	public AdministratorNotification findByTypeAndObjectId(NotificationType type, Long objectId) {
		BooleanExpression p = administratorNotification.type.eq(type.toString());
		
		switch (type) {
		case NEW_SHOP_REQUEST:
			p = p.and(administratorNotification.shop.id.eq(objectId));
			break;

		case SELLER_AUTHORIZATION_REQUEST:
			p = p.and(administratorNotification.seller.id.eq(objectId));
			break;
			
		default:
			throw new IllegalArgumentException("Invalid Argument " + type.toString() + " -- " + objectId);
		}

		return findOne(p);
	}

}
