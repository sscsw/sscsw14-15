package it.unito.lmf.dal.entities;

import it.unito.lmf.dal.entities.utils.SQLConstraintViolationMeta;

/**
 * Every subclass MUST have a constructor like
 * {@link ConstraintViolationException#ConstraintViolationException(String, SQLConstraintViolationMeta, Throwable)}
 * .
 * 
 * @author Biava Lorenzo
 *
 */
public class ConstraintViolationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private SQLConstraintViolationMeta sqlConstraintViolationMeta;

	public ConstraintViolationException(String message,
			SQLConstraintViolationMeta sqlConstraintViolationMeta,
			Throwable cause) {
		super(message, cause);
		this.sqlConstraintViolationMeta = sqlConstraintViolationMeta;
	}

	public SQLConstraintViolationMeta getSqlConstraintViolationMeta() {
		return sqlConstraintViolationMeta;
	}

}
