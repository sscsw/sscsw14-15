package it.unito.lmf.dal.entities.utils;

public class SQLConstraintViolationMeta {
	String constraintName;
	String constraintValue;

	public SQLConstraintViolationMeta() {
		super();
	}

	public SQLConstraintViolationMeta(String constraintName,
			String constraintValue) {
		super();
		this.constraintName = constraintName;
		this.constraintValue = constraintValue;
	}

	public String getConstraintName() {
		return constraintName;
	}

	public void setConstraintName(String constraintName) {
		this.constraintName = constraintName;
	}

	public String getConstraintValue() {
		return constraintValue;
	}

	public void setConstraintValue(String constraintValue) {
		this.constraintValue = constraintValue;
	}
}
