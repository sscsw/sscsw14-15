package it.unito.lmf.dal.entities.accounting;

import it.unito.lmf.dal.entities.notification.AdministratorNotification;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

//@objid ("8924d459-6f87-4cee-80a4-904721039132")
public class Administrator extends User  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5843679583585705357L;
	//@objid ("d3c7a414-6565-424d-8157-6ea7e37e7266")
    private Set<AdministratorNotification> administratorNotifications = new HashSet<AdministratorNotification> ();
    
}
