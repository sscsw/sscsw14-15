package it.unito.lmf.dal.entities.deal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name="DealCategory")
//@objid ("b447bb85-a470-4666-9f16-5062723f1d67")
public class DealCategory  implements Serializable {
	
	private static final long serialVersionUID = 2097765242672813473L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dealCategoryID", unique = true, nullable = false)
    //@objid ("adfba868-12b4-471c-8a33-3748569967d3")
    private long id;

    //@objid ("7029f892-0b15-4678-a1dc-2324a8a966ba")
    private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "DealCategory [id=" + id + ", name=" + name + "]";
	}

}
