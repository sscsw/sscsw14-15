package it.unito.lmf.dal.utils;

import it.unito.lmf.dal.entities.ConstraintViolationException;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.accounting.exceptions.EmailAlreadyInUseException;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.dal.entities.utils.SQLConstraintViolationMeta;
import it.unito.lmf.dal.entities.utils.UniqueConstraintExceptionMapping;
import it.unito.lmf.dal.entities.utils.UniqueConstraintMappings;

import java.lang.reflect.Constructor;
import java.sql.SQLException;

public class ExceptionManagementHelper {

	private final static String SQL_PARSE_CSTR_VIOL_VALUE_S = "entry '";
	private final static String SQL_PARSE_CSTR_VIOL_VALUE_E = "' for key";
	private final static String SQL_PARSE_CSTR_VIOL_NAME_S = "for key '";
	private final static String SQL_PARSE_CSTR_VIOL_NAME_E = "'";
	
	private static SQLConstraintViolationMeta parseSQLConstraintViolationException(
			SQLException ivcEx) {
		try {
			// Example: Duplicate entry 'value' for key 'name'
			String msg = ivcEx.getMessage();
			String value = msg.substring(
					msg.indexOf(SQL_PARSE_CSTR_VIOL_VALUE_S)
							+ SQL_PARSE_CSTR_VIOL_VALUE_S.length(),
					msg.indexOf(SQL_PARSE_CSTR_VIOL_VALUE_E));
			String name = msg.substring(msg.indexOf(SQL_PARSE_CSTR_VIOL_NAME_S)
					+ SQL_PARSE_CSTR_VIOL_NAME_S.length(), msg.length() - 1);
			return new SQLConstraintViolationMeta(name, value);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void handleSQLConstraintViolationException(SQLException ex) throws EmailAlreadyInUseException, UsernameAlreadyInUseException {
		SQLConstraintViolationMeta cvm = parseSQLConstraintViolationException(ex);
		if (cvm != null) {
			UniqueConstraintMappings annotation = User.class.getAnnotation(UniqueConstraintMappings.class);
			if(annotation!=null) {
				for(UniqueConstraintExceptionMapping item : annotation.value()) {
					if(item.constraintName().equals(cvm.getConstraintName())) {
						ConstraintViolationException exception = null;
						try {
							Constructor<?> c = Class.forName(
									item.exception().getName())
									.getConstructor(String.class,
											SQLConstraintViolationMeta.class,
											Throwable.class);
							exception = (ConstraintViolationException) c
									.newInstance("A constraint violation happened",
											cvm, ex);
						} catch (Exception e) {
						}
						if (exception != null)
							throw exception;
					}
				}
			}
			
			User.UniqueConstraints cstr = null;
			try {
				cstr = User.UniqueConstraints.valueOf(cvm.getConstraintName());
			} catch (Exception e) {
			}
			if (cstr != null) {
				switch (cstr) {
				case UNIQUE_email:
					throw new EmailAlreadyInUseException(
							cvm.getConstraintValue());	

				case UNIQUE_username:
					throw new UsernameAlreadyInUseException(
							cvm.getConstraintValue());
				}
			}
		}
	}
}
