package it.unito.lmf.dal.dao;

import java.util.Collection;

/**
 * Generic CRUD DAO interface definition.
 * 
 * @author Biava Lorenzo
 *
 * @param <T>
 *            the type of the entity.
 */
public interface GenericDAO<T> {

	/**
	 * Method which creates the provided entity.
	 * 
	 * @param entity
	 *            The <i>entity</i> to be inserted.
	 * @return returns the inserted <i>entity</i>. Use the returned instance for
	 *         further operations.
	 */
	public T create(T entity);

	/**
	 * Method which updates the provided <i>entity</i>.
	 * 
	 * @param entity
	 *            The updated <i>entity</i>.
	 * @return the updated <i>entity</i>. Use the returned instance for further
	 *         operations.
	 */
	public T update(T entity);

	/**
	 * Method which deletes the given <i>entity</i>.
	 * 
	 * @param entity
	 *            The <i>entity</i> to be deleted.
	 */
	// public void delete(T entity);

	/**
	 * Method which deletes the <i>entity</i> identified by <i>id</i>.
	 * 
	 * @param id
	 *            The <i>id</i> of the <i>entity</i> to be deleted.
	 * @return <tt>true</tt> if the entity existed before deletion,
	 *         <tt>false</tt> otherwise.
	 */
	public boolean delete(Object id);

	/**
	 * Method which finds a <i>entity</i> by the provided <i>id</i>.
	 * 
	 * @param id
	 *            the <i>id</i> of the <i>entity</i> to be found.
	 * @return the searched <i>entity</i> if exists, <tt>null</tt> otherwise.
	 */
	public T find(Object id);

	/**
	 * Looks for all the entities available.
	 * 
	 * @return the {@link Collection} of all the entities. The collection is
	 *         NEVER <tt>null</tt>. It might be empty if the are no entities.
	 */
	public Collection<T> findAll();

	/**
	 * 
	 * @return the total count of the entities.
	 */
	public long count();

	/**
	 * @return <tt>true</tt> if the entity exists, <tt>false</tt> otherwise.
	 */
	public boolean exists(final Object id);

	// Utilities

	/**
	 * Call this method to flush the EntityManager.
	 */
	public void forceFlush();

}
