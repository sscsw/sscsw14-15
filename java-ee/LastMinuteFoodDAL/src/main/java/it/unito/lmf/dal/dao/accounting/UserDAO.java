package it.unito.lmf.dal.dao.accounting;

import static it.unito.lmf.dal.entities.accounting.QUser.user;
import it.unito.lmf.dal.dao.QueryDSLAbstractGenericDAO;
import it.unito.lmf.dal.entities.ConstraintViolationException;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.accounting.exceptions.EmailAlreadyInUseException;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.dal.entities.utils.SQLConstraintViolationMeta;
import it.unito.lmf.dal.entities.utils.UniqueConstraintExceptionMapping;
import it.unito.lmf.dal.entities.utils.UniqueConstraintMappings;
import it.unito.lmf.dal.utils.ExceptionManagementHelper;

import java.lang.reflect.Constructor;
import java.sql.SQLException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import com.mysema.query.jpa.impl.JPAQuery;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
@LocalBean
public class UserDAO extends QueryDSLAbstractGenericDAO<User> {

	@PersistenceContext
	protected EntityManager entityManager;

	public UserDAO() {
		super(User.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	public User findByEmail(String email) {
		if (email == null)
			throw new NullPointerException("Email cannot be null.");

		JPAQuery q = super.prepareJPAQuery().from(user)
				.where(user.email.eq(email));

		return q.singleResult(user);
	}

	@Override
	public User create(User user) {
		try {
			return super.create(user);
		} catch (PersistenceException pe) {
			Throwable ex = pe.getCause();
			while (ex != null) {
				if (ex instanceof SQLException) {
					ExceptionManagementHelper.handleSQLConstraintViolationException((SQLException) ex);
				}
				ex = ex.getCause();
			}
			throw pe;
		}
		// } catch (Exception e) {
		// throw new RuntimeException("", e);
		// }

	}
}
