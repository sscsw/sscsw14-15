package it.unito.lmf.dal.entities.deal;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.dal.entities.shop.Shop;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

@Entity
@Table(name = "Deal")
// @objid ("4b0305e7-c9db-4139-bbab-2be525c61b89")
public class Deal implements Serializable {

	private static final long serialVersionUID = -62557519052790110L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dealID", unique = true, nullable = false)
	// @objid ("1f077a60-b150-4373-b219-db9dda1b735f")
	private long id;

	private String name;

	// @objid ("51040e68-b9c4-4edd-8b4a-e4d15327f912")
	private String description;

	// @objid ("a9f4e53b-b9b1-4355-8ea2-cf7f2293a5ab")
	private Double price;

	/**
	 * Example: €/Kg -> currency=€, priceUnit=/Kg
	 */
	private String currency;

	private String priceUnit;

	// @objid ("63d998c3-ce68-4cef-8411-23b7a11e1160")
	private String image;

	// @objid ("b090b6ce-6af8-42bd-a168-d9a214a3e26b")
	private Double quantity;

	private String quantityUnit;

	// @objid ("d2a43fe3-ccda-422f-b8a9-76a78da6e060")
	private String discountReason;

	// @objid ("a1319185-9d5b-4eab-96dc-641dafd259bc")
	private Double discountPercentage;

	// @objid ("0b27d1e7-cac2-4634-8cda-d506a64a4989")
	private Double startingPrice;

	// @objid ("de77f106-4158-459f-9e68-488bba54bc87")
	@Column(nullable = true)
	private Date expireDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "shopID", nullable = false)
	private Shop shop;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdAt", nullable = false, length = 19)
	// @objid ("9841509a-8ff6-4aa2-a0ce-aace78a92509")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedAt", nullable = false, length = 19)
	// @objid ("bf0a09eb-9492-4495-ab87-ee85baabf7a5")
	private Date modifiedAt;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "deal")
	// @objid ("d7285ec7-69db-4312-a627-86c0b43c274d")
	private Set<DealNotification> dealNotifications = new HashSet<DealNotification>();

	@ManyToMany(fetch = FetchType.EAGER)
	// @objid ("475ad709-dfc4-4e1f-9e77-6b8d9306dffe")
	private Set<DealCategory> categories = new HashSet<DealCategory>();

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
		this.modifiedAt = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		this.modifiedAt = new Date();
	}

	public Seller getSeller() {
		return shop.getOwner();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public Shop getShop() {
		return shop;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getDiscountReason() {
		return discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(Double startingPrice) {
		this.startingPrice = startingPrice;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Set<DealNotification> getDealNotifications() {
		return dealNotifications;
	}

	public void setDealNotifications(Set<DealNotification> dealNotifications) {
		this.dealNotifications = dealNotifications;
	}

	public Set<DealCategory> getCategories() {
		return categories;
	}

	public void setCategories(Set<DealCategory> categories) {
		this.categories = categories;
	}

	@Override
	public String toString() {
		return "Deal [id=" + id + ", name=" + name + ", description="
				+ description + ", price=" + price + ", image=" + image
				+ ", quantity=" + quantity + ", discountReason="
				+ discountReason + ", discountPercentage=" + discountPercentage
				+ ", startingPrice=" + startingPrice + ", expireDate="
				+ expireDate + ", shop=" + shop + ", createdAt=" + createdAt
				+ ", modifiedAt=" + modifiedAt + ", dealNotifications="
				+ dealNotifications + ", categories=" + categories + "]";
	}
}
