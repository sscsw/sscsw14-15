<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<!-- This pom builds the EAR artifact and includes the ejb and web modules. -->

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>lmf</artifactId>
		<groupId>it.unito.lmf</groupId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<artifactId>lmf-ee-ear</artifactId>
	<packaging>ear</packaging>

	<name>Last Minute Food - J2EE Application - EAR</name>
	<description>Last Minute Food - J2EE Application - EAR

This module gathers all the modules componing the application. This is the one which must be used for deployment.</description>

	<properties>
		<javaee.version>6.0</javaee.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-framework-bom</artifactId>
				<version>${version.spring}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- Dependencies on the ejb and web modules so that they can be found 
			by the ear plugin -->
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-ws</artifactId>
			<type>war</type>
		</dependency>
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-web</artifactId>
			<type>war</type>
		</dependency>
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-ejb</artifactId>
			<type>ejb</type>
		</dependency>
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-dal</artifactId>
			<type>ejb</type>
		</dependency>
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-domain</artifactId>
			<type>jar</type>
		</dependency>
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-utility</artifactId>
			<type>jar</type>
		</dependency>

		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>6.0</version>
			<scope>provided</scope>
		</dependency>


		<!-- TODO try to move this in dependency management and include only in 
			required modules -->
		<!-- Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
		</dependency>

		<!-- Spring Security -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${version.spring-security}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${version.spring-security}</version>
		</dependency>

		<!-- Spring for pagination facilities -->
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-commons</artifactId>
			<version>${version.spring-data}</version>
		</dependency>

		<!-- Template Engine - Thymeleaf -->
		<dependency>
			<groupId>org.thymeleaf</groupId>
			<artifactId>thymeleaf-spring4</artifactId>
			<version>${version.thymeleaf}</version>
		</dependency>

		<dependency>
			<groupId>nz.net.ultraq.thymeleaf</groupId>
			<artifactId>thymeleaf-layout-dialect</artifactId>
			<version>1.2.1</version>
		</dependency>

		<!-- Thymeleaf - Spring Security Dialect -->
		<dependency>
			<groupId>org.thymeleaf.extras</groupId>
			<artifactId>thymeleaf-extras-springsecurity3</artifactId>
			<version>2.1.1.RELEASE</version>
		</dependency>

		<!-- Utility Libraries -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.3.2</version>
		</dependency>

		<!-- Apache Commons FileUpload -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.1</version>
		</dependency>

		<!-- Apache Commons IO -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>
	</dependencies>

	<build>
		<finalName>${project.artifactId}</finalName>
		<pluginManagement>
			<plugins>
				<!-- To deploy jar with log4j configs -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<version>2.3</version>
					<configuration>
						<descriptor>${project.basedir}/src/main/resources/assembly.xml</descriptor>
						<appendAssemblyId>false</appendAssemblyId>
						<finalName>log4j-cfg</finalName>
						<outputDirectory>${project.build.directory}/extra/lib</outputDirectory>
						<debug>true</debug>
					</configuration>
					<executions>
						<execution>
							<id>add-log4j-cfg</id>
							<phase>validate</phase>
							<goals>
								<goal>single</goal>
							</goals>
						</execution>
					</executions>
				</plugin>

				<!-- Ear plugin -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-ear-plugin</artifactId>
					<version>${version.ear.plugin}</version>
					<!-- configuring the ear plugin -->
					<configuration>
						<!-- Tell Maven we are using Java EE 7 -->
						<version>7</version>
						<!-- Use Java EE ear libraries as needed. Java EE ear libraries are 
							in easy way to package any libraries needed in the ear, and automatically 
							have any modules (EJB-JARs and WARs) use them -->
						<defaultLibBundleDir>lib</defaultLibBundleDir>
						<modules>
							<!-- Set a custom context-root -->
							<webModule>
								<groupId>it.unito.lmf</groupId>
								<artifactId>lmf-ee-web</artifactId>
								<contextRoot>/web</contextRoot>
							</webModule>
							<!-- Set a custom context-root -->
							<webModule>
								<groupId>it.unito.lmf</groupId>
								<artifactId>lmf-ee-ws</artifactId>
								<contextRoot>/ws</contextRoot>
							</webModule>
						</modules>
						<earSourceIncludes>${project.build.directory}/extra/**</earSourceIncludes>
						<fileNameMapping>no-version</fileNameMapping>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
</project>