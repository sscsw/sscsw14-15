package it.unito.lmf.utils.mapping;

import java.util.Collection;
import java.util.HashSet;

/**
 * A base implementation of the {@link MappingHelper}.
 * 
 * @author l.biava
 *
 * @param <ENTITY>
 * @param <DSL>
 */
public abstract class MappingHelperBase<ENTITY, DSL> implements
		MappingHelper<ENTITY, DSL> {

	protected Class<ENTITY> entityClass;
	protected Class<DSL> dslClass;

	protected MappingHelperBase() {
	}

	public MappingHelperBase(Class<ENTITY> entityClass, Class<DSL> dslClass) {
		this.entityClass = entityClass;
		this.dslClass = dslClass;
	}

	@Override
	public Class<ENTITY> getEntityClass() {
		return entityClass;
	}

	@Override
	public Class<DSL> getDSLClass() {
		return dslClass;
	}

	@Override
	public Collection<ENTITY> getEntity(Collection<DSL> objs) {
		Collection<ENTITY> dsls = new HashSet<ENTITY>();
		return getEntity(objs, dsls);
	}

	@Override
	public Collection<DSL> getDSL(Collection<ENTITY> objs) {
		Collection<DSL> dsls = new HashSet<DSL>();
		return getDSL(objs, dsls);
	}
	
	@Override
	public Collection<ENTITY> getEntity(Collection<DSL> objs, Collection<ENTITY> collection) {		
		for (DSL obj : objs) {
			collection.add(getEntity(obj));
		}
		return collection;
	}

	@Override
	public Collection<DSL> getDSL(Collection<ENTITY> objs, Collection<DSL> collection) {
		for (ENTITY obj : objs) {
			collection.add(getDSL(obj));
		}
		return collection;
	}
}
