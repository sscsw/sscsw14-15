package it.unito.lmf.utils.validation;

public class RegexpRepository {
	public static final String REGEXP_PARTITA_IVA="^[0-9]{11}$";
	public static final String REGEXP_COD_FISCALE="^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$";
	public static final String REGEXP_PHONE_NUMBER="^[0-9]+[0-9]+$";
	public static final String REGEXP_EMAIL="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String PASSWORD_PATTERN = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,20}$";
	/**
	 *  RULES:
	 *  	^				   				# Start of the line
	 *        ([A-Za-z0-9])					# must start with characters and number
	 *  	  +(?:[._-][A-Za-z0-9]+)*$      # Should contains characters and symbols in the list, a-z, A-Z, 0-9, ._-
	 *		$                 				# End of the line
	 */
	public static final String ALPHANUMERIC_DOT_UNDERSCORE_HYPHEN_PATTERN = "^([A-Za-z0-9])+(?:[._-][A-Za-z0-9]+)*$";
																			 
	
	/**
	 *  RULES:
	 *  	^                    # Start of the line
	 *  	  [a-z0-9_-]	     # Must contains characters and symbols in the list, a-z, A-Z, 0-9
	 *      $                    # End of the line
	 */
	public static final String ALPHANUMERIC_PATTERN = "^([A-Za-z0-9]+)*$";
	
	
	public static final String ALPHANUMERIC_WHITESPACE_PATTERN = "^[a-zA-Z0-9][-a-zA-Z0-9 ]+$";
}
