package it.unito.lmf.util.picture;

import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIClient;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIException;

import java.io.InputStream;
import java.util.UUID;

public class PictureRepoHelper {
	
	public static String uploadPicture(InputStream picture, String pictureContentType) throws Exception{
		return uploadPicture(picture, pictureContentType, 3);
	}
		
	public static String uploadPicture(InputStream picture, String pictureContentType, int retryCount) throws Exception{
		boolean retry = true;
		int retries = 0;
		while (retry == true) {
			retry = false;
			retries++;
			try {
				String pictureKey = UUID.randomUUID().toString();
				LMFPictureRepoAPIClient prc = new LMFPictureRepoAPIClient(LMFPictureRepoAPIClient.TOKEN);
				
				return prc.uploadPicture(pictureKey, picture, pictureContentType);
				
			} catch (LMFPictureRepoAPIException e) {
				if (e.getResponseMessage().getHttpStatusCode() == 409)
					if (retries < retryCount)
						retry = true;
					else
						throw new Exception("Cannot upload picture.");
				else
					throw e;
			}
		}
		
		return null;
	}
}
