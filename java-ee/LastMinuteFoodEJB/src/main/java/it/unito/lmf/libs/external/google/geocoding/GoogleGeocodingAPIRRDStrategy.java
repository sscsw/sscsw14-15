package it.unito.lmf.libs.external.google.geocoding;

import it.unito.lmf.domain.dsl.external.google.geocoding.GeocodingResponse;
import it.unito.lmf.util.web.rs.decoding.RestMessage;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecodeStrategy;
import it.unito.lmf.util.web.rs.decoding.exceptions.NoMappingModelFoundException;
import it.unito.lmf.util.web.rs.decoding.exceptions.ServerErrorResponseException;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Generic Rest Decode strategy for the Cloudify 'protocol'.
 * 
 * @author l.biava
 * 
 */
public class GoogleGeocodingAPIRRDStrategy implements
		RestResponseDecodeStrategy {

	@Override
	public JavaType getModelClass(RestMessage msg)
			throws NoMappingModelFoundException, ServerErrorResponseException {
		Pair<StatusType, JavaType> result = strategy(msg);
		return result.getRight();
	}

	@Override
	public StatusType getStatus(RestMessage msg)
			throws NoMappingModelFoundException, ServerErrorResponseException {
		Pair<StatusType, JavaType> result = strategy(msg);
		return result.getLeft();
	}

	private Pair<StatusType, JavaType> strategy(RestMessage msg)
			throws NoMappingModelFoundException, ServerErrorResponseException {
		StatusType status = Status.OK;
		JavaType clazz;
		ObjectMapper mapper = new ObjectMapper();

		int httpStatusCode = msg.getHttpStatusCode();

		if (httpStatusCode == 200) {
			// ResponseWrapper
			clazz = mapper.getTypeFactory().constructType(
					GeocodingResponse.class);
		} else {
			throw new ServerErrorResponseException("SERVER_ERROR_RESPONSE",
					null, msg, httpStatusCode);
		}

		return new ImmutablePair<StatusType, JavaType>(status, clazz);
	}

}