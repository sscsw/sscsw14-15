package it.unito.lmf.biz.notification;

import it.unito.lmf.biz.exceptions.ItemNotFoundException;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;

/**
 * Checked exception thrown when a {@link AdministratorNotification} cannot be found.
 * @author Biava Lorenzo
 *
 */
public class AdministratorNotificationNotFoundException extends ItemNotFoundException {

	private static final long serialVersionUID = -3958251103676652597L;
	
	private static final Class<?> itemClass=User.class;

	public AdministratorNotificationNotFoundException(String fieldName,
			Object fieldValue) {
		super(itemClass, fieldName, fieldValue);
	}

	public AdministratorNotificationNotFoundException(String message,
			Throwable cause) {
		super(itemClass, message, cause);
	}

	public AdministratorNotificationNotFoundException() {
		super(itemClass);
	}

}
