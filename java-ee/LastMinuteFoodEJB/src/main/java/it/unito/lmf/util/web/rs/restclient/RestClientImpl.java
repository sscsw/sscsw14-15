package it.unito.lmf.util.web.rs.restclient;

import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;
import it.unito.lmf.util.web.rs.decoding.RestMessage;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecodeStrategy;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecoder;
import it.unito.lmf.util.web.rs.decoding.exceptions.MappingException;
import it.unito.lmf.util.web.rs.decoding.exceptions.NoMappingModelFoundException;
import it.unito.lmf.util.web.rs.decoding.exceptions.ServerErrorResponseException;
import it.unito.lmf.util.web.rs.restclient.exceptions.RestClientException;

import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.client.JerseyWebTarget;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

public class RestClientImpl<T extends BaseRestResponseResult> extends
		AbstractRestClient<T> {

	/***************************** GET Requests ******************************/

	@Override
	public T getRequest(String URL, MultivaluedMap<String, Object> headers,
			RequestOptions reqOptions, RestResponseDecoder<T> rrd,
			RestResponseDecodeStrategy strategy) throws RestClientException,
			NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		RestMessage msg = doRequest(RestMethod.GET, URL, headers, null, null,
				null, reqOptions);

		return rrd.decode(msg, strategy);
	}

	@Override
	public RestMessage getRequest(String URL,
			MultivaluedMap<String, Object> headers, RequestOptions reqOptions)
			throws RestClientException {

		return doRequest(RestMethod.GET, URL, headers, null, null, null,
				reqOptions);
	}

	/***************************** HEAD Requests ******************************/

	@Override
	public T headRequest(String URL, MultivaluedMap<String, Object> headers,
			RequestOptions reqOptions, RestResponseDecoder<T> rrd,
			RestResponseDecodeStrategy strategy) throws RestClientException,
			NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		RestMessage msg = doRequest(RestMethod.HEAD, URL, headers, null, null,
				null, reqOptions);

		return rrd.decode(msg, strategy);
	}

	@Override
	public RestMessage headRequest(String URL,
			MultivaluedMap<String, Object> headers, RequestOptions reqOptions)
			throws RestClientException {

		return doRequest(RestMethod.HEAD, URL, headers, null, null, null,
				reqOptions);
	}

	/***************************** POST Requests ******************************/

	@Override
	public <E> T postRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		RestMessage msg = doRequest(RestMethod.POST, URL, headers, null, body,
				bodyMediaType, reqOptions);
		return rrd.decode(msg, strategy);
	}

	@Override
	public <E> RestMessage postRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions)
			throws RestClientException {

		return doRequest(RestMethod.POST, URL, headers, null, body,
				bodyMediaType, reqOptions);
	}

	/***************************** PUT Requests ******************************/

	@Override
	public <E> T putRequest(String URL, MultivaluedMap<String, Object> headers,
			GenericEntity<E> body, MediaType bodyMediaType,
			RequestOptions reqOptions, RestResponseDecoder<T> rrd,
			RestResponseDecodeStrategy strategy) throws RestClientException,
			NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		RestMessage msg = doRequest(RestMethod.PUT, URL, headers, null, body,
				bodyMediaType, reqOptions);

		return rrd.decode(msg, strategy);
	}

	@Override
	public <E> RestMessage putRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions)
			throws RestClientException {

		return doRequest(RestMethod.PUT, URL, headers, null, body,
				bodyMediaType, reqOptions);
	}

	/***************************** DELETE Requests ******************************/

	@Override
	public <E> T deleteRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		RestMessage msg = doRequest(RestMethod.DELETE, URL, headers, null,
				body, bodyMediaType, reqOptions);

		return rrd.decode(msg, strategy);
	}

	@Override
	public <E> RestMessage deleteRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions)
			throws RestClientException {

		return doRequest(RestMethod.DELETE, URL, headers, null, body,
				bodyMediaType, reqOptions);
	}

	/***************************** Generic Requests ******************************/

	@Override
	public <E> T doRequest(RestMethod method, String URL,
			MultivaluedMap<String, Object> headers,
			MultivaluedMap<String, Object> queryParams, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		RestMessage msg = doRequest(method, URL, headers, queryParams, body,
				bodyMediaType, reqOptions);

		return rrd.decode(msg, strategy);
	}

	// /***************************** PRIVATE methods
	// ******************************/

	@Override
	public <E> RestMessage doRequest(RestMethod method, String URL,
			MultivaluedMap<String, Object> headers,
			MultivaluedMap<String, Object> queryParams, GenericEntity<E> body,
			MediaType bodyMediaType, RequestOptions reqOptions)
			throws RestClientException {

		JerseyClientBuilder cb = new JerseyClientBuilder();

		// // Socket Timeout
		// if (reqOptions != null && reqOptions.getTimeout() > 0)
		// cb.socketTimeout(reqOptions.getTimeout(), TimeUnit.MILLISECONDS);
		// else
		// cb.socketTimeout(this.defaultTimeout, TimeUnit.MILLISECONDS);
		//
		// // Proxy
		// if (reqOptions != null && reqOptions.getProxy() != null) {
		// cb.defaultProxy(reqOptions.getProxy().getHostname(), reqOptions
		// .getProxy().getPort(), reqOptions.getProxy().getProtocol());
		// } else {
		// if(System.getProperty("java.net.useSystemProxies") != null &&
		// System.getProperty("java.net.useSystemProxies").equals("true"))
		// {
		// cb.defaultProxy("proxy.reply.it",8080,"HTTP");
		// // TODO Fix using system properties
		// // try {
		// // List l=ProxySelector.getDefault().select(new
		// URI("http://foo/bar"));
		// // for (Iterator iter = l.iterator(); iter.hasNext();) {
		// // java.net.Proxy proxy = (java.net.Proxy) iter.next();
		// // System.out.println("proxy hostname : " + proxy.type());
		// //
		// // InetSocketAddress addr = (InetSocketAddress) proxy.address();
		// //
		// // if (addr == null) {
		// // System.out.println("No Proxy");
		// // } else {
		// // System.out.println("proxy hostname : " + addr.getHostName());
		// // System.setProperty("http.proxyHost", addr.getHostName());
		// // System.out.println("proxy port : " + addr.getPort());
		// // System.setProperty("http.proxyPort",
		// Integer.toString(addr.getPort()));
		// //
		// // cb.defaultProxy(addr.getHostName(), addr.getPort());
		// // }
		// // }
		// // } catch (URISyntaxException e) {
		// // // TODO Auto-generated catch block
		// // e.printStackTrace();
		// // }
		// }
		// }

		JerseyClient client = cb.build();
		client.register(MultiPartFeature.class);

		Response response = null;
		try {
			JerseyWebTarget target = client.target(URL);

			// Add Query Params
			if (queryParams != null)
				for (Map.Entry<String, List<Object>> item : queryParams
						.entrySet())
					target = target.queryParam(item.getKey(), item.getValue().get(0));

			Builder reqBuilder = target.request();

			if (headers != null) {
				reqBuilder = reqBuilder.headers(headers);
			}

			switch (method) {
			case GET:
				if (body == null)
					response = reqBuilder.get();
				else
					response = reqBuilder.method("GET",
							Entity.entity(body, bodyMediaType));
				break;
			case HEAD:
				if (body == null)
					response = reqBuilder.head();
				else
					response = reqBuilder.method("HEAD",
							Entity.entity(body, bodyMediaType));
			case POST:
				if (body == null) {
					response = reqBuilder.method("POST");
				} else
					response = reqBuilder.post(Entity.entity(body,
							bodyMediaType));
				break;
			case PUT:
				if (body == null) {
					response = reqBuilder.method("PUT");
				} else {
					response = reqBuilder.put(Entity
							.entity(body, bodyMediaType));
				}
				break;
			case DELETE:
				if (body == null)
					response = reqBuilder.delete();
				else
					response = reqBuilder.method("DELETE",
							Entity.entity(body, bodyMediaType));
				break;
			}

			RestMessage msg = new RestMessage(response.getHeaders(),
					response.readEntity(String.class), response.getStatus());

			return msg;

		} catch (Exception e) {
			throw new RestClientException(e.getMessage(), e);
		} finally {
			if (response != null)
				response.close();
			if (client != null)
				client.close();
		}

	}

}
