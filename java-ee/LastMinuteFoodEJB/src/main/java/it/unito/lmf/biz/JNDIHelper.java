package it.unito.lmf.biz;

/**
 * This class provides facilities for JNDI lookup.
 * @author Biava Lorenzo
 *
 */
public class JNDIHelper {

	//public static final String AppName="LastMinuteFood";
	public static final String AppName="lmf-ee-ear";
	public static final String EJBModuleName="lmf-ee-ejb";
	
	public static String getJNDIName(Class<?> clazz) {
		return "java:global/"+AppName+"/"+EJBModuleName+"/"+clazz.getSimpleName()+"!"+clazz.getCanonicalName();
	}
	
	public static final String EJB_UserManagementBean="java:global/LastMinuteFood/lmf-ee-ejb/UserMgmtBean!it.unito.lmf.biz.accounting.UserMgmtBean";
	
	//public static final String EJB_BasePath="java:global/LastMinuteFood/lmf-ee-ejb/";
	public static final String EJB_BasePath="java:global/"+AppName+"/"+EJBModuleName+"/";
}
