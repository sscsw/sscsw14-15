package it.unito.lmf.util.geocoding;

import it.unito.lmf.domain.dsl.external.google.geocoding.GeocodingResponse;
import it.unito.lmf.domain.dsl.external.google.geocoding.Location;
import it.unito.lmf.libs.external.google.geocoding.GoogleGeocodingAPIClient;
import it.unito.lmf.util.geocoding.exceptions.InvalidAddressException;

public class GeocodingHelper {
	
	public static Location getLocation (String address) {
		try {
			GeocodingResponse geoResponse = new GoogleGeocodingAPIClient("")
					.getLatLng(address);
			if (geoResponse.getResults().size() < 1)
				throw new InvalidAddressException(address);
	
			return geoResponse.getResults().get(0).getGeometry().getLocation();
			
		} catch (Exception e) {
			throw new InvalidAddressException(address);
		}
	}
}
