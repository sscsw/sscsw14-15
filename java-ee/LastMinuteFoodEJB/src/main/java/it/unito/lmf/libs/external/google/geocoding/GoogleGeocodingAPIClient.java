package it.unito.lmf.libs.external.google.geocoding;

import it.unito.lmf.domain.dsl.external.google.geocoding.GeocodingResponse;
import it.unito.lmf.util.web.rs.apiclient.AbstractAPIClient;
import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;
import it.unito.lmf.util.web.rs.decoding.exceptions.MappingException;
import it.unito.lmf.util.web.rs.restclient.RestClient;
import it.unito.lmf.util.web.rs.restclient.RestClientFactory;
import it.unito.lmf.util.web.rs.restclient.RestClient.RestMethod;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

public class GoogleGeocodingAPIClient extends AbstractAPIClient {
	
	public static final String API_KEY="AIzaSyDoCPurE9u-iG6jT7LHHwiMXn4NHwmZF5o";
	// https://maps.googleapis.com/maps/api/geocode/json?address=via+Pessinetto+12,Torino,Italia&key=AIzaSyDoCPurE9u-iG6jT7LHHwiMXn4NHwmZF5o
	// https://maps.googleapis.com/maps/api/geocode/json?latlng=45.0899166,7.6594132&key=AIzaSyDoCPurE9u-iG6jT7LHHwiMXn4NHwmZF5o

	private static final String BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json";
	private String apiKey;

	RestClient<BaseRestResponseResult> client;

	public GoogleGeocodingAPIClient(String apiKey,
			RestClientFactory restClientFactory) {
		super(BASE_URL, restClientFactory);
		this.apiKey = apiKey;
	}

	public GoogleGeocodingAPIClient(String apiKey) {
		super(BASE_URL);
		this.apiKey = apiKey;
	}

	public GeocodingResponse getLatLng(String address)
			throws GoogleGeocodingAPIException, Exception {

		String URL = baseWSUrl;

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();

		MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<String, Object>();
		queryParams.add("address", address);
		//queryParams.add("key", this.apiKey);

		BaseRestResponseResult result = restClient.doRequest(RestMethod.GET,
				URL, headers, queryParams, null, null, null,
				new GoogleGeocodingAPIRRD(), null);

		return handleResult(result);
	}
	
	public GeocodingResponse getAddress(Double latitude, Double longitude)
			throws GoogleGeocodingAPIException, Exception {

		String URL = baseWSUrl;

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();

		MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<String, Object>();
		//queryParams.add("latlng", latitude+","+longitude);
		//queryParams.add("key", this.apiKey);
		URL+="?latlng="+latitude+","+longitude;

		BaseRestResponseResult result = restClient.doRequest(RestMethod.GET,
				URL, headers, queryParams, null, null, null,
				new GoogleGeocodingAPIRRD(), null);

		return handleResult(result);
	}


	protected <RespType> void handleErrorResult(BaseRestResponseResult result)
			throws GoogleGeocodingAPIException {
		GoogleGeocodingAPIError apiError = new GoogleGeocodingAPIError();

		try {
			apiError.setMessage(((GeocodingResponse) result.getResult())
					.getErrorMessage());
			apiError.setMessage(((GeocodingResponse) result.getResult())
					.getStatus());
		} catch (Exception e) {
			throw new RuntimeException(
					"Cannot extract API error informations: " + e.getMessage(),
					e);
		}

		throw new GoogleGeocodingAPIException("API_ERROR", null,
				result.getOriginalRestMessage(), apiError);
	}

	@SuppressWarnings("unchecked")
	protected <RespType> RespType handleSuccessResult(
			BaseRestResponseResult result) throws MappingException {
		try {

			return ((RespType) result.getResult());
		} catch (Exception e) {
			throw new MappingException("Unexpected response type.", null,
					result.getOriginalRestMessage());
		}
	}

	protected <RespType> RespType handleResult(BaseRestResponseResult result)
			throws MappingException, GoogleGeocodingAPIException {
		String status = ((GeocodingResponse) result.getResult()).getStatus();
		
		if (status.equals(GeocodingResponse.StatusCode.OK.toString())||status.equals(GeocodingResponse.StatusCode.ZERO_RESULTS.toString()))
		//if (((GeocodingResponse) result.getResult()).getErrorMessage()==null)
			return handleSuccessResult(result);
		else
			handleErrorResult(result);

		return null;
	}
}
