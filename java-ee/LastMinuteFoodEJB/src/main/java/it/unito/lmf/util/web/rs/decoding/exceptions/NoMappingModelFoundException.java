package it.unito.lmf.util.web.rs.decoding.exceptions;

import it.unito.lmf.util.web.rs.decoding.RestMessage;

/**
 * Exception representing that a Rest Result Decoding Strategy wasn't able to
 * decide what mapping class use for the given Rest message. <br/>
 * The original {@link RestMessage} is embedded for inspection.
 * 
 * @author l.biava
 * 
 */
public class NoMappingModelFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3965412299072282395L;

	private RestMessage responseMessage;

	public RestMessage getResponseMessage() {
		return responseMessage;
	}

	public boolean hasResponseMessage() {
		return responseMessage != null;
	}

	/*
	 * public NoMappingModelFoundException(String msg) { super(msg); }
	 */

	public NoMappingModelFoundException(RestMessage responseMessage) {
		super();
		this.responseMessage = responseMessage;
	}
}
