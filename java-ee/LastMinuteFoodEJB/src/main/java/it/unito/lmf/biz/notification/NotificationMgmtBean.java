package it.unito.lmf.biz.notification;

import it.unito.lmf.biz.JNDIHelper;
import it.unito.lmf.dal.dao.accounting.UserDAO;
import it.unito.lmf.dal.dao.notification.AdministratorNotificationDAO;
import it.unito.lmf.dal.dao.notification.DealNotificationDAO;
import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.dal.entities.notification.AdministratorNotification.NotificationType;
import it.unito.lmf.dal.entities.notification.DealNotification.VisibilityType;
import it.unito.lmf.dal.entities.shop.Shop;

import java.util.Collection;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class UserMgmtBean
 */
@Stateless
@LocalBean
public class NotificationMgmtBean {

	public static final String JNDIName = JNDIHelper.EJB_BasePath
			+ "NotificationMgmtBean!it.unito.lmf.biz.notification.NotificationMgmtBean";

	@EJB
	protected DealNotificationDAO dealNotificationDAO;

	@EJB
	protected AdministratorNotificationDAO administratorNotificationDAO;

	@EJB
	protected UserDAO userDAO;

	/**
	 * Default constructor.
	 */
	public NotificationMgmtBean() {
	}

	public AdministratorNotification createSellerAuthRequestAdminNotification(
			Seller seller) {
		AdministratorNotification an = new AdministratorNotification();
		an.setDone(false);
		an.setMessage("SELLER REQUEST");
		an.setType(AdministratorNotification.NotificationType.SELLER_AUTHORIZATION_REQUEST
				.toString());
		an.setSeller(seller);

		// SEND EMAIL TO ADMINS ?

		return administratorNotificationDAO.create(an);
	}

	public AdministratorNotification createShopAuthRequestAdminNotification(
			Shop shop) {
		AdministratorNotification an = new AdministratorNotification();
		an.setDone(false);
		an.setMessage("SHOP REQUEST");
		an.setType(AdministratorNotification.NotificationType.NEW_SHOP_REQUEST
				.toString());
		an.setShop(shop);

		// SEND EMAIL TO ADMINS ?

		return administratorNotificationDAO.create(an);
	}

	public AdministratorNotification setNotificationDone(
			AdministratorNotification notification) {
		notification.setDone(true);
		return administratorNotificationDAO.update(notification);
	}

	public AdministratorNotification setNotificationDone(Long notificationId) {
		AdministratorNotification notification = administratorNotificationDAO
				.find(notificationId);
		if (notification == null)
			throw new AdministratorNotificationNotFoundException("Id",
					notificationId);

		return setNotificationDone(notification);
	}
	
	public boolean deleteNotification(AdministratorNotification notification) {
		return deleteNotification(notification.getId());
	}
	
	public boolean deleteNotification(Long notificationId) {
		return administratorNotificationDAO.delete(notificationId);
	}
	
	public DealNotification setNotificationRead(
			DealNotification notification, Long userId) {
		if(notification.getUser().getId()!=userId)
			throw new IllegalArgumentException("User is not the owner of the notification");
			
		notification.setRead(true);
		return dealNotificationDAO.update(notification);
	}
	
	public DealNotification setNotificationRead(Long notificationId, Long userId) {
		DealNotification notification = findDealNotification(notificationId, userId);
		return setNotificationRead(notification, userId);
	}
	
	public DealNotification findDealNotification (Long notificationId, Long userId) {
		DealNotification notification = dealNotificationDAO
				.find(notificationId);
		if (notification == null)
			throw new AdministratorNotificationNotFoundException("Id",
					notificationId);

		if(notification.getUser().getId()!=userId)
			throw new IllegalArgumentException("User is not the owner of the notification");
		
		return notification;
	}

	public Collection<AdministratorNotification> getAllAdminNotifications() {
		return administratorNotificationDAO.findAll();
	}

	public Collection<AdministratorNotification> getAllPendingAdminNotifications() {
		return administratorNotificationDAO.findAllPending();
	}

	public AdministratorNotification getAdministratorNotificationById(Long id) {
		return administratorNotificationDAO.find(id);
	}

	public Collection<DealNotification> getAllPendingUserNotifications(
			Long userId, VisibilityType visibility) {
		return dealNotificationDAO.findAllPending(userId, visibility);
	}

	public DealNotification createDealNotification(Deal deal, User user,
			VisibilityType[] visibilities) {
		DealNotification dn = new DealNotification();

		dn.setDeal(deal);
		dn.setEvent("NEW DEAL");
		dn.setRead(false);
		dn.setUser(user);

		String visibility = "";
		for (VisibilityType vis : visibilities)
			visibility += vis.name() + ",";
		visibility = visibility.substring(0, visibility.length() - 1);
		dn.setVisibility(visibility);

		return dealNotificationDAO.create(dn);
	}

	
	public AdministratorNotification getNotificationByTypeAndObjectId(NotificationType type, Long objectId) {
		return administratorNotificationDAO.findByTypeAndObjectId(type, objectId);
	}
	
	
	@Asynchronous
	public void onDealCreated(Deal deal) {
		Collection<User> users = userDAO.findAll();
		for (User user : users) {
			if (!user.isAdmin() && user.getId() != deal.getSeller().getId()) {
				// TODO: if(user) interested
				// TODO: find user's notification settings

				// Web Notification
				createDealNotification(deal, user, VisibilityType.values());
			}
		}
	}
}
