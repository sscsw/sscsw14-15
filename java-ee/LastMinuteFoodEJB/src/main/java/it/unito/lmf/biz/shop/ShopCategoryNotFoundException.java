package it.unito.lmf.biz.shop;

import it.unito.lmf.biz.exceptions.ItemNotFoundException;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.shop.ShopCategory;

/**
 * Checked exception thrown when a {@link ShopCategory} cannot be found.
 * @author Biava Lorenzo
 *
 */
public class ShopCategoryNotFoundException extends ItemNotFoundException {

	private static final long serialVersionUID = -3958251103676652597L;

	private static final Class<?> itemClass=User.class;

	public ShopCategoryNotFoundException(String fieldName,
			Object fieldValue) {
		super(itemClass, fieldName, fieldValue);
	}

	public ShopCategoryNotFoundException(String message,
			Throwable cause) {
		super(itemClass, message, cause);
	}

	public ShopCategoryNotFoundException() {
		super(itemClass);
	}

}
