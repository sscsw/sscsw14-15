package it.unito.lmf.libs.external.google.geocoding;

import it.unito.lmf.util.web.rs.decoding.BaseRestResponseDecoder;
import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;
import it.unito.lmf.util.web.rs.decoding.RestMessage;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecodeStrategy;
import it.unito.lmf.util.web.rs.decoding.exceptions.MappingException;
import it.unito.lmf.util.web.rs.decoding.exceptions.NoMappingModelFoundException;
import it.unito.lmf.util.web.rs.decoding.exceptions.ServerErrorResponseException;

import java.io.IOException;

import javax.ws.rs.core.Response.StatusType;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Basic implementation of a Rest response Decoder. This implementation uses
 * Jackson JSON mapping system.
 * 
 * @author l.biava
 * 
 */
public class GoogleGeocodingAPIRRD extends BaseRestResponseDecoder {

	public GoogleGeocodingAPIRRD() {
		super();
		this.defaultDecodeStrategy = new GoogleGeocodingAPIRRDStrategy();
	}

	/**
	 * <b>Supports only default strategy. MUST pass null in strategy field !</b><br/>
	 * {@inheritDoc}
	 */
	@Override
	public BaseRestResponseResult decode(RestMessage msg,
			RestResponseDecodeStrategy strategy)
			throws NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		if (strategy != null)
			throw new UnsupportedOperationException(
					"Only default decoding strategy can be used with this decoder.");

		return decode(msg);
	}

	@Override
	public BaseRestResponseResult decode(RestMessage msg)
			throws NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		StatusType status = defaultDecodeStrategy.getStatus(msg);
		JavaType mappingClass = defaultDecodeStrategy.getModelClass(msg);

		// Check also content type (application/json)
		if (msg.getHeaders().containsKey("Content-Type")
				& !((String) (msg.getHeaders().getFirst("Content-Type")))
						.contains("application/json"))
			throw new MappingException("Not JSON encoded body.", null, msg);

		Object result;
		try {
			result = new ObjectMapper().readValue(msg.getBody(), mappingClass);

			// Check if result is error
			// if(((GeocodingResponse) result).getErrorMessage()!=null)
			// status=((GeocodingResponse) result).getStatus();
			// else
			// status=StatusType.OK;
		} catch (IOException e) {
			throw new MappingException(e.getMessage(), e, msg);
		}

		return new BaseRestResponseResult(status, result, mappingClass);
	}
}
