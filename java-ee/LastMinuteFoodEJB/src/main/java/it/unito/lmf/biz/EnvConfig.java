package it.unito.lmf.biz;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class EnvConfig {

	Logger logger = LoggerFactory.getLogger(EnvConfig.class);

	@PostConstruct
	private void init() {
		logger.warn("STARTED !");
		System.out.println("STARTED !");
	}

}
