package it.unito.lmf.biz.exceptions;

/**
 * Checked exception thrown when an entity cannot be found.
 * 
 * @author Biava Lorenzo
 *
 */
public abstract class ItemNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3958251103676652597L;

	protected Class<?> itemClass;
	protected String fieldName;
	protected Object fieldValue;

	public ItemNotFoundException(Class<?> itemClass) {
		super();
	}

	public ItemNotFoundException(Class<?> itemClass, String message,
			Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs an instance of this class, taking information about the failed
	 * lookup.
	 * 
	 * @param itemClass
	 *            the class of the item not found.
	 * @param fieldName
	 *            the name of the field used for search.
	 * @param fieldValue
	 *            the value of the field used for search.
	 */
	public ItemNotFoundException(Class<?> itemClass, String fieldName,
			Object fieldValue) {
		super(generateMessage(itemClass, fieldName, fieldValue));
		this.itemClass = itemClass;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}

	private static final String generateMessage(Class<?> itemClass,
			String fieldName, Object fieldValue) {
		return itemClass.getName() + " cannot be found with " + fieldName + "="
				+ fieldValue;
	}

	public Class<?> getItemClass() {
		return itemClass;
	}

	public String getFieldName() {
		return fieldName;
	}

	public Object getFieldValue() {
		return fieldValue;
	}
}
