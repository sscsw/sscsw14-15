package it.unito.lmf.util.web.rs.restclient;

import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;

/**
 * Factory interface to get {@link RestClient} implementations.
 * 
 * @author l.biava
 * 
 */
public abstract class RestClientFactory {

	public static final String BASE_REST_CLIENT_CRITERIA = "base";
	public static final String AUTHENTICATION_REST_CLIENT_CRITERIA = "auth";
	public static final String SECURE_REST_CLIENT_CRITERIA = "secure";

	public abstract <T extends BaseRestResponseResult> RestClient<T> getRestClient(
			String criteria);

	public abstract <T extends BaseRestResponseResult> RestClient<T> getRestClient();
}
