package it.unito.lmf.biz.accounting;

import it.unito.lmf.biz.JNDIHelper;
import it.unito.lmf.biz.accounting.exceptions.UserNotFoundException;
import it.unito.lmf.biz.notification.NotificationMgmtBean;
import it.unito.lmf.dal.dao.accounting.SellerDAO;
import it.unito.lmf.dal.dao.accounting.UserDAO;
import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.accounting.exceptions.EmailAlreadyInUseException;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.domain.dsl.external.google.geocoding.Location;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIClient;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIException;
import it.unito.lmf.util.geocoding.GeocodingHelper;

import java.io.InputStream;
import java.util.Collection;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class UserMgmtBean
 */
@Stateless
@LocalBean
public class UserMgmtBean implements UserMgmtBeanLocal {

	public static final String JNDIName = JNDIHelper.EJB_BasePath
			+ "UserMgmtBean!it.unito.lmf.biz.accounting.UserMgmtBean";
	// ="java:global/LastMinuteFood/lmf-ee-ejb/UserMgmtBean!it.unito.lmf.biz.accounting.UserMgmtBean";

	@EJB
	protected UserDAO userDAO;

	@EJB
	protected SellerDAO sellerDAO;

	@EJB
	protected NotificationMgmtBean notificationBean;

	// @EJB
	// protected GenericDAO<User> genUserDAO;

	/**
	 * Default constructor.
	 */
	public UserMgmtBean() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * public User createNewUser(String username) { User user = new User();
	 * user.setUsername(username); // user.setId(new
	 * Double(Math.random()).longValue()); userDAO.create(user);
	 * 
	 * return user; }
	 */

	public User createNewUser(User u) throws UsernameAlreadyInUseException,	EmailAlreadyInUseException {
		// TODO: user validation?

		// if (u.getSignInProvider() != null)
			// Social Network sign up enabled by default
			u.setEnabled(true);
		// else
		// u.setEnabled(false);
		try {
			return userDAO.create(u);
		} catch (EJBException e) {
			RuntimeException t = extractInnerEJBException(e);
			throw (t != null ? t : e);
		}
	}

	protected RuntimeException extractInnerEJBException(EJBException ejbEx) {
		Throwable ex = ejbEx.getCause();
		while (ex != null) {
			if (!(ex instanceof EJBException)) {
				return (RuntimeException) ex;
			}
			ex = ex.getCause();
		}
		return null;
	}

	public User getUserById(Long id) {
		return userDAO.find(id);
	}

	public User getUserByEmail(String email) {
		return userDAO.findByEmail(email);
	}
	
	public User updateUser(User user) throws UsernameAlreadyInUseException,	EmailAlreadyInUseException {
		//TODO Validation ?
		User oldUser = getUserById(user.getId());
		
		String newAddress = user.getAddress().trim(),
				oldAddress = oldUser.getAddress();
		
		if(newAddress != null && !newAddress.equals("") && !newAddress.equals(oldAddress)) { // address changed
			// Geocode address to coordinates
			Location l = GeocodingHelper.getLocation(newAddress);
			user.setLatitude(l.getLat());
			user.setLongitude(l.getLng());
		}
		
		try {
			return userDAO.update(user);
		} catch (EJBException e) {
			RuntimeException t = extractInnerEJBException(e);
			throw (t != null ? t : e);
		}
	}
	
	public User updateProfileImg(User user, InputStream picture, String pictureContentType) {
		try {
			boolean retry = true;
			int retries = 0;
			while (retry == true) {
				retry = false;
				retries++;
				try {
					String pictureKey = UUID.randomUUID().toString();
					LMFPictureRepoAPIClient prc = new LMFPictureRepoAPIClient(
							LMFPictureRepoAPIClient.TOKEN);
					user.setImage(prc.uploadPicture(pictureKey, picture, pictureContentType));
				} catch (LMFPictureRepoAPIException e) {
					if (e.getResponseMessage().getHttpStatusCode() == 409)
						if (retries < 3)
							retry = true;
						else
							throw new Exception("Cannot upload picture.");
					else
						throw e;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(
					"Problem uploading the user's profile image. Error: "
							+ e.getMessage(), e);
		}
		
		return updateUser(user);
	}

	public Collection<User> getAllUsers() {
		return userDAO.findAll();
	}

	/**
	 * Creates a new seller, waiting for authorization by an Administrator. So
	 * it also creates an Administrator notification.
	 * 
	 * @param seller
	 *            the seller data to be created.
	 * @return the seller newly created.
	 */
	public Seller requestSellerAuthorization(Seller seller, long userId) {
		User user = userDAO.find(userId);
		if (user == null)
			throw new UserNotFoundException("Id", userId);

		// TODO: Validate seller ?
		seller.setUser(user);
		seller.setAuthorized(false);

		Seller newSeller = sellerDAO.create(seller);

		// TODO: Create admin notification
		notificationBean.createSellerAuthRequestAdminNotification(seller);

		return newSeller;
	}

	/**
	 * Creates a new seller, waiting for authorization by an Administrator. So
	 * it also creates an Administrator notification.
	 * 
	 * @param seller
	 *            the seller data to be created.
	 * @return the seller newly created.
	 */
	public Seller sellerAuthorizationRequestResult(Long sellerId,
			boolean accepted) {
		Seller seller = sellerDAO.find(sellerId);
		if (seller == null)
			throw new UserNotFoundException("Id", sellerId);

		if (accepted) {
			seller.setAuthorized(accepted);
			return sellerDAO.update(seller);
		} else {
			sellerDAO.delete(seller.getId());
			return null;
		}

		// if(authorization)
		// //SEND CONFIRMATION EMAIL
		// else
		// //SEND REJECTION EMAIL

	}
	
	public Collection<Seller> getAllPendingSellers() {
		return sellerDAO.findAllPending();
	}
}
