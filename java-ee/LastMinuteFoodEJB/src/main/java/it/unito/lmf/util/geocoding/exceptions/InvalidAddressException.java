package it.unito.lmf.util.geocoding.exceptions;

public class InvalidAddressException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6948778795192233187L;
	
	private String address;
	
	public InvalidAddressException(String address) {
		super("Invalid address: " + address);
		this.address = address;
	}

	public String getAddress() {
		return address;
	}
}
