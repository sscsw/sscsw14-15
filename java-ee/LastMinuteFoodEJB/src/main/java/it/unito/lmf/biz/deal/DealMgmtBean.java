package it.unito.lmf.biz.deal;

import static com.mysema.query.types.expr.MathExpressions.acos;
import static com.mysema.query.types.expr.MathExpressions.cos;
import static com.mysema.query.types.expr.MathExpressions.radians;
import static com.mysema.query.types.expr.MathExpressions.sin;
import it.unito.lmf.biz.JNDIHelper;
import it.unito.lmf.biz.accounting.exceptions.UserNotFoundException;
import it.unito.lmf.biz.notification.NotificationMgmtBean;
import it.unito.lmf.dal.dao.accounting.UserDAO;
import it.unito.lmf.dal.dao.deal.DealCategoryDAO;
import it.unito.lmf.dal.dao.deal.DealDAO;
import it.unito.lmf.dal.dao.shop.ShopDAO;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.deal.DealCategory;
import it.unito.lmf.dal.entities.deal.QDeal;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.domain.dsl.deal.DealSearchData;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIClient;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIException;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mysema.query.support.Expressions;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.NumberPath;

/**
 * Session Bean implementation class UserMgmtBean
 */
@Stateless
@LocalBean
public class DealMgmtBean {

	public static final String JNDIName = JNDIHelper.EJB_BasePath
			+ "DealMgmtBean!it.unito.lmf.biz.deal.DealMgmtBean";

	@EJB
	protected DealDAO dealDAO;

	@EJB
	protected ShopDAO shopDAO;

	@EJB
	protected DealCategoryDAO dealCategoryDAO;

	@EJB
	protected UserDAO userDAO;

	@EJB
	protected NotificationMgmtBean notificationBean;

	/**
	 * Creates a new deal, creating automatic notification for interested users.
	 * 
	 * @param deal
	 *            the deal data for the deal creation.
	 * @param userId
	 *            the id of the user creating the deal.
	 * @param categoriesId
	 *            a list of {@link DealCategory} ids.
	 * @param shop
	 *            the shop to publish the offer into.
	 * @return the deal newly created.
	 * @throws IllegalArgumentException
	 *             if the owner of the shop is not the given user
	 * @throws UserNotFoundException
	 *             if the given userId does not exist.
	 */
	public Deal createNewDeal(Deal deal, long userId,
			Collection<Long> categoriesId, Shop shop, InputStream picture,
			String pictureContentType) throws UserNotFoundException,
			IllegalArgumentException {
		User user = userDAO.find(userId);
		if (user == null)
			throw new UserNotFoundException("Id", userId);

		if (shop.getOwner().getId() != user.getId())
			throw new IllegalArgumentException(
					"You're not the owner of this shop.");

		if (!shop.isAuthorized())
			throw new IllegalArgumentException(
					"This shop has not been authorized yet.");

		Set<DealCategory> categories = new HashSet<DealCategory>();
		for (Long categoryId : categoriesId) {
			DealCategory category = dealCategoryDAO.find(categoryId);
			if (category == null)
				throw new UserNotFoundException("Id", categoryId);

			categories.add(category);
		}

		// TODO: Validate deal ?
		deal.setShop(shop);
		deal.setCategories(categories);
		deal.setDiscountPercentage(Math.floor((deal.getStartingPrice() - deal
				.getPrice()) / deal.getStartingPrice() * 100));

		try {
			boolean retry = true;
			int retries = 0;
			while (retry == true) {
				retry = false;
				retries++;
				try {
					String pictureKey = UUID.randomUUID().toString();
					LMFPictureRepoAPIClient prc = new LMFPictureRepoAPIClient(
							LMFPictureRepoAPIClient.TOKEN);
					deal.setImage(prc.uploadPicture(pictureKey, picture,
							pictureContentType));
				} catch (LMFPictureRepoAPIException e) {
					if (e.getResponseMessage().getHttpStatusCode() == 409)
						if (retries < 3)
							retry = true;
						else
							throw new Exception("Cannot upload picture.");
					else
						throw e;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(
					"Problem uploading the deal's picture. Error: "
							+ e.getMessage(), e);
		}

		Deal newDeal = dealDAO.create(deal);

		// Create users notification
		notificationBean.onDealCreated(newDeal);

		return newDeal;
	}

	public Deal getDealById(Long id) {
		return dealDAO.find(id);
	}

	public Collection<Deal> getDealsByShop(Long shopId) {
		return dealDAO.findByShop(shopId);
	}

	public Page<Deal> getDealsByShop(Long shopId, Pageable pageable) {
		return dealDAO.findByShop(shopId, pageable);
	}

	public Collection<Deal> getAllDeals() {
		return dealDAO.findAll();
	}

	public Collection<DealCategory> getAllDealCategories() {
		return dealCategoryDAO.findAll();
	}

	public Page<Deal> getAllDeals(Pageable pageable) {
		return dealDAO.findAll(null, pageable);
	}

	/**
	 * By default not shoring expired deals.
	 * 
	 * @param pageable
	 * @param searchData
	 *            a {@link DealSearchData} instance, specifying defined search
	 *            parameters. If <code>null</code> will be ignored.
	 * @return
	 */
	public Page<Deal> getAllDealsWithSearch(Pageable pageable,
			DealSearchData searchData) {

		QDeal deal = QDeal.deal;
		Predicate p = deal.expireDate.isNull().or(
				deal.expireDate.after(new Date()));

		if (searchData != null) {

			// MUST BE THE FIRST !
			if (searchData.isExpired()) {
				p = null;
			}

			// Is in one of these categories
			Predicate cats = null;
			for (long catId : searchData.getCategories()) {
				DealCategory cat = new DealCategory();
				cat.setId(catId);
				cats = deal.categories.contains(cat).or(cats);
			}
			if (cats != null)
				p = ((BooleanExpression) cats).and(p);

			if (searchData.getDiscountReason() != null) {
				p = deal.discountReason.containsIgnoreCase(
						searchData.getDiscountReason()).and(p);
			}

			if (searchData.getName() != null) {
				p = deal.name.containsIgnoreCase(searchData.getName()).and(p);
			}

			if (searchData.getMaxAge() != null) {
				Calendar calendar = Calendar.getInstance(); // this would
															// default to now
				calendar.add(Calendar.DAY_OF_YEAR, -searchData.getMaxAge());
				p = deal.createdAt.after(calendar.getTime()).and(p);
			}

			if (searchData.getMaxPrice() != null) {
				p = deal.price.loe(searchData.getMaxPrice()).and(p);
			}

			if (searchData.getMinPrice() != null) {
				p = deal.price.goe(searchData.getMinPrice()).and(p);
			}

			if (searchData.getMinQuantity() != null) {
				p = deal.quantity.goe(searchData.getMinQuantity()).and(p);
			}

			// Search deal inside the range
			if (searchData.getDistance() != null
					&& searchData.getDistanceFromLatLng() != null) {

				Double rqdLat = Double.parseDouble(searchData
						.getDistanceFromLatLng().split(",")[0]);
				Double rqdLng = Double.parseDouble(searchData
						.getDistanceFromLatLng().split(",")[1]);
				NumberPath<Double> lat = deal.shop.latitude;
				NumberPath<Double> lng = deal.shop.longitude;

//				NumberExpression<Double> formula = (acos(cos(
//						radians(Expressions.constant(rqdLat)))
//						.multiply(
//								cos(radians(lat))
//										.multiply(
//												cos(radians(lng)
//														.subtract(
//																radians(Expressions
//																		.constant(rqdLng)))))
//														.add(sin(
//																radians(Expressions
//																		.constant(rqdLat)))
//																.multiply(
//																		sin(radians(lat)))))))))
//						.multiply(Expressions.constant(6371)));
				
				NumberExpression<Double> formula = acos(
															cos(radians(Expressions.constant(rqdLat)))
															.multiply(
																	cos(radians(lat))
															).multiply(
																	cos(
																			radians(lng)
																			.subtract(radians(Expressions.constant(rqdLng)))
																	)
															).add(
																	sin(radians(Expressions.constant(rqdLat)))
																	.multiply(sin(radians(lat)))
															)
														).multiply(Expressions.constant(6371));

				p = formula.lt(searchData.getDistance()).and(p);
			}

			// TODO
			// if(searchData.getMinShopFeedback()!=null){
			// deal.shop.feedback.
			// }
		}

		return dealDAO.findAll(p, pageable);
	}
}
