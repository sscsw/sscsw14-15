package it.unito.lmf.util.web.rs.decoding;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * This POJO holds a Rest Message (HttpStatusCode, Headers and Body).
 * @author l.biava
 *
 */
public class RestMessage {

	private MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
	private String body;
	private int httpStatusCode;

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public RestMessage(MultivaluedMap<String, Object> headers, String body) {
		super();
		this.body = body;
		this.headers = headers;
	}

	public RestMessage(MultivaluedMap<String, Object> headers, String body,
			int httpStatusCode) {
		super();
		this.headers = headers;
		this.body = body;
		this.httpStatusCode = httpStatusCode;
	}

	public void setHeaders(MultivaluedMap<String, Object> headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public MultivaluedMap<String, Object> getHeaders() {
		return headers;
	}

	@Override
	public String toString() {
		return "RestMessage [headers=" + headers + ", body=" + body
				+ ", httpStatusCode=" + httpStatusCode + "]";
	}
	
}
