package it.unito.lmf.libs.picturerepo;

import it.unito.lmf.util.web.rs.apiclient.AbstractAPIClient;
import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;
import it.unito.lmf.util.web.rs.decoding.RestMessage;
import it.unito.lmf.util.web.rs.decoding.exceptions.MappingException;
import it.unito.lmf.util.web.rs.restclient.RestClient;
import it.unito.lmf.util.web.rs.restclient.RestClientFactory;
import it.unito.lmf.util.web.rs.restclient.RestClientHelper;
import it.unito.lmf.util.web.rs.restclient.RestClient.RestMethod;

import java.io.InputStream;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;

public class LMFPictureRepoAPIClient extends AbstractAPIClient {

	public static final String TOKEN = "f3065f20-8944-11e4-b4a9-0800200c9a66";

	private static final String BASE_URL = "https://graceful-splice-801.appspot.com/picture";
	private String apiKey;

	RestClient<BaseRestResponseResult> client;

	public LMFPictureRepoAPIClient(String apiKey,
			RestClientFactory restClientFactory) {
		super(BASE_URL, restClientFactory);
		this.apiKey = apiKey;
	}

	public LMFPictureRepoAPIClient(String apiKey) {
		super(BASE_URL);
		this.apiKey = apiKey;
	}

	public String uploadPicture(String key, InputStream picture,
			String contentType) throws LMFPictureRepoAPIException, Exception {

		String URL = baseWSUrl;

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();

		MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<String, Object>();
		queryParams.add("key", key);
		queryParams.add("token", this.apiKey);

		RestMessage result = restClient.doRequest(
				RestMethod.PUT,
				URL,
				headers,
				queryParams,
				new RestClientHelper.FormDataEntityBuilder().addFile("picture",
						picture, MediaType.valueOf(contentType), "picture.png")
						.build(),
				RestClientHelper.FormDataEntityBuilder.MEDIA_TYPE, null);

		if (handleResult(result))
			return baseWSUrl + "/" + key;
		else
			return null;
	}

	protected <RespType> void handleErrorResult(RestMessage result)
			throws LMFPictureRepoAPIException {

		throw new LMFPictureRepoAPIException("Error "
				+ result.getHttpStatusCode(), null, result);
	}

	protected boolean handleSuccessResult(RestMessage result)
			throws MappingException {
		return true;
	}

	protected boolean handleResult(RestMessage result) throws MappingException,
			LMFPictureRepoAPIException {
		Status status = Status.fromStatusCode(result.getHttpStatusCode());

		if (status.getFamily() == Family.SUCCESSFUL)
			return handleSuccessResult(result);
		else
			handleErrorResult(result);

		return false;
	}
}
