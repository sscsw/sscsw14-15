package it.unito.lmf.util.web.rs.restclient;

import it.unito.lmf.util.json.JsonUtility;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * Helper class for {@link RestClient} request body.
 * 
 * @author l.biava
 * 
 */
public class RestClientHelper {

	/**
	 * Builder for JSON body. <br/>
	 * <b>Usage example:</b></br>
	 * {@code GenericEntity<String> ge = new RestClientHelper.JsonEntityBuilder().build(some_POJO);
	 * }
	 * 
	 * @author l.biava
	 * 
	 */
	public static class JsonEntityBuilder {
		public static final MediaType MEDIA_TYPE = MediaType.APPLICATION_JSON_TYPE;

		public GenericEntity<String> build(Object request)
				throws JsonParseException, JsonMappingException, IOException {
			return new GenericEntity<String>(JsonUtility.serializeJson(request)) {
			};
		}
	}

	/**
	 * Builder for MultiPart Form Data body. <br/>
	 * <br/>
	 * <b>Usage example:</b></br>
	 * {@code GenericEntity<MultipartFormDataOutput> ge = new
	 * RestClientHelper.FormDataEntityBuilder().create().addFormData(field_name,
	 * field_entity, field_media_type).build(); }
	 * 
	 * @author l.biava
	 * 
	 */
	public static class FormDataEntityBuilder {

		public static final MediaType MEDIA_TYPE = MediaType.MULTIPART_FORM_DATA_TYPE;

		private FormDataMultiPart mdo;

		// builder methods for setting property
		private FormDataEntityBuilder create() {
			this.mdo = new FormDataMultiPart();
			this.mdo.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
			return this;
		}

		// public FormDataEntityBuilder addFormData(String key, Object entity,
		// MediaType mediaType, String filename) {
		// if (this.mdo == null)
		// this.create();
		//
		// this.mdo.field(key, entity, mediaType, filename);
		// return this;
		// }

		public FormDataEntityBuilder addFormData(String key, Object entity,
				MediaType mediaType) {
			if (this.mdo == null)
				this.create();
			this.mdo.field(key, entity, mediaType);
			return this;
		}

		public FormDataEntityBuilder addFile(String key, InputStream file,
				MediaType mediaType, String filename) {
			if (this.mdo == null)
				this.create();

			this.mdo.bodyPart(new StreamDataBodyPart(key, file, filename,
					mediaType));
			return this;
		}

		/**
		 * 
		 * @return the fully built object.
		 */
		public GenericEntity<FormDataMultiPart> build() {
			return new GenericEntity<FormDataMultiPart>(this.mdo) {
			};
		}
	}

}
