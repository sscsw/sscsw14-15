package it.unito.lmf.biz.shop;

import it.unito.lmf.biz.JNDIHelper;
import it.unito.lmf.biz.accounting.exceptions.UserNotFoundException;
import it.unito.lmf.biz.notification.NotificationMgmtBean;
import it.unito.lmf.biz.shop.exceptions.ShopNotFoundException;
import it.unito.lmf.dal.dao.accounting.UserDAO;
import it.unito.lmf.dal.dao.shop.ShopCategoryDAO;
import it.unito.lmf.dal.dao.shop.ShopDAO;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.dal.entities.shop.ShopCategory;
import it.unito.lmf.domain.dsl.external.google.geocoding.GeocodingResponse;
import it.unito.lmf.domain.dsl.external.google.geocoding.Location;
import it.unito.lmf.libs.external.google.geocoding.GoogleGeocodingAPIClient;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIClient;
import it.unito.lmf.libs.picturerepo.LMFPictureRepoAPIException;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Session Bean implementation class UserMgmtBean
 */
@Stateless
@LocalBean
public class ShopMgmtBean {

	public static final String JNDIName = JNDIHelper.EJB_BasePath
			+ "ShopMgmtBean!it.unito.lmf.biz.shop.ShopMgmtBean";

	@EJB
	protected ShopDAO shopDAO;

	@EJB
	protected ShopCategoryDAO shopCategoryDAO;

	@EJB
	protected UserDAO userDAO;
	
	@EJB
	protected NotificationMgmtBean notificationBean;

	public Shop getShopById(Long id) {
		return shopDAO.find(id);
	}

	public Collection<Shop> getShopsByOwner(Long ownerId) {
		return shopDAO.findByOwner(ownerId);
	}

	public Page<Shop> getShopsByOwner(Long ownerId, Pageable pageable) {
		return shopDAO.findByOwner(ownerId, pageable);
	}

	public Collection<ShopCategory> getAllShopCategories() {
		return shopCategoryDAO.findAll();
	}

	/**
	 * Creates a new shop, waiting for authorization by an Administrator. So it
	 * also creates an Administrator notification.
	 * 
	 * @param shop
	 *            the shop data to be created.
	 * @return the shop newly created.
	 */
	public Shop createNewShop(Shop shop, long userId,
			Collection<Long> categoriesId, InputStream picture,
			String pictureContentType) {
		User user = userDAO.find(userId);
		if (user == null)
			throw new UserNotFoundException("Id", userId);

		Set<ShopCategory> categories = new HashSet<ShopCategory>();
		for (Long categoryId : categoriesId) {
			ShopCategory category = shopCategoryDAO.find(categoryId);
			if (category == null)
				throw new UserNotFoundException("Id", categoryId);

			categories.add(category);
		}

		// Geocode address to coordinates
		try {
			GeocodingResponse geoResponse = new GoogleGeocodingAPIClient("")
					.getLatLng(shop.getAddress());
			if (geoResponse.getResults().size() < 1)
				throw new RuntimeException("No results.");

			Location location = geoResponse.getResults().get(0).getGeometry()
					.getLocation();
			shop.setLatitude(location.getLat());
			shop.setLongitude(location.getLng());
		} catch (Exception e) {
			throw new RuntimeException(
					"Cannot get coordinates for the given address.", e);
		}

		// TODO: Validate shop ?
		shop.setOwner(user.getSeller());
		shop.setCategories(categories);
		shop.setAuthorized(false);

		// Upload picture
		try {
			boolean retry = true;
			int retries = 0;
			while (retry == true) {
				retry = false;
				retries++;
				try {
					String pictureKey = UUID.randomUUID().toString();
					LMFPictureRepoAPIClient prc = new LMFPictureRepoAPIClient(
							LMFPictureRepoAPIClient.TOKEN);
					shop.setImage(prc.uploadPicture(pictureKey, picture,
							pictureContentType));
				} catch (LMFPictureRepoAPIException e) {
					if (e.getResponseMessage().getHttpStatusCode() == 409)
						if (retries < 3)
							retry = true;
						else
							throw new Exception("Cannot upload picture.");
					else
						throw e;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(
					"Problem uploading the deal's picture. Error: "
							+ e.getMessage(), e);
		}
		
		Shop newShop = shopDAO.create(shop);

		// TODO: Create admin notification
		notificationBean.createShopAuthRequestAdminNotification(shop);

		return newShop;
	}

	public Shop shopAuthorizationRequestResult(Long shopId, boolean accepted) {
		Shop shop = shopDAO.find(shopId);
		if (shop == null)
			throw new ShopNotFoundException("Id", shopId);

		if (accepted) {

			shop.setAuthorized(accepted);
			return shopDAO.update(shop);
		} else {
			shopDAO.delete(shop.getId());
			return null;
		}

		// TODO: send notification e-mail
	}

	// public Collection<Deal> getDealsByShop(Long shopId) {
	// return dealDAO.findByShop(shopId);
	// }

	public Collection<Shop> getAllShops() {
		return shopDAO.findAll();
	}

	public Page<Shop> getAllShops(Pageable pageable) {
		return shopDAO.findAll(null, pageable);
	}
	
	public Page<Shop> getAllAuthorizedShops(Pageable pageable) {
		return shopDAO.findAllAuthorized(pageable);
	}
	
	public Collection<Shop> getAllPendingShops() {
		return shopDAO.findAllPending();
	}
}
