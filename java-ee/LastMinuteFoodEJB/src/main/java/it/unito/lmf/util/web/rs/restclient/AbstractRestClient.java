package it.unito.lmf.util.web.rs.restclient;

import it.unito.lmf.util.web.rs.decoding.BaseRestResponseResult;
import it.unito.lmf.util.web.rs.decoding.RestMessage;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecodeStrategy;
import it.unito.lmf.util.web.rs.decoding.RestResponseDecoder;
import it.unito.lmf.util.web.rs.decoding.exceptions.MappingException;
import it.unito.lmf.util.web.rs.decoding.exceptions.NoMappingModelFoundException;
import it.unito.lmf.util.web.rs.decoding.exceptions.ServerErrorResponseException;
import it.unito.lmf.util.web.rs.restclient.exceptions.RestClientException;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

public abstract class AbstractRestClient<T extends BaseRestResponseResult>
		implements RestClient<T> {

	protected int defaultTimeout = 30000; // in ms

	@Override
	public int getDefaultTimeout() {
		return this.defaultTimeout;
	}

	@Override
	public void setDefaultTimeout(int defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
	}

	/***************************** GET Requests ******************************/

	@Override
	public T getRequest(String URL, MultivaluedMap<String, Object> headers,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		return getRequest(URL, headers, null, rrd, strategy);
	}

	@Override
	public RestMessage getRequest(String URL,
			MultivaluedMap<String, Object> headers) throws RestClientException {

		return getRequest(URL, headers, null);
	}

	/***************************** HEAD Requests ******************************/

	@Override
	public T headRequest(String URL, MultivaluedMap<String, Object> headers,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		return headRequest(URL, headers, null, rrd, strategy);
	}

	@Override
	public RestMessage headRequest(String URL,
			MultivaluedMap<String, Object> headers) throws RestClientException {

		return headRequest(URL, headers, null);
	}

	/***************************** POST Requests ******************************/

	@Override
	public <E> T postRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RestResponseDecoder<T> rrd,
			RestResponseDecodeStrategy strategy) throws RestClientException,
			NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		return postRequest(URL, headers, body, bodyMediaType, null, rrd,
				strategy);

	}

	@Override
	public <E> RestMessage postRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType) throws RestClientException {

		return postRequest(URL, headers, body, bodyMediaType, null);
	}

	/***************************** PUT Requests ******************************/

	@Override
	public <E> T putRequest(String URL, MultivaluedMap<String, Object> headers,
			GenericEntity<E> body, MediaType bodyMediaType,
			RestResponseDecoder<T> rrd, RestResponseDecodeStrategy strategy)
			throws RestClientException, NoMappingModelFoundException,
			MappingException, ServerErrorResponseException {

		return putRequest(URL, headers, body, bodyMediaType, null, rrd,
				strategy);
	}

	@Override
	public <E> RestMessage putRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType) throws RestClientException {

		return putRequest(URL, headers, body, bodyMediaType, null);
	}

	/***************************** DELETE Requests ******************************/

	@Override
	public <E> T deleteRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType, RestResponseDecoder<T> rrd,
			RestResponseDecodeStrategy strategy) throws RestClientException,
			NoMappingModelFoundException, MappingException,
			ServerErrorResponseException {

		return deleteRequest(URL, headers, body, bodyMediaType, null, rrd,
				strategy);
	}

	@Override
	public <E> RestMessage deleteRequest(String URL,
			MultivaluedMap<String, Object> headers, GenericEntity<E> body,
			MediaType bodyMediaType) throws RestClientException {

		return deleteRequest(URL, headers, body, bodyMediaType, null);
	}

}
