package it.unito.lmf.biz.accounting.exceptions;

import it.unito.lmf.biz.exceptions.ItemNotFoundException;
import it.unito.lmf.dal.entities.accounting.User;

/**
 * Checked exception thrown when a {@link User} cannot be found.
 * @author Biava Lorenzo
 *
 */
public class UserNotFoundException extends ItemNotFoundException {

	private static final long serialVersionUID = -3958251103676652597L;
	
	private static final Class<?> itemClass=User.class;

	public UserNotFoundException(String fieldName,
			Object fieldValue) {
		super(itemClass, fieldName, fieldValue);
	}

	public UserNotFoundException(String message,
			Throwable cause) {
		super(itemClass, message, cause);
	}

	public UserNotFoundException() {
		super(itemClass);
	}

}
