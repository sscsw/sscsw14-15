package it.unito.lmf.libs.external.google.geocoding;

import it.unito.lmf.util.web.rs.decoding.RestMessage;

public class GoogleGeocodingAPIException extends RuntimeException {

	private static final long serialVersionUID = 3965412299072282395L;

	private RestMessage responseMessage;
	private GoogleGeocodingAPIError APIError;

	public RestMessage getResponseMessage() {
		return responseMessage;
	}

	public boolean hasResponseMessage() {
		return responseMessage != null;
	}

	public GoogleGeocodingAPIException(String msg, Throwable t,
			RestMessage responseMessage, GoogleGeocodingAPIError APIError) {
		super(msg, t);
		this.responseMessage = responseMessage;
		this.APIError = APIError;
	}

	public GoogleGeocodingAPIError getAPIError() {
		return APIError;
	}
}