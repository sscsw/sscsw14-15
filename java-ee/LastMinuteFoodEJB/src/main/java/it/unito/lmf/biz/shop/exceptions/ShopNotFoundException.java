package it.unito.lmf.biz.shop.exceptions;

import it.unito.lmf.biz.exceptions.ItemNotFoundException;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.shop.Shop;

/**
 * Checked exception thrown when a {@link User} cannot be found.
 * @author Biava Lorenzo
 *
 */
public class ShopNotFoundException extends ItemNotFoundException {

	private static final long serialVersionUID = -3958251103676652597L;
	
	private static final Class<?> itemClass=Shop.class;

	public ShopNotFoundException(String fieldName,
			Object fieldValue) {
		super(itemClass, fieldName, fieldValue);
	}

	public ShopNotFoundException(String message,
			Throwable cause) {
		super(itemClass, message, cause);
	}

	public ShopNotFoundException() {
		super(itemClass);
	}

}
