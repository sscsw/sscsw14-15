package it.unito.lmf.libs.picturerepo;

import it.unito.lmf.util.web.rs.decoding.RestMessage;

public class LMFPictureRepoAPIException extends RuntimeException {

	private static final long serialVersionUID = 3965412299072282395L;

	private RestMessage responseMessage;

	public RestMessage getResponseMessage() {
		return responseMessage;
	}

	public boolean hasResponseMessage() {
		return responseMessage != null;
	}

	public LMFPictureRepoAPIException(String msg, Throwable t,
			RestMessage responseMessage) {
		super(msg, t);
		this.responseMessage = responseMessage;
	}
}