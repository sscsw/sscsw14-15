var serverURL = window.location.origin + "/web";

/*
 * {0}: number of new messages
 */
var newMessagesHTMLTemplate = '<li><p class="notification-header">You have {0} new messages</p></li>';
var seeAllMessagesHTML = '<li><a href="#">See all messages</a></li>';

/*
 * {0}: deal ID {1}: img src {2}: shop name {3}: time {4}: message
 */
var notificationHTMLTemplate = '<li><a href="'+ serverURL + '{0}"> <span class="photo"><img alt="avatar" src="{1}"></span> <span class="subject"> <span class="from">{2}</span> <span class="time">{3}</span></span> <span class="message">{4}</span></a></li>';

var notificationsUL = $("#notificationMenu+ul.dropdown-menu");

$(function() {

	String.prototype.format = String.prototype.f = function() {
		var s = this, i = arguments.length;

		while (i--) {
			s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
		}
		return s;
	};

	String.prototype.trimToLength = function(m) {
		return (this.length > m) ? jQuery.trim(this).substring(0, m).split(" ")
				.slice(0, -1).join(" ")
				+ "..." : this;
	};

	// loading notifications on page load
	loadNotifications();
});


function showTime(ms) {
	var diff = (new Date().getTime() - ms);
	var mins = diff / 60000;

	if (mins < 3)
		return "Just Now";

	return moment(ms).fromNow()
}

function showMessage(event, deal) {
	var s = event + " in " + deal.categories.toString();
	return s.trimToLength(35);
}


function insertNotification(i, notification) {
	notificationsUL.append(notificationHTMLTemplate.f(
			"/notifications/" + notification.id + "/read",
			(notification.deal.image != null) ? notification.deal.image : "//placehold.it/40x40", notification.deal.shop.name,
			showTime(notification.createdAt), 
			showMessage(notification.event,	notification.deal))
	);
}


function loadNotifications() {
	$.ajax({
		url : serverURL + "/notifications/my",
		type : "GET",
		success : function(result) {
			//console.log(result); // DEBUG
			if (result.length > 0)
				$("#notificationMenu>.badge").text(result.length);
			
			notificationsUL.html(newMessagesHTMLTemplate.f(result.length));
			$.each(result, function(index, obj){
				insertNotification(index, obj)
			});
			notificationsUL.append(seeAllMessagesHTML);
		},
		error : function(request, state, errors) {
			console.log("Error during notification loading");
		},
		timeout : 10000, // timeout che scade dopo 10 secondi
		async : true
	// richiesta asincrona
	}).then(function() {
       setTimeout(loadNotifications, 60000);  // refresh notifications
    });;
}
