$(document).ready(function(){
    $("#show-login-dialog").click(function(){
        $("#login-dialog-wrapper").modal("show");
    });
    $("#login-dialog-username").keyup(function() {
        login_dialog_check_values();
    });
    $("#login-dialog-password").keyup(function() {
        login_dialog_check_values();
    });
    $('#error-dialog-wrapper').modal('show');
});

// input type file - helper
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

// enable bootstrap tooltips
$(function () {
	$('[tooltip="tooltip"]').tooltip();
})

function login_dialog_check_values() {
    if ($("#login-dialog-username").val().length != 0 && $("#login-dialog-password").val().length != 0) {
        $("#login-dialog-btn-submit").removeClass("disabled");
    }
}