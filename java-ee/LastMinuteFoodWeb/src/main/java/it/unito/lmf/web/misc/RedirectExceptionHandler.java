package it.unito.lmf.web.misc;

import it.unito.lmf.web.controllers.exceptions.RedirectException;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

@ControllerAdvice
public class RedirectExceptionHandler {

	// @ResponseStatus(HttpStatus.MOVED_PERMANENTLY)
	// @ExceptionHandler(RedirectException.class)
	// public String redirectException(RedirectException e, HttpSession session,
	// RedirectAttributes redirectAttrs) {
	//
	// if (e.getErrorMessage() != null)
	// redirectAttrs.addFlashAttribute("errorMsg", e.getErrorMessage());
	// return "redirect:" + e.getUrl();
	// }

	@ResponseStatus(HttpStatus.MOVED_PERMANENTLY)
	@ExceptionHandler(RedirectException.class)
	public RedirectView redirectException(RedirectException e,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		RedirectView rw = new RedirectView(e.getUrl(), true);
		
		FlashMap outputFlashMap = RequestContextUtils
				.getOutputFlashMap(request);
		if (outputFlashMap != null && e.getPopupMessage() != null) {
			switch(e.getPopupMessage().getType())
			{
			case ERROR:
				outputFlashMap.put("errorMsg", e.getPopupMessage().getMessage());
				break;
				
			default:
				outputFlashMap.put("popupMsg", e.getPopupMessage().getMessage());
				outputFlashMap.put("popupMsgType", e.getPopupMessage().getType().toString());
				break;
			}
		}
		return rw;
	}
}