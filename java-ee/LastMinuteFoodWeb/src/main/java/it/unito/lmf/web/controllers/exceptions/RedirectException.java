package it.unito.lmf.web.controllers.exceptions;

import it.unito.lmf.web.misc.PopupMessage;
import it.unito.lmf.web.misc.RedirectExceptionHandler;
import it.unito.lmf.web.misc.PopupMessage.MessageType;

/**
 * This exception is meant to be used for redirection purpose. It gets caught by
 * the appropriate {@link RedirectExceptionHandler} to cause the redirection to
 * the given URL. <br/>
 * In addition, you can specify an error message to be shown in the given
 * redirect page.
 * 
 * @author Biava Lorenzo
 *
 */
public class RedirectException extends RuntimeException {

	private static final long serialVersionUID = 541244114281690945L;

	private String url;
	private boolean contextRelativeUrl = true;
	private PopupMessage popupMessage;

	public RedirectException(String url) {
		this.url = url;
	}

	/**
	 * Constructs a redirect to the given (context-relative by default) URL,
	 * with the given {@link PopupMessage}.
	 * 
	 * @param url
	 * @param message
	 * @param messageType
	 */
	public RedirectException(String url, String message, MessageType messageType) {
		this.url = url;
		this.popupMessage = new PopupMessage(message, messageType);
	}

	public boolean isContextRelativeUrl() {
		return contextRelativeUrl;
	}

	public void setContextRelativeUrl(boolean contextRelativeUrl) {
		this.contextRelativeUrl = contextRelativeUrl;
	}

	public String getUrl() {
		return url;
	}

	public PopupMessage getPopupMessage() {
		return popupMessage;
	}
}
