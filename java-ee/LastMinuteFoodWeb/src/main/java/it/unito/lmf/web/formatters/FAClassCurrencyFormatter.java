package it.unito.lmf.web.formatters;

import it.unito.lmf.domain.dsl.values.Currency;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

public class FAClassCurrencyFormatter implements Formatter<Currency> {

	protected final String FA_EURO_CLASS = "fa-eur";
	protected final String FA_DOLLAR_CLASS = "fa-dollar";

	public FAClassCurrencyFormatter() {
		super();
	}

	@Override
	public String print(Currency object, Locale locale) {
		switch (object) {
		case EURO:
			return FA_EURO_CLASS;
		case DOLLAR:
			return FA_DOLLAR_CLASS;

		default:
			return FA_EURO_CLASS;
		}
	}

	@Override
	public Currency parse(String text, Locale locale) throws ParseException {
		switch (text) {
		case FA_EURO_CLASS:
			return Currency.EURO;

		case FA_DOLLAR_CLASS:
			return Currency.DOLLAR;

		default:
			return Currency.EURO;
		}
	}
	
	public Currency valueOf (String text) {
		try {
			return Currency.valueOf(text);
		} catch (Exception e) {
			return Currency.EURO;
		}
	}

}
