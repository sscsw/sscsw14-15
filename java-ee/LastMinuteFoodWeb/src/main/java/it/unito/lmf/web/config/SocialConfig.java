package it.unito.lmf.web.config;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
//import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;

import com.sun.appserv.jdbc.DataSource;

@Configuration
@EnableSocial
public class SocialConfig implements SocialConfigurer {

	private TextEncryptor textEncryptor;

	@Autowired
	private Environment env;
	
	@PostConstruct
	public void initializeTextEncryptor() {
		//TODO: Change key in production !!
		//See http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html before use
		textEncryptor = Encryptors.text(env
				.getProperty("security.social.encrypt.key"), KeyGenerators
				.string().generateKey());
	}

	@Override
	public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig,
			Environment env) {
		cfConfig.addConnectionFactory(new FacebookConnectionFactory(env
				.getProperty("facebook.app.id"), env
				.getProperty("facebook.app.secret")));
	}

	@Override
	public UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}

	@Resource(mappedName = "jdbc/mysql-db")
	DataSource dataSource;

	@Override
	public UsersConnectionRepository getUsersConnectionRepository(
			ConnectionFactoryLocator connectionFactoryLocator) {
		/*
		 * return new InMemoryUsersConnectionRepository(
		 * connectionFactoryLocator );
		 */
		return new JdbcUsersConnectionRepository(dataSource,
				connectionFactoryLocator, textEncryptor
		// Encryptors.noOpText()		
		);
	}

	@Bean
	public ConnectController connectController(
			ConnectionFactoryLocator connectionFactoryLocator,
			ConnectionRepository connectionRepository) {
		return new ConnectController(connectionFactoryLocator,
				connectionRepository);
	}

}
