package it.unito.lmf.web.forms.shop;

import it.unito.lmf.utils.validation.RegexpRepository;
import it.unito.lmf.web.validation.file.PictureFile;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class ShopRequestForm implements Serializable {

	private static final long serialVersionUID = -6971273404947634725L;

	@Pattern(regexp = RegexpRepository.REGEXP_PARTITA_IVA)
	private String partitaIVA;

	@Size(min = 10)
	private String address;

	@Size(min = 2, max = 64)
	private String name;

	@Pattern(regexp = RegexpRepository.REGEXP_PHONE_NUMBER)
	private String phoneNumber;

	@Size(min = 20, max = 256)
	private String description;

	@NotNull
	@PictureFile(maxSize=1024000)
	private MultipartFile picture;
	
	private List<Long> categories;

	public String getPartitaIVA() {
		return partitaIVA;
	}

	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}

	public MultipartFile getPicture() {
		return picture;
	}

	public void setPicture(MultipartFile picture) {
		this.picture = picture;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Long> getCategories() {
		return categories;
	}

	public void setCategories(List<Long> categories) {
		this.categories = categories;
	}

}
