package it.unito.lmf.web.security;

import it.unito.lmf.dal.entities.accounting.User;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.social.security.SocialUser;

/**
 * Class representing an authenticated user in the website. It is used by the
 * security framework.
 * 
 * @author Biava Lorenzo
 *
 */
public class LMFUserDetails extends SocialUser {

	private static final long serialVersionUID = -8256575640688338217L;

	private Long id;
	private String firstName;
	private String lastName;
	private List<UserRole> roles;
	private SocialMediaService socialSignInProvider;
	private User internalUser;

	public LMFUserDetails(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRole(List<UserRole> roles) {
		this.roles = roles;
	}

	public SocialMediaService getSocialSignInProvider() {
		return socialSignInProvider;
	}

	public User getInternalUser() {
		return internalUser;
	}

	public void setInternalUser(User internalUser) {
		this.internalUser = internalUser;
	}

	public void setSocialSignInProvider(SocialMediaService socialSignInProvider) {
		this.socialSignInProvider = socialSignInProvider;
	}
	
	public boolean isApprovedSeller() {
		return this.roles.contains(UserRole.ROLE_SELLER);
	}

	public static Builder getBuilder() {
		return new Builder();
	}

	public static class Builder {

		private Long id;
		private String username;
		private String firstName;
		private String lastName;
		private String password;
		private List<UserRole> roles;
		private SocialMediaService socialSignInProvider;
		private Set<GrantedAuthority> authorities;
		private User internalUser;

		public Builder() {
			this.authorities = new HashSet<>();
		}

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder password(String password) {
			if (password == null) {
				password = "SocialUser";
			}

			this.password = password;
			return this;
		}

		public Builder roles(List<UserRole> roles) {
			this.roles = roles;

			// SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
			// StringUtils.join(roles, ","));
			for (UserRole role : roles) {
				this.authorities
						.add(new SimpleGrantedAuthority(role.toString()));
			}

			return this;
		}

		public Builder socialSignInProvider(SocialMediaService socialSignInProvider) {
			this.socialSignInProvider = socialSignInProvider;
			return this;
		}

		public Builder username(String username) {
			this.username = username;
			return this;
		}

		public Builder internalUser(User internalUser) {
			this.internalUser = internalUser;
			return this;
		}

		public LMFUserDetails build() {
			LMFUserDetails user = new LMFUserDetails(username, password, authorities);

			user.id = id;
			user.firstName = firstName;
			user.lastName = lastName;
			user.roles = roles;
			user.socialSignInProvider = socialSignInProvider;
			user.internalUser = internalUser;

			return user;
		}
	}
}
