package it.unito.lmf.web.forms;

import java.io.Serializable;

/**
 * Base form, MUST be implemented by every form. Supplying utilities.
 * 
 * @author Biava Lorenzo
 *
 */
public abstract class BaseForm implements Serializable {

	private static final long serialVersionUID = 396538686622477339L;

}
