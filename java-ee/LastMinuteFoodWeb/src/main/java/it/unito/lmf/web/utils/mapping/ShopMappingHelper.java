package it.unito.lmf.web.utils.mapping;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.dal.entities.shop.ShopCategory;
import it.unito.lmf.domain.dsl.deal.SellerRepresentation;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShopMappingHelper extends
		MappingHelperBase<Shop, ShopRepresentation> {
	
	@Autowired
	MappingHelperBase<Seller, SellerRepresentation> sellerMH;

	@Override
	public Shop getEntity(ShopRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public ShopRepresentation getDSL(Shop o) {
		if(o==null) return null;
		ShopRepresentation dsl = new ShopRepresentation();

		dsl.setId(o.getId());
		List<String> categories = new ArrayList<String>();
		for (ShopCategory item : o.getCategories())
			categories.add(item.getName());
		dsl.setCategories(categories);
		//dsl.setCreatedAt(o.getCreatedAt());
		dsl.setDescription(o.getDescription());
		dsl.setImage(o.getImage());
		dsl.setLatitude(o.getLatitude());
		dsl.setLongitude(o.getLongitude());
		dsl.setName(o.getName());
		dsl.setOwner(sellerMH.getDSL(o.getOwner()));
		dsl.setPartitaIVA(o.getPartitaIVA());
		dsl.setPhoneNumber(o.getPhoneNumber());		

		return dsl;
	}
}
