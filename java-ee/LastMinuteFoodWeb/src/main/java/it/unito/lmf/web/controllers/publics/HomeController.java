package it.unito.lmf.web.controllers.publics;

import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.security.LMFUserDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController extends BaseController {

	Logger LOG = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String landingPage(Model model, @AuthenticationPrincipal LMFUserDetails user) {
		return "pages/landing";
	}
	
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String aboutPage(Model model) {
		return "pages/home";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homePageAtuhenticated(Model model) {
		//return getTemplatePath("authenticated_home");
		return "redirect:deals";
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(Model model) {
		return "layouts/authenticated-layout";
	}
}