package it.unito.lmf.web.misc.pagination;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.Assert;

/**
 * Utility class to add metadata to {@link Page} data structure, for view
 * rendering facilitations (i.e. url for other pages).
 * 
 * @author Biava Lorenzo
 *
 * @param <T>
 */
public class PageWrapper<T> implements Page<T> {

	public static final String QUERY_PARAM_SORT = "sort";
	public static final String QUERY_PARAM_PAGE = "page";
	public static final String QUERY_PARAM_SIZE = "size";

	// public static final int MAX_PAGE_ITEM_DISPLAY = 5;
	private Page<T> page;
	// private List<PageItem> items;
	// private int currentNumber;
	private String url;

	private List<SortableField> sortableFields;

	/**
	 * 
	 * @param page
	 *            the original {@link Page} containing results and pagination
	 *            settings.
	 * @param url
	 *            the base URL to add pagination settings for other pages.
	 * @throws MalformedURLException
	 */
	public PageWrapper(Page<T> page, String url,
			List<SortableField> sortableFields) {
		Assert.notNull(page);
		Assert.notNull(url);
		if (sortableFields == null)
			sortableFields = new ArrayList<SortableField>();

		this.page = page;
		this.url = url;
		this.sortableFields = sortableFields;

		// items = new ArrayList<PageItem>();
		//
		// currentNumber = page.getNumber() + 1; // start from 1 to match
		// page.page
		//
		// int start, size;
		// if (page.getTotalPages() <= MAX_PAGE_ITEM_DISPLAY) {
		// start = 1;
		// size = page.getTotalPages();
		// } else {
		// if (currentNumber <= MAX_PAGE_ITEM_DISPLAY - MAX_PAGE_ITEM_DISPLAY
		// / 2) {
		// start = 1;
		// size = MAX_PAGE_ITEM_DISPLAY;
		// } else if (currentNumber >= page.getTotalPages()
		// - MAX_PAGE_ITEM_DISPLAY / 2) {
		// start = page.getTotalPages() - MAX_PAGE_ITEM_DISPLAY + 1;
		// size = MAX_PAGE_ITEM_DISPLAY;
		// } else {
		// start = currentNumber - MAX_PAGE_ITEM_DISPLAY / 2;
		// size = MAX_PAGE_ITEM_DISPLAY;
		// }
		// }
		//
		// for (int i = 0; i < size; i++) {
		// items.add(new PageItem(start + i, (start + i) == currentNumber));
		// }
	}

	public PageWrapper(Page<T> page, String url) {
		this(page, url, null);
	}

	public Page<T> getPage() {
		return page;
	}

	public List<SortableField> getSortableFields() {
		return sortableFields;
	}

	public String getUrl() {
		return url;
	}

	public String currentPageUrl() {
		UriBuilder u = UriBuilder.fromPath(url)
				.queryParam(QUERY_PARAM_PAGE, getNumber())
				.queryParam(QUERY_PARAM_SIZE, getSize());

		return addSortingParams(u).build().toString();
	}

	public String nextPageUrl() {
		UriBuilder u = UriBuilder.fromPath(url)
				.queryParam(QUERY_PARAM_PAGE, nextPageable().getPageNumber())
				.queryParam(QUERY_PARAM_SIZE, nextPageable().getPageSize());

		return addSortingParams(u).build().toString();
	}

	public String previousPageUrl() {
		UriBuilder u = UriBuilder
				.fromPath(url)
				.queryParam(QUERY_PARAM_PAGE,
						previousPageable().getPageNumber())
				.queryParam(QUERY_PARAM_SIZE, previousPageable().getPageSize());

		return addSortingParams(u).build().toString();
	}

	public String firstPageUrl() {
		UriBuilder u = UriBuilder.fromPath(url)
				.queryParam(QUERY_PARAM_PAGE, "0")
				.queryParam(QUERY_PARAM_SIZE, page.getSize());

		return addSortingParams(u).build().toString();
	}

	public String lastPageUrl() {
		UriBuilder u = UriBuilder.fromPath(url)
				.queryParam(QUERY_PARAM_PAGE, page.getTotalPages() - 1)
				.queryParam(QUERY_PARAM_SIZE, page.getSize());

		return addSortingParams(u).build().toString();
	}

	protected UriBuilder addSortingParams(UriBuilder u) {
		List<String> params = new ArrayList<String>();
		if (page.getSort() != null)
			for (Order item : page.getSort())
				params.add(item.getProperty() + "," + item.getDirection());
		
		u.queryParam(QUERY_PARAM_SORT, params.toArray());
		return u;
	}

	// public List<PageItem> getItems() {
	// return items;
	// }

	public int getNumber() {
		return page.getNumber();
	}

	public List<T> getContent() {
		return page.getContent();
	}

	public int getSize() {
		return page.getSize();
	}

	public int getTotalPages() {
		return page.getTotalPages();
	}

	public boolean isFirst() {
		return page.isFirst();
	}

	public boolean isLast() {
		return page.isLast();
	}

	public boolean hasPrevious() {
		return page.hasPrevious();
	}

	public boolean hasNext() {
		return page.hasNext();
	}

	@Override
	public int getNumberOfElements() {
		return page.getNumberOfElements();
	}

	@Override
	public boolean hasContent() {
		return page.hasContent();
	}

	@Override
	public Sort getSort() {
		return page.getSort();
	}

	@Override
	public Pageable nextPageable() {
		return page.nextPageable();
	}

	@Override
	public Pageable previousPageable() {
		return page.previousPageable();
	}

	@Override
	public Iterator<T> iterator() {
		return page.iterator();
	}

	@Override
	public long getTotalElements() {
		return page.getTotalElements();
	}

	// public class PageItem {
	// private int number;
	// private boolean current;
	//
	// public PageItem(int number, boolean current) {
	// this.number = number;
	// this.current = current;
	// }
	//
	// public int getNumber() {
	// return this.number;
	// }
	//
	// public boolean isCurrent() {
	// return this.current;
	// }
	// }
}