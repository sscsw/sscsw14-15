package it.unito.lmf.web.controllers.shop;

import it.unito.lmf.biz.deal.DealMgmtBean;
import it.unito.lmf.biz.shop.ShopMgmtBean;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.dal.entities.shop.ShopCategory;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.controllers.exceptions.RedirectException;
import it.unito.lmf.web.forms.shop.ShopRequestForm;
import it.unito.lmf.web.misc.pagination.SortableField;
import it.unito.lmf.web.security.LMFUserDetails;

import java.io.ByteArrayInputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("shops")
public class ShopController extends BaseController {

	@EJB(mappedName = DealMgmtBean.JNDIName)
	DealMgmtBean dealBean;

	@EJB(mappedName = ShopMgmtBean.JNDIName)
	ShopMgmtBean shopBean;

	@Secured("ROLE_SELLER")
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String showNewShopPage(Model model, ShopRequestForm shopRequestForm) {
		if (shopRequestForm == null)
			shopRequestForm = new ShopRequestForm();

		model.addAttribute("shopRequestForm", shopRequestForm);
		model.addAttribute("formAction", "/shops/new");

		Collection<ShopCategory> shopCategories = shopBean
				.getAllShopCategories();
		model.addAttribute("shopCategories", shopCategories);

		return getTemplatePath("new_shop");
	}

	@Secured("ROLE_SELLER")
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String createNewShop(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@ModelAttribute @Valid ShopRequestForm shopRequestForm,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return showNewShopPage(model, shopRequestForm);
		}

		Shop shop = new Shop();
		
		try {
			shop.setAddress(shopRequestForm.getAddress());
			shop.setDescription(shopRequestForm.getDescription());
			shop.setName(shopRequestForm.getName());
			shop.setPartitaIVA(shopRequestForm.getPartitaIVA());
			shop.setPhoneNumber(shopRequestForm.getPhoneNumber());
			
			shop = shopBean.createNewShop(shop, user.getId(),
					shopRequestForm.getCategories(),new ByteArrayInputStream(
							shopRequestForm.getPicture().getBytes()), shopRequestForm
							.getPicture().getContentType());
		} catch (Exception e) {
			model.addAttribute("errorMsg", "Unable to crate the shop...");
			return showNewShopPage(model, shopRequestForm);
		}

		return "redirect:/shops/" + shop.getId();
	}

	@Secured("ROLE_SELLER")
	@RequestMapping(value = "/my", method = RequestMethod.GET)
	public String showOwnerShopsList(Model model,
			@AuthenticationPrincipal LMFUserDetails user, Pageable pageable)
			throws RedirectException {

		List<SortableField> sortableFields = new ArrayList<SortableField>();
		// sortableFields.add(new SortableField("name", "Name"));
		// sortableFields.add(new SortableField("price", "Price"));
		// sortableFields.add(new SortableField("discountPercentage",
		// "Discount Percentage"));
		// sortableFields.add(new SortableField("createdAt", "Creation Date"));
		// sortableFields.add(new SortableField("expireDate",
		// "Expiration Date"));

		checkSortableFields(pageable, sortableFields);

		Page<Shop> page = shopBean.getShopsByOwner(user.getId(), pageable);

		model.addAttribute("shops", page.getContent());
		model.addAttribute("page",
				getPageWrapper(page, "/shops/my", sortableFields));

		return getTemplatePath("shops_list_owner");
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showShopsList(Model model,
			@AuthenticationPrincipal LMFUserDetails user, Pageable pageable)
			throws RedirectException {

		List<SortableField> sortableFields = new ArrayList<SortableField>();
		// sortableFields.add(new SortableField("name", "Name"));
		// sortableFields.add(new SortableField("price", "Price"));
		// sortableFields.add(new SortableField("discountPercentage",
		// "Discount Percentage"));
		// sortableFields.add(new SortableField("createdAt", "Creation Date"));
		// sortableFields.add(new SortableField("expireDate",
		// "Expiration Date"));

		checkSortableFields(pageable, sortableFields);

		Page<Shop> page = shopBean.getAllAuthorizedShops(pageable);

		model.addAttribute("shops", page.getContent());
		model.addAttribute("page",
				getPageWrapper(page, "/shops", sortableFields));

		return getTemplatePath("shops_list");
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/{shop-id}", method = RequestMethod.GET)
	public String showShopDetails(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@PathVariable("shop-id") Long shopId, Pageable pageable)
			throws RedirectException {
		
		List<SortableField> sortableFields = new ArrayList<SortableField>();
		// sortableFields.add(new SortableField("name", "Name"));
		// sortableFields.add(new SortableField("price", "Price"));
		// sortableFields.add(new SortableField("discountPercentage",
		// "Discount Percentage"));
		// sortableFields.add(new SortableField("createdAt", "Creation Date"));
		// sortableFields.add(new SortableField("expireDate",
		// "Expiration Date"));

		checkSortableFields(pageable, sortableFields);

		Shop shop = checkResourceExists(shopBean.getShopById(shopId));

		model.addAttribute("shop", shop);

		Page<Deal> page = dealBean.getDealsByShop(shopId, pageable);

		model.addAttribute("deals", page.getContent());
		model.addAttribute("page",
				getPageWrapper(page, "/shops/" + shopId, sortableFields));

		
		if (shop.getOwner().getId() == user.getInternalUser().getId()) {
			// TODO: impostare URL sensata
			model.addAttribute("QrCodeUrl", "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + URLEncoder.encode("http://www.facebook.it"));
			return getTemplatePath("shop_details_owner");
		}
		else if (shop.isAuthorized())
			return getTemplatePath("shop_details");
		else
			return getTemplatePath("unreachable_shop");
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/{shop-id}", method = RequestMethod.POST)
	public String editShopDetails(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@PathVariable("shop-id") Long shopId,
			@ModelAttribute @Valid ShopRequestForm shopRequestForm,
			BindingResult bindingResult) {

		// TODO
		return null;
		// if (bindingResult.hasErrors()) {
		// return showShopDetails(model, shopRequestForm);
		// }
		//
		// try {
		// Shop shop = new Shop();
		// shop.setAddress(shopRequestForm.getAddress());
		// shop.setDescription(shopRequestForm.getDescription());
		// shop.setName(shopRequestForm.getName());
		// shop.setPartitaIVA(shopRequestForm.getPartitaIVA());
		// shop.setPhoneNumber(shopRequestForm.getPhoneNumber());
		//
		// shopBean.createNewShop(shop, user.getId(),
		// shopRequestForm.getCategories());
		// } catch (Exception e) {
		// model.addAttribute("errorMsg", "Unable to crate the shop...");
		// return showNewShopPage(model, shopRequestForm);
		// }
		// return getTemplatePath("shop_details");
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/feedback/{code}", method = RequestMethod.GET)
	public String showShopsList(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@PathVariable("code") Long code) {

		model.addAttribute("shop",
				checkResourceExists(shopBean.getShopById(code)));

		return getTemplatePath("shop_feedback");
	}

}