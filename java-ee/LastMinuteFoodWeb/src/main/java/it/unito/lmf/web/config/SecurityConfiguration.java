package it.unito.lmf.web.config;

import it.unito.lmf.web.security.DBUserDetailsService;
import it.unito.lmf.web.security.LMFAccessDeniedHandler;
import it.unito.lmf.web.security.LMFSocialUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * Configuration class for all the security realms of Spring Security. <br/>
 * Note that there are two realms: one unauthenticated for RESTful WS (/rest/*)
 * and one for the website (/web/*).
 * 
 * @author Biava Lorenzo
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {

	public static final String ROUTE_LOGIN_SUCCESFUL=AppConfiguration.WEB_APP_URL_PREFIX+"/home";
	
	/**
	 * Configures the {@link AuthenticationManager} for using the custom
	 * {@link DBUserDetailsService} and related {@link PasswordEncoder}.
	 * 
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(
				passwordEncoder());
	}

	// RESTful WS branch configuration
	@Configuration
	@Order(1)
	public static class RestufulWSWebSecurityConfigurationAdapter extends
			WebSecurityConfigurerAdapter {
		protected void configure(HttpSecurity http) throws Exception {
			http.antMatcher("/rest/**").authorizeRequests().anyRequest()
					.permitAll();
		}
	}

	@Configuration
	@Order(2)
	public static class WebsiteWebSecurityConfigurerAdapter extends
			WebSecurityConfigurerAdapter {

		@Override
		public void configure(WebSecurity web) throws Exception {
			String PREFIX = AppConfiguration.WEB_APP_URL_PREFIX;

			web.ignoring()
			// Spring Security should completely ignore URLs starting with
			// /resources/
					.antMatchers(PREFIX + "/static/**");
			// .antMatchers("/**");
		}

		/**
		 * Defines the web based security configuration.
		 * 
		 * @param http
		 *            It allows configuring web based security for specific http
		 *            requests.
		 * @throws Exception
		 */
		@Override
		protected void configure(HttpSecurity http) throws Exception {

			String PREFIX = AppConfiguration.WEB_APP_URL_PREFIX;
			// @formatter:off
			http.antMatcher(PREFIX + "/**")
					.authorizeRequests()
					// Routes privileges
					.antMatchers(PREFIX + "/", PREFIX + "/about",
							PREFIX + "/signup", PREFIX + "/registration",
							PREFIX + "/login", PREFIX + "/home", PREFIX + "/deals")
					.permitAll()
					// .antMatchers("/admin/**").hasRole("ADMIN")
					// .antMatchers("/seller/**").hasRole("SELLER")
					.anyRequest()
					.authenticated()
					.and()
					// Errors pages overrides
					.exceptionHandling()
					.accessDeniedHandler(lmfAccessDeniedHandler())
					.accessDeniedPage(PREFIX + "/errors/access_denied")
					.and()
					// Logout
					.logout()
					.logoutUrl(PREFIX + "/logout")
					.logoutRequestMatcher(
							new AntPathRequestMatcher(PREFIX + "/logout", "GET"))
					.logoutSuccessUrl(PREFIX + "/")
					.and()
					// Login
					.formLogin()
					.usernameParameter("username")
					.passwordParameter("password")
					.loginPage(PREFIX + "/login?error=required")
					.failureUrl(PREFIX + "/login?error=invalid")
					.loginProcessingUrl(PREFIX + "/login")
					.defaultSuccessUrl(ROUTE_LOGIN_SUCCESFUL)
					.permitAll()
					.and()
					// Adds the SocialAuthenticationFilter to Spring Security's
					// filter chain.
					.apply(new SpringSocialConfigurer().postLoginUrl(ROUTE_LOGIN_SUCCESFUL));
			// @formatter:on
		}

		/**
		 * Produces a LMFAccessDeniedHandler.
		 * 
		 * @return
		 */
		@Bean
		public LMFAccessDeniedHandler lmfAccessDeniedHandler() {
			return new LMFAccessDeniedHandler();
		}
	}

	/**
	 * Produces a password encoder for users password encryption.
	 * 
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		//return NoOpPasswordEncoder.getInstance();
		// TODO: Ripristinare quello con hash
		return new BCryptPasswordEncoder(10);
	}

	@Bean
	public SocialUserDetailsService socialUserDetailsService() {
		return new LMFSocialUserDetailsService();
	}

	/**
	 * Produces a DB implementation of a {@link UserDetailsService}.
	 * 
	 * @return
	 */
	@Bean
	public UserDetailsService userDetailsService() {
		return new DBUserDetailsService();
	}
}