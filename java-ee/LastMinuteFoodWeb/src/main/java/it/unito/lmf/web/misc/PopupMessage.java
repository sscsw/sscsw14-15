package it.unito.lmf.web.misc;

public class PopupMessage {
	public enum MessageType {
		INFO, ERROR, WARN, SUCCESS
	}

	private String message;
	private MessageType type;

	public PopupMessage(String message, MessageType type) {
		super();
		this.message = message;
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

}
