package it.unito.lmf.web.controllers.accounting;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.accounting.exceptions.EmailAlreadyInUseException;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.util.geocoding.GeocodingHelper;
import it.unito.lmf.web.config.SecurityConfiguration;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.forms.accounting.RegistrationForm;
import it.unito.lmf.web.security.DBUserDetailsService;
import it.unito.lmf.web.security.SecurityUtils;
import it.unito.lmf.web.security.SocialMediaService;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
//@SessionAttributes("user")
public class RegistrationController extends BaseController {
	private static final Logger LOG = LoggerFactory
			.getLogger(RegistrationController.class);

	private static final String VIEW_NAME_REGISTRATION_PAGE = "basic_registration";

	@EJB(mappedName = UserMgmtBean.JNDIName)
	UserMgmtBean userBean;

	@Autowired
	private DBUserDetailsService userDetailsService;

	/**
	 * Redirects request forward to the registration page. This hack is required
	 * because there is no way to set the sign in url to the
	 * SocialAuthenticationFilter class. Another option is to move registration
	 * page to to url '/signup' but I did not want to do that because that url
	 * looks a bit ugly to me.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String redirectRequestToRegistrationPage() {
		LOG.debug("Redirecting request to registration page.");

		return "redirect:/registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String showRegistrationPage(
			org.springframework.web.context.request.WebRequest request,
			Model model, 
			@RequestParam(required = false) String error) {
		LOG.info("Registration page requested.");

		if (error != null && getPageError(model) == null) {
			String errorMsg = "";
			switch (error) {
			default:
				errorMsg = "Error during registration.";
			}
			setPageError(model, errorMsg);
		}
		
		org.springframework.social.connect.Connection<?> connection = new ProviderSignInUtils().getConnectionFromSession(request);
		org.springframework.social.connect.UserProfile socialMediaProfile = null;
		
		
		RegistrationForm registrationForm = new RegistrationForm();
		model.addAttribute("user", registrationForm);
		
		
		if (connection != null) { // user connected with Social Network
			org.springframework.social.connect.ConnectionKey providerKey = connection.getKey();
			SocialMediaService signInProvider = SocialMediaService.UNKNOWN;
			
			try{
				signInProvider = SocialMediaService.valueOf(providerKey.getProviderId().toUpperCase());
			}
			catch (Exception e) {
			}

			socialMediaProfile = connection.fetchUserProfile();
			if (socialMediaProfile != null) {
				User user = userBean.getUserByEmail(socialMediaProfile.getEmail());
				
			
				// User already registered -> LOGIN and save connection to the UserConnection table (necessary if the user was registered with email+password or another social)
				if (user != null) { 
					/****************
					 * serve?
					 ******************
					org.springframework.social.connect.ConnectionKey providerKey = connection.getKey();
					SocialMediaService signInProvider = it.unito.lmf.web.security.SocialMediaService
							.valueOf(providerKey.getProviderId().toUpperCase());
					user.setSignInProvider(signInProvider.toString());
	
					user = userBean.updateUser(user);
					*/
					SecurityUtils.logInUser(userDetailsService.loadUserByUsername(socialMediaProfile.getEmail()));

					// If the user is signing in by using a social provider, this method call stores the connection to the UserConnection table. 
					// Otherwise, this method does not do anything.
					new ProviderSignInUtils().doPostSignUp(socialMediaProfile.getEmail(), request);
					
					return "redirect:"+SecurityConfiguration.ROUTE_LOGIN_SUCCESFUL;
				}
				else { // NEW USER signed-up with Social -> register
					registrationForm.setEmail(socialMediaProfile.getEmail());
					registrationForm.setFirstName(socialMediaProfile.getFirstName());
					registrationForm.setLastName(socialMediaProfile.getLastName());
					// ATTENTION: This is because some SN (as Facebook) don't allow access Username
					registrationForm.setUsername(socialMediaProfile.getEmail());
					registrationForm.setSignInProvider(signInProvider);
					
					return registerUserAccount(model, registrationForm, new DirectFieldBindingResult(user, "user"), request);
				}
			}
		}

		// user NOT connected with Social (or no user connected)
		return getTemplatePath(VIEW_NAME_REGISTRATION_PAGE);
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registerUserAccount(Model model,
			@Valid @ModelAttribute("user") RegistrationForm userAccountData,
			BindingResult result,
			org.springframework.web.context.request.WebRequest request) {
		LOG.debug("Registering user account with information: {}", userAccountData);
		if (result.hasErrors()) {
			LOG.debug("Validation errors found. Rendering form view.");
			return getTemplatePath(VIEW_NAME_REGISTRATION_PAGE);
		}

		LOG.debug("No validation errors found. Continuing registration process.");

		User registered=null;
		try {
			registered = createUserAccount(userAccountData);	
		} catch (EJBException ejbE) {
			// Catch the expected EJBException
			Throwable t = extractInnerEJBException(ejbE);
			if (t instanceof EmailAlreadyInUseException) {
				// Mail already used
				addFieldError(RegistrationForm.class.toString() , RegistrationForm.FIELD_NAME_EMAIL,
					((EmailAlreadyInUseException) t).getEmail(), "The email address has already been used.", result);
				return getTemplatePath(VIEW_NAME_REGISTRATION_PAGE);
			} else if (t instanceof UsernameAlreadyInUseException) {
				// Username already used
				addFieldError(RegistrationForm.class.toString() , RegistrationForm.FIELD_NAME_USERNAME,
					((UsernameAlreadyInUseException) t).getUsername(), "The username has already been used.", result);
				return getTemplatePath(VIEW_NAME_REGISTRATION_PAGE);
			}
			throw ejbE;
		} catch (Exception e) {
			// Generic exception
			LOG.warn("Registration exception occurred:" + e.getMessage(), e);
			setPageError(model, "An unexpected error has occurred.");
			return getTemplatePath(VIEW_NAME_REGISTRATION_PAGE);
		}

		LOG.debug("Registered user account with information: {}", registered);
		
		SocialMediaService signInProvider = SocialMediaService.UNKNOWN;
		try{
			signInProvider = SocialMediaService.valueOf(registered.getSignInProvider());
		}
		catch (Exception e) {
		}
		
		if (signInProvider != SocialMediaService.UNKNOWN) {
			
			ProviderSignInUtils socialSignInUtils = new ProviderSignInUtils();
			org.springframework.social.connect.Connection<?> connection = socialSignInUtils.getConnectionFromSession(request);
			
			
			String profileImgUrl = null;
			
			try { 
				profileImgUrl = connection.getImageUrl();
			} catch (Exception e) {
				
			}
			
			if (profileImgUrl != null)
				registered.setImage(profileImgUrl);
				
			if (signInProvider == SocialMediaService.FACEBOOK) { // Current Location
				Facebook facebook = (Facebook) connection.getApi();								
				FacebookProfile profile = facebook.userOperations().getUserProfile();
				
				org.springframework.social.facebook.api.Reference actualLocation = profile.getLocation();
				LOG.debug("User location from FB: " + (actualLocation != null ? actualLocation.getName() : null));
				
				String address = actualLocation.getName();
				it.unito.lmf.domain.dsl.external.google.geocoding.Location location = null;
				try {
					location = GeocodingHelper.getLocation(address);
				} catch (Exception e) {
					address = null;
				}
				
				if (address != null) {
					registered.setAddress(address);
					registered.setLatitude(location.getLat());
					registered.setLongitude(location.getLng());
				}
			}
			
			// If the user is signing in by using a social provider, this method call stores the connection to the UserConnection table. 
			// Otherwise, this method does not do anything.
			socialSignInUtils.doPostSignUp(registered.getEmail(), request);
			
			try {
				registered = userBean.updateUser(registered);
			} catch (Exception e) {
				
			}
		}
		
		// Logs the user in.
		// TODO: automatic login only if user signs in with social?
		// It is not a good idea to log in a user who has created a normal user
		// account. Typically you want to send a confirmation email which is
		// used to verify his email address.

		SecurityUtils.logInUser(userDetailsService.loadUserByUsername(registered.getEmail()));
		LOG.debug("User {} has been signed in", registered);
				
		return "redirect:" + SecurityConfiguration.ROUTE_LOGIN_SUCCESFUL;
	}

	protected RuntimeException extractInnerEJBException(EJBException ejbEx) {
		Throwable ex = ejbEx.getCause();
		while (ex != null) {
			if (!(ex instanceof EJBException)) {
				return (RuntimeException) ex;
			}
			ex = ex.getCause();
		}
		return null;
	}
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Creates a new user account by calling the service method. If the email
	 * address is found from the database, this method adds a field error to the
	 * email field of the form object.
	 */
	private User createUserAccount(RegistrationForm userAccountData) {
		LOG.debug("Creating user account with information: {}", userAccountData);

		User u = new User();
		u.setUsername(userAccountData.getUsername());
		u.setEmail(userAccountData.getEmail());
		u.setFirstName(userAccountData.getFirstName());
		u.setLastName(userAccountData.getLastName());

		// Hashing password with environment PasswordEncoder
		u.setPassword((userAccountData.getPassword() == null ? null
				: passwordEncoder.encode(userAccountData.getPassword())));
		u.setSignInProvider((userAccountData.getSignInProvider() != null ? userAccountData
				.getSignInProvider().name() : null));

		User registered = null;

		registered = userBean.createNewUser(u);

		return registered;
	}
	
	private void addFieldError(String objectName, String fieldName, String fieldValue,  String errorCode, BindingResult result) {
        FieldError error = new FieldError(
                objectName,
                fieldName,
                fieldValue,
                false,
                new String[]{errorCode},
                new Object[]{},
                errorCode
        );

        result.addError(error);
    }
	
}
