package it.unito.lmf.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetailsService;

public class LMFSocialUserDetailsService implements SocialUserDetailsService {

	// @EJB(mappedName = UserMgmtBean.JNDIName)
	// UserMgmtBean userBean;

	@Autowired
	private DBUserDetailsService dbUserDetailsService;

	@Override
	public LMFUserDetails loadUserByUserId(String userId)
			throws UsernameNotFoundException, DataAccessException {
		// User user = userBean.getUserByEmail(userId); // TODO: cercare per ID
		// del
		// // social?
		//
		// if (user == null || !user.isEnabled()) {
		// throw new UsernameNotFoundException("No user found with userId: "
		// + userId);
		// }
		//
		// List<UserRole> roles = new ArrayList<UserRole>();
		// roles.add(UserRole.ROLE_USER);
		// if (user.isAdmin())
		// roles.add(UserRole.ROLE_ADMIN);
		// if (user.isSeller() && user.getSeller().isAuthorized())
		// roles.add(UserRole.ROLE_SELLER);

		LMFUserDetails principal = dbUserDetailsService
				.loadUserByUsername(userId);

		return principal;
	}
}
