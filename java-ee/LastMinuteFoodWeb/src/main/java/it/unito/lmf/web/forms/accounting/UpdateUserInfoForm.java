package it.unito.lmf.web.forms.accounting;

import it.unito.lmf.web.validation.file.PictureFile;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class UpdateUserInfoForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5216992669716621977L;

	public static final String FIELD_NAME_ADDRESS = "address";
	public static final String FIELD_NAME_USERNAME = "username";
	
	// user can modify his/her username only once and only if logs-in with a social network
	// if it's empty (or null) user don't change its username
	@Size(max=255)
	private String username;
    
    @NotEmpty
    @Size(max = 100)
    private String firstName;

    @NotEmpty
    @Size(max = 100)
    private String lastName;

	@PictureFile(maxSize=1024000, message="Max size: 1 MB", canBeEmpty=true)
	private MultipartFile picture; 
	
	@NotEmpty
	@Size(max = 255)
	private String address;
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public MultipartFile getPicture() {
		return picture;
	}

	public void setPicture(MultipartFile picture) {
		this.picture = picture;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
