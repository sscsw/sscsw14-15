package it.unito.lmf.web.controllers.utils;

import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.security.LMFAccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

@Controller
@RequestMapping("/errors")
public class ErrorController extends BaseController {

	private static final Logger LOG = LoggerFactory
			.getLogger(ErrorController.class);

	@RequestMapping(value = "/access_denied")
	public String showAccessDeniedPage(Model model,
			@AuthenticationPrincipal User user, HttpServletRequest request,
			NativeWebRequest webRequest) {
		LOG.info("Access Denied page requested.");

		HttpServletRequest originalRequest = (HttpServletRequest) request
				.getAttribute(LMFAccessDeniedHandler.ACCESS_DENIED_HANDLER_ORIGINAL_REQUEST);
		if (originalRequest != null) {
			originalRequest.getServletPath();
		}

		AccessDeniedException adEx = (AccessDeniedException) webRequest
				.getAttribute(WebAttributes.ACCESS_DENIED_403, 1);

		if (user != null)
			model.addAttribute(
					"errorMessage",
					"Siamo spiacenti "
							+ user.getUsername()
							+ ", non hai i permessi per visitare questa pagina !");
		else
			model.addAttribute("errorMessage",
					"Non hai i permessi per visitare questa pagina !");

		return getTemplatePath("access_denied");
	}

	@RequestMapping(value = "/{error-code}")
	public String showAccessDeniedPage(Model model,
			@PathVariable("error-code") int errorCode,
			HttpServletRequest request) {
		model.addAttribute("exception",request.getAttribute("javax.servlet.error.exception"));
		return getTemplatePath("" + errorCode);
	}
}