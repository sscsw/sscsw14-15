package it.unito.lmf.web.controllers.experimental;

import it.unito.lmf.domain.dsl.external.google.geocoding.GeocodingResponse;
import it.unito.lmf.domain.dsl.external.google.geocoding.Location;
import it.unito.lmf.libs.external.google.geocoding.GoogleGeocodingAPIClient;
import it.unito.lmf.libs.external.google.geocoding.GoogleGeocodingAPIException;
import it.unito.lmf.web.controllers.BaseController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/experimental")
public class ExperimentalController extends BaseController {

	private static final Logger LOG = LoggerFactory
			.getLogger(ExperimentalController.class);

	@RequestMapping(value = "/geocoding/address", method = RequestMethod.GET)
	public @ResponseBody String getAddress(Model model,
			@RequestParam("lat") Double lat, @RequestParam("lng") Double lng)
			throws GoogleGeocodingAPIException, Exception {

		GoogleGeocodingAPIClient client = new GoogleGeocodingAPIClient(
				GoogleGeocodingAPIClient.API_KEY);
		GeocodingResponse resp = client.getAddress(lat, lng);
		if (resp.getResults().size() > 0)
			return resp.getResults().get(0).getFormattedAddress();
		else
			return "ZERO_RESULTS";

	}

	@RequestMapping(value = "/geocoding/latlng", method = RequestMethod.GET)
	public @ResponseBody Location getLatLng(Model model,
			@RequestParam("address") String address)
			throws GoogleGeocodingAPIException, Exception {

		GoogleGeocodingAPIClient client = new GoogleGeocodingAPIClient(
				GoogleGeocodingAPIClient.API_KEY);
		return client.getLatLng(address).getResults().get(0).getGeometry()
				.getLocation();

	}

}