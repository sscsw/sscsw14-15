package it.unito.lmf.web.utils.mapping;

import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.notification.DealNotificationRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DealNotificationMappingHelper extends
		MappingHelperBase<DealNotification, DealNotificationRepresentation> {

	// @Autowired
	// MappingHelperBase<User, UserRepresentation> userMH;

	@Autowired
	MappingHelperBase<Deal, DealRepresentation> dealMH;

	@Override
	public DealNotification getEntity(DealNotificationRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public DealNotificationRepresentation getDSL(DealNotification o) {
		if(o==null) return null;
		DealNotificationRepresentation dsl = new DealNotificationRepresentation();

		dsl.setCreatedAt(o.getCreatedAt().getTime());
		dsl.setDeal(dealMH.getDSL(o.getDeal()));
		dsl.setEvent(o.getEvent());
		dsl.setId(o.getId());
		dsl.setRead(o.isRead());
		dsl.setUser(null);
		dsl.setVisibility(o.getVisibility());

		return dsl;
	}
}
