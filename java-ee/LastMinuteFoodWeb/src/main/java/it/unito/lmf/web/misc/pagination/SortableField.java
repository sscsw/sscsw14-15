package it.unito.lmf.web.misc.pagination;

import java.util.List;

/**
 * Sort options for the view, represented by a field {@code name},
 * {@code displayName}, {@code sortType}.
 * 
 * @author Biava Lorenzo
 *
 */
public class SortableField {

	public enum SortType {
		NONE, ASC, DESC, ASC_DESC
	}

	private String displayName;
	private String name;
	private SortType sortType;

	public SortableField(String fieldName, String displayName, SortType sortType) {
		super();
		this.displayName = displayName;
		this.name = fieldName;
		this.sortType = sortType;
	}

	public SortableField(String fieldName, String displayName) {
		super();
		this.displayName = displayName;
		this.name = fieldName;
		this.sortType = SortType.ASC_DESC;
	}

	public SortableField(String fieldName) {
		super();
		this.displayName = fieldName;
		this.name = fieldName;
		this.sortType = SortType.ASC_DESC;
	}

	public boolean canSortAsc() {
		return this.sortType.equals(SortType.ASC) || this.sortType.equals(SortType.ASC_DESC);
	}
	
	public boolean canSortDesc() {
		return this.sortType.equals(SortType.DESC) || this.sortType.equals(SortType.ASC_DESC);
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SortType getSortType() {
		return sortType;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	public boolean isEnabled() {
		return sortType != SortType.NONE;
	}
	
	public static boolean containsSortableField(List<SortableField> sortableFields, String fieldName) {
		if(sortableFields==null)
			return false;
		
			for(SortableField field : sortableFields)
				if(field.getName().equals(fieldName))
					return true;
			
		return false;
	}
}