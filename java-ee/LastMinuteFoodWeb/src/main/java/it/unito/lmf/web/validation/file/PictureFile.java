package it.unito.lmf.web.validation.file;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.swing.text.StyledEditorKit.BoldAction;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validates a picture file upload.
 * 
 * @author Biava Lorenzo
 *
 */
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE,
		ElementType.CONSTRUCTOR, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { PictureFileValidator.class })
@Documented
public @interface PictureFile {

	/**
	 * Max picture size in bytes.<br/>
	 * Default: 2048000 bytes.
	 * 
	 * @return
	 */
	long maxSize() default 2048000;

	/**
	 * Valid picture formats.<br/>
	 * Default: jpeg, png.
	 * 
	 * @return
	 */
	String[] formats() default { "jpeg", "png" };

	String message() default "{pictureFile.message}";
	
	boolean canBeEmpty() default false;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
