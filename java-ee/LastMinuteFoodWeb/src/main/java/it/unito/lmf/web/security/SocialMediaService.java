package it.unito.lmf.web.security;

/**
 * Represents the possible Socia Network type, available for authentication.
 * 
 * @author Biava Lorenzo
 *
 */
public enum SocialMediaService {
		FACEBOOK
	//, 	TWITTER
		,UNKNOWN
}