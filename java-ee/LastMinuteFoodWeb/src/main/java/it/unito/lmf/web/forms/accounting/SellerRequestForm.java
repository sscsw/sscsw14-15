package it.unito.lmf.web.forms.accounting;

import it.unito.lmf.utils.validation.RegexpRepository;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class SellerRequestForm implements Serializable {
	
	private static final long serialVersionUID = -6892682303671700093L;

	@NotEmpty
	private String ragioneSociale;

	@NotEmpty
    private String codiceFiscale;

    @Pattern(regexp = RegexpRepository.REGEXP_PHONE_NUMBER)
    private String phoneNumber;

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
