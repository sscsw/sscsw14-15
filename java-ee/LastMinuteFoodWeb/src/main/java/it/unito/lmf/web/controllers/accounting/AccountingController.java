package it.unito.lmf.web.controllers.accounting;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.accounting.exceptions.UsernameAlreadyInUseException;
import it.unito.lmf.dal.utils.ExceptionManagementHelper;
import it.unito.lmf.util.geocoding.exceptions.InvalidAddressException;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.forms.accounting.ChangePasswordForm;
import it.unito.lmf.web.forms.accounting.SellerRequestForm;
import it.unito.lmf.web.forms.accounting.UpdateUserInfoForm;
import it.unito.lmf.web.security.DBUserDetailsService;
import it.unito.lmf.web.security.LMFUserDetails;
import it.unito.lmf.web.security.SecurityUtils;

import java.sql.SQLException;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/accounts")
public class AccountingController extends BaseController {

	public static enum ME_ACTIONS {
		CHANGE_INFO, CHANGE_PASSWORD, NEW_SELLER
	}

	private static final Logger LOG = LoggerFactory
			.getLogger(AccountingController.class);

	@EJB(mappedName = UserMgmtBean.JNDIName)
	UserMgmtBean userBean;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private DBUserDetailsService dbUserDetailsService;

	@Secured("ROLE_ADMIN")
	@RequestMapping("")
	public String listAllAccounts(Model model) {

		model.addAttribute("users", userBean.getAllUsers());
		return getTemplatePath("accounts");
	}

	/*
	 * @RequestMapping(value = "/registration", method = RequestMethod.GET)
	 * public String registration(Model model, @RequestParam("email") String
	 * email) {
	 * 
	 * LOG.warn("FB Registration - email: {}", email); return
	 * getTemplatePath("registration"); }
	 */

	@Secured("ROLE_USER")
	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public String showUserAccountPage(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@RequestParam(required = false) String action,
			@RequestParam(required = false) String msg) {

		// Refresh internal user seller status if updated on DB
		User updatedUser = userBean.getUserById(user.getInternalUser().getId());
		
		if ((updatedUser.isSeller() != user.getInternalUser().isSeller())
				|| (updatedUser.isSeller() && updatedUser.getSeller()
						.isAuthorized() != user.getInternalUser().getSeller()
						.isAuthorized())) {
			user=dbUserDetailsService.loadUserById(user.getId());
			SecurityUtils.logInUser(user);			
		}

		ME_ACTIONS a = null;
		try {
			a = ME_ACTIONS.valueOf(action.toUpperCase());
		} catch (Exception e) {
			a = null;
		}

		if (action != null && a == null)
			return "redirect:/accounts/me";

		if (msg != null) {
			String stringMsg = "";

			switch (a) {
			case CHANGE_INFO:
				switch (msg) {
				case "ok":
					model.addAttribute("userInfoInfoMsg",
							"Information Updated!");
					stringMsg = null;
					break;
				case "img_error":
					model.addAttribute("userInfoInfoMsg",
							"Information Updated!");
					stringMsg = "Error on image update!";
					break;
				default:
					stringMsg = "Error during information update.";
				}
				model.addAttribute("userInfoErrorMsg", stringMsg);

				break;

			case CHANGE_PASSWORD:
				switch (msg) {
				case "ok":
					model.addAttribute("passwordInfoMsg", "Password Changed!");
					stringMsg = null;
					break;
				default:
					stringMsg = "Error during password update.";
				}
				model.addAttribute("passwordErrorMsg", stringMsg);

				break;

			case NEW_SELLER:
				switch (msg) {
				case "ok":
					stringMsg = null;
					model.addAttribute("becomeSellerOkMsg", stringMsg);

					break;
				default:
					stringMsg = "Error during seller request.";
				}

				model.addAttribute("becomeSellerErrorMsg", stringMsg);

				break;

			default:
				return "redirect:/accounts/me";
			}
		}

		User internalUser = user.getInternalUser();

		model.addAttribute("user", internalUser);
		model.addAttribute("favoriteShops", internalUser.getFavoriteShops());
		model.addAttribute("favoriteDeals", internalUser.getFavoriteDeals());

		SellerRequestForm sellerRequestForm = null;
		UpdateUserInfoForm updateUserInfoForm = null;
		ChangePasswordForm changePasswordForm = null;

		if (sellerRequestForm == null)
			sellerRequestForm = new SellerRequestForm();

		if (updateUserInfoForm == null)
			updateUserInfoForm = createUserInfoForm(internalUser);

		if (changePasswordForm == null)
			changePasswordForm = new ChangePasswordForm();

		model.addAttribute("sellerRequestForm", sellerRequestForm);
		model.addAttribute("updateUserInfoForm", updateUserInfoForm);
		model.addAttribute("changePasswordForm", changePasswordForm);

		model.addAttribute("sellerRequestFormAction", "new_seller");
		model.addAttribute("updateUserInfoFormAction", "change_info");
		model.addAttribute("changePasswordFormAction", "change_password");

		return getTemplatePath("user_account");
	}

	private UpdateUserInfoForm createUserInfoForm(User user) {
		if (user == null)
			return new UpdateUserInfoForm();

		UpdateUserInfoForm f = new UpdateUserInfoForm();
		if (user.getSignInProvider() != null
				&& user.getUsername().equals(user.getEmail())) // user can
																// change its
																// username
			f.setUsername(user.getUsername());
		else
			f.setUsername(null);

		f.setFirstName(user.getFirstName());
		f.setLastName(user.getLastName());
		f.setAddress(user.getAddress());

		return f;
	}

	private String onError(Model model, User user,
			UpdateUserInfoForm updateUserInfoForm,
			ChangePasswordForm changePasswordForm,
			SellerRequestForm sellerRequestForm) {
		model.addAttribute("user", user);
		model.addAttribute("favoriteShops", user.getFavoriteShops());
		model.addAttribute("favoriteDeals", user.getFavoriteDeals());

		// reset other forms
		if (updateUserInfoForm == null)
			model.addAttribute("updateUserInfoForm", createUserInfoForm(user));

		if (changePasswordForm == null)
			model.addAttribute("changePasswordForm", new ChangePasswordForm());

		if (sellerRequestForm == null)
			model.addAttribute("sellerRequestForm", new SellerRequestForm());

		model.addAttribute("sellerRequestFormAction", "new_seller");
		model.addAttribute("updateUserInfoFormAction", "change_info");
		model.addAttribute("changePasswordFormAction", "change_password");

		return getTemplatePath("user_account");
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/me", method = RequestMethod.POST, params = "action=new_seller")
	public String becomeSeler(Model model,
			@ModelAttribute @Valid SellerRequestForm sellerRequestForm,
			BindingResult bindingResult,
			@AuthenticationPrincipal LMFUserDetails user) {

		// Admins can't be sellers !
		if(user.getInternalUser().isAdmin())
			return "redirect:/accounts/me";
		
		// Already a seller? Redirect to his home
		if (user.getInternalUser().isSeller())
			return "redirect:/accounts/me/seller";

		if (bindingResult.hasErrors()) {
			return onError(model, user.getInternalUser(), null, null,
					sellerRequestForm);
		} else { // seller request

			try {
				Seller seller = new Seller();
				seller.setCodiceFiscale(sellerRequestForm.getCodiceFiscale());
				seller.setPhoneNumber(sellerRequestForm.getPhoneNumber());
				seller.setRagioneSociale(sellerRequestForm.getRagioneSociale());

				userBean.requestSellerAuthorization(seller, user.getId());

				// Update session's user details
				user.setInternalUser(userBean.getUserById(user.getId()));
			} catch (Exception e) {
				return "redirect:/accounts/me?action=new_seller&msg=error";
			}
		}

		return "redirect:/accounts/me?action=new_seller";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/me", method = RequestMethod.POST, params = "action=change_info")
	public String updateUserProfile(Model model,
			@ModelAttribute @Valid UpdateUserInfoForm updateUserInfoForm,
			BindingResult bindingResult,
			@AuthenticationPrincipal LMFUserDetails user) {

		User internalUser = user.getInternalUser();
		if (bindingResult.hasErrors()) {
			return onError(model, internalUser, updateUserInfoForm, null, null);
		} else { // update user info
			try {

				User updatedUser = (User) internalUser.clone();

				if (internalUser.getSignInProvider() != null
						&& internalUser.getUsername().equals(
								internalUser.getEmail())
						&& updateUserInfoForm.getUsername() != null
						&& !updateUserInfoForm.getUsername().trim().equals(""))
					updatedUser.setUsername(updateUserInfoForm.getUsername());

				updatedUser.setFirstName(updateUserInfoForm.getFirstName());
				updatedUser.setLastName(updateUserInfoForm.getLastName());
				updatedUser.setAddress(updateUserInfoForm.getAddress());
				updatedUser.setFirstName(updateUserInfoForm.getFirstName());

				user.setInternalUser(userBean.updateUser(updatedUser));

			} catch (EJBException ejbE) {
				// Catch the expected EJBException
				Throwable t = extractInnerEJBException(ejbE);

				if (t instanceof UsernameAlreadyInUseException) {
					// Username already used
					addFieldError(UpdateUserInfoForm.class.toString(),
							UpdateUserInfoForm.FIELD_NAME_USERNAME,
							((UsernameAlreadyInUseException) t).getUsername(),
							"The username has already been used.",
							bindingResult);
					return onError(model, internalUser, updateUserInfoForm,
							null, null);
				}

				if (t instanceof InvalidAddressException) {
					addFieldError(UpdateUserInfoForm.class.toString(),
							UpdateUserInfoForm.FIELD_NAME_ADDRESS,
							((InvalidAddressException) t).getAddress(),
							"Invalid Address", bindingResult);
					return onError(model, internalUser, updateUserInfoForm,
							null, null);
				}

				throw ejbE;
			} catch (Exception e) {
				return "redirect:/accounts/me?action=change_info&msg=error";
			}

			try {
				MultipartFile picture = updateUserInfoForm.getPicture();
				if (picture != null && picture.getSize() > 0)
					user.setInternalUser(userBean.updateProfileImg(
							internalUser, picture.getInputStream(),
							picture.getContentType()));
			} catch (Exception e) {
				return "redirect:/accounts/me?action=change_info&msg=img_error";
			}
		}

		return "redirect:/accounts/me";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/me", method = RequestMethod.POST, params = "action=change_password")
	public String changePassword(Model model,
			@ModelAttribute @Valid ChangePasswordForm changePasswordForm,
			BindingResult bindingResult,
			@AuthenticationPrincipal LMFUserDetails user) {

		User internalUser = user.getInternalUser();

		if (bindingResult.hasErrors()) {
			return onError(model, internalUser, null, changePasswordForm, null);
		} else { // update user password
			try {
				internalUser
						.setPassword(changePasswordForm.getPassword() == null ? null
								: passwordEncoder.encode(changePasswordForm
										.getPassword()));

				user.setInternalUser(userBean.updateUser(internalUser));
			} catch (Exception e) {
				return "redirect:/accounts/me?action=change_password&msg=error";
			}
		}

		return "redirect:/accounts/me?action=change_password&msg=ok";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/me/seller", method = RequestMethod.GET)
	public String showSellerAccountPage(Model model,
			@AuthenticationPrincipal LMFUserDetails user) {
		// TODO: E' utile mettere un'altra pagina ?
		return "redirect:/shops/my";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/{user-id}", method = RequestMethod.GET)
	public String showUserAccountPage(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@PathVariable("user-id") Long userId) {

		if (user.getInternalUser().getId() == userId)
			return "redirect:/accounts/me";

		User u = userBean.getUserById(userId);

		if (u == null)
			return PAGES_PATH + "error/access_denied";

		model.addAttribute("user", u);
		return getTemplatePath("user_public_account");
	}

	// @Secured("ROLE_USER")
	// @RequestMapping(value = "/seller/new", method = RequestMethod.GET)
	// public String showNewSellerPage(Model model,
	// @AuthenticationPrincipal LMFUserDetails user) {
	// model.addAttribute("user", user.getInternalUser());
	//
	// return getTemplatePath("seller_authorization_request");
	// }

	/*
	 * 
	 * MOVED TO /me?action=new_seller
	 * 
	 * 
	 * @Secured("ROLE_USER")
	 * 
	 * @RequestMapping(value = "/me/seller/new", method = RequestMethod.GET)
	 * public String showSellerRequestPage(Model model, SellerRequestForm
	 * sellerRequestForm,
	 * 
	 * @AuthenticationPrincipal LMFUserDetails user) { // Already a seller ?
	 * Redirect to his home if (user.getInternalUser().isSeller()) return
	 * "redirect:/accounts/me/seller";
	 * 
	 * if (sellerRequestForm == null) sellerRequestForm = new
	 * SellerRequestForm();
	 * 
	 * model.addAttribute("sellerRequestForm", sellerRequestForm);
	 * model.addAttribute("formAction", "/accounts/me/seller/new");
	 * 
	 * return getTemplatePath("seller_authorization_request"); }
	 */

	/*
	 * 
	 * MOVED TO /me?action=new_seller
	 * 
	 * 
	 * @Secured("ROLE_USER")
	 * 
	 * @RequestMapping(value = "/me/seller/new", method = RequestMethod.POST)
	 * public String requestSellerAuthorization(Model model,
	 * 
	 * @AuthenticationPrincipal LMFUserDetails user,
	 * 
	 * @ModelAttribute @Valid SellerRequestForm sellerRequestForm, BindingResult
	 * bindingResult) {
	 * 
	 * // Already a seller ? Redirect to his home if
	 * (user.getInternalUser().isSeller()) return
	 * "redirect:/accounts/me/seller";
	 * 
	 * if (bindingResult.hasErrors()) { //return showSellerRequestPage(model,
	 * sellerRequestForm, user); }
	 * 
	 * try { Seller seller = new Seller();
	 * seller.setCodiceFiscale(sellerRequestForm.getCodiceFiscale());
	 * seller.setPhoneNumber(sellerRequestForm.getPhoneNumber());
	 * seller.setRagioneSociale(sellerRequestForm.getRagioneSociale());
	 * 
	 * userBean.requestSellerAuthorization(seller, user.getId());
	 * 
	 * // Update session's user details
	 * user.setInternalUser(userBean.getUserById(user.getId())); } catch
	 * (Exception e) { model.addAttribute("errorMsg",
	 * "Unable to crate the seller request..."); //return
	 * showSellerRequestPage(model, sellerRequestForm, user); }
	 * 
	 * return getTemplatePath("seller_authorization_request_result"); }
	 */

	private void addFieldError(String objectName, String fieldName,
			String fieldValue, String errorCode, BindingResult result) {
		FieldError error = new FieldError(objectName, fieldName, fieldValue,
				false, new String[] { errorCode }, new Object[] {}, errorCode);

		result.addError(error);
	}

	protected RuntimeException extractInnerEJBException(EJBException ejbEx) {
		Throwable ex = ejbEx.getCause();
		while (ex != null) {
			if (ex instanceof SQLException) {
				try {
					ExceptionManagementHelper
							.handleSQLConstraintViolationException((SQLException) ex);
				} catch (UsernameAlreadyInUseException ue) {
					return ue;
				} catch (Exception e) {
					return null;
				}

			} else if (ex instanceof InvalidAddressException) {
				return (RuntimeException) ex;
			}

			ex = ex.getCause();
		}
		return null;
	}
}