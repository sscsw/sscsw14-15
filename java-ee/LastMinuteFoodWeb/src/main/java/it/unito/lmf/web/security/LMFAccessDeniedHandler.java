package it.unito.lmf.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

public class LMFAccessDeniedHandler extends AccessDeniedHandlerImpl implements AccessDeniedHandler {

	public static final String ACCESS_DENIED_HANDLER_ORIGINAL_REQUEST = "ACCESS_DENIED_HANDLER_ORIGINAL_REQUEST";

	@Override
	public void handle(HttpServletRequest request,
			HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException,
			ServletException {

		request.setAttribute(ACCESS_DENIED_HANDLER_ORIGINAL_REQUEST, request);

		super.handle(request, response, accessDeniedException);
	}
}
