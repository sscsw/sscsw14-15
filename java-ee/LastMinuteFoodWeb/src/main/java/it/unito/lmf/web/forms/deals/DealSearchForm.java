package it.unito.lmf.web.forms.deals;

import it.unito.lmf.domain.dsl.deal.DealSearchData;

public class DealSearchForm extends DealSearchData {

	private static final long serialVersionUID = 1598180038398694456L;

	private String distanceCAP;

	private Integer distanceStep;
	
	public String getDistanceCAP() {
		return distanceCAP;
	}

	public void setDistanceCAP(String distanceCAP) {
		this.distanceCAP = distanceCAP;
	}
	
	public Integer getDistance() {
		return super.getDistance();
	}
	
	public Integer getDistanceStep() {
		return distanceStep;
	}

	public void setDistanceStep(Integer distance) {
		Integer ranges[] = { 1, 2, 5, 10, 20, 50, 100, 200, 1000 };
		if(distance>ranges.length)
			distance=ranges.length-1;
		
		distanceStep=distance;
		
		super.setDistance(ranges[distance]);
	}
}