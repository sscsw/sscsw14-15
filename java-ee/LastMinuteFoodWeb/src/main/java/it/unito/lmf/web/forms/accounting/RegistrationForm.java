package it.unito.lmf.web.forms.accounting;

import java.io.Serializable;

import it.unito.lmf.web.forms.BaseForm;
import it.unito.lmf.web.security.SocialMediaService;
import it.unito.lmf.web.validation.password.PasswordsNotEmpty;
import it.unito.lmf.web.validation.password.PasswordsNotEqual;

import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


@PasswordsNotEmpty(
        triggerFieldName = "signInProvider",
        passwordFieldName = "password",
        passwordVerificationFieldName = "passwordVerification",
        message = "Password can't be empty"
)
@PasswordsNotEqual(
        passwordFieldName = "password",
        passwordVerificationFieldName = "passwordVerification",
        message = "Passwords must be equals"
)
public class RegistrationForm implements Serializable {

	private static final long serialVersionUID = 7363075194773270332L;

	public static final String FIELD_NAME_EMAIL = "email";
	public static final String FIELD_NAME_USERNAME = "username";
	
	@Size(max=255)
	private String username;

	@Email
    @NotEmpty
    @Size(max = 255)
    private String email;

    @NotEmpty
    @Size(max = 100)
    private String firstName;

    @NotEmpty
    @Size(max = 100)
    private String lastName;

    private String password;

    private String passwordVerification;

    private SocialMediaService signInProvider;

    public RegistrationForm() {

    }

    public boolean isNormalRegistration() {
        return signInProvider == null;
    }

    public boolean isSocialSignIn() {
        return signInProvider != null;
    }
    
    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordVerification() {
        return passwordVerification;
    }

    public void setPasswordVerification(String passwordVerification) {
        this.passwordVerification = passwordVerification;
    }

    public SocialMediaService getSignInProvider() {
        return signInProvider;
    }

    public void setSignInProvider(SocialMediaService signInProvider) {
        this.signInProvider = signInProvider;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
        		.append("username", username)
        		.append("firstName", firstName)
                .append("lastName", lastName)
                .append("email", email)
                .append("signInProvider", signInProvider)
                .toString();
    }
}
