package it.unito.lmf.web.controllers.deal;

import it.unito.lmf.biz.deal.DealMgmtBean;
import it.unito.lmf.biz.shop.ShopMgmtBean;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.deal.DealCategory;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.domain.dsl.external.google.geocoding.Location;
import it.unito.lmf.domain.dsl.values.Currency;
import it.unito.lmf.domain.dsl.values.PriceUnit;
import it.unito.lmf.domain.dsl.values.QuantityUnit;
import it.unito.lmf.util.geocoding.GeocodingHelper;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.controllers.exceptions.RedirectException;
import it.unito.lmf.web.forms.deals.DealSearchForm;
import it.unito.lmf.web.forms.deals.NewDealForm;
import it.unito.lmf.web.misc.PopupMessage.MessageType;
import it.unito.lmf.web.misc.pagination.SortableField;
import it.unito.lmf.web.security.LMFUserDetails;
import it.unito.lmf.web.utils.mapping.DealMappingHelper;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("")
public class DealController extends BaseController {

	@EJB(mappedName = DealMgmtBean.JNDIName)
	DealMgmtBean dealBean;

	@EJB(mappedName = ShopMgmtBean.JNDIName)
	ShopMgmtBean shopBean;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"), true, 19));
	}

	// @Secured("ROLE_USER")
	// @RequestMapping(value = "/shops/{shop-id}/deals", method =
	// RequestMethod.GET)
	// public String listDealsPerShop(Model model, @PathVariable("shop-id") Long
	// shopId) {
	// model.addAttribute("shop", shopBean.getShopById(shopId));
	// model.addAttribute("deals", dealBean.getDealsByShop(shopId));
	// return getTemplatePath("deals_per_shop");
	// }

	@Autowired
	DealMappingHelper dealMH;

	// @Secured("ROLE_USER")
	@RequestMapping(value = "/deals", method = RequestMethod.GET)
	public String listDeals(Model model, Pageable pageable,
			@ModelAttribute DealSearchForm searchForm) throws RedirectException {
		List<SortableField> sortableFields = new ArrayList<SortableField>();
		sortableFields.add(new SortableField("createdAt", "Creation Date"));
		sortableFields.add(new SortableField("price", "Price"));
		sortableFields.add(new SortableField("discountPercentage",
				"Discount Percentage"));
		sortableFields.add(new SortableField("expireDate", "Expiration Date"));
		sortableFields.add(new SortableField("name", "Name"));

		if (pageable.getSort() == null)
			pageable = new PageRequest(pageable.getPageNumber(),
					pageable.getPageSize(), new Sort(new Order(Direction.DESC,
							"createdAt")));

		checkSortableFields(pageable, sortableFields);

		if (searchForm.getDistanceCAP() != null && !searchForm.getDistanceCAP().equals("")) {
			try {
				Location loc = GeocodingHelper.getLocation(searchForm
						.getDistanceCAP()+",Italy");
				if (loc != null)
					searchForm.setDistanceFromLatLng(loc.getLat() + ","
							+ loc.getLng());
			} catch (Exception e) {
			}
		}

		Page<Deal> page = dealBean.getAllDealsWithSearch(pageable, searchForm);

		model.addAttribute("dealsJSON", dealMH.getDSL(page.getContent()));
		model.addAttribute("deals", page.getContent());
		model.addAttribute("page",
				getPageWrapper(page, "/deals", sortableFields));

		return getTemplatePath("deals_list");
	}

	// @Secured("ROLE_USER")
	// @RequestMapping(value = "/deals", method = RequestMethod.GET)
	// public String listDeals(Model model, Pageable pageable) {
	// model.addAttribute("deals", dealBean.getAllDeals());
	// return getTemplatePath("deals_list");
	// }

	@Secured("ROLE_USER")
	@RequestMapping(value = "/deals/{deal-id}", method = RequestMethod.GET)
	public String dealDetails(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@PathVariable("deal-id") Long dealId) {

		Deal deal = dealBean.getDealById(dealId);
		checkResourceExists(deal);

		model.addAttribute("deal", deal);

		if (deal.getShop().getOwner().getId() == user.getInternalUser().getId())
			return getTemplatePath("deal_details_owner");
		else
			return getTemplatePath("deal_details");
	}

	@Secured("ROLE_SELLER")
	@RequestMapping(value = "/shops/{shop-id}/deals/new", method = RequestMethod.GET)
	public String showNewDealPage(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			NewDealForm newDealForm, @PathVariable("shop-id") Long shopId) {

		Shop shop = shopBean.getShopById(shopId);
		checkNewDealPreconditions(shop, user, "/shops/" + shopId);

		if (newDealForm == null)
			newDealForm = new NewDealForm();

		model.addAttribute("newDealForm", newDealForm);
		model.addAttribute("formAction", "/shops/" + shopId + "/deals/new");

		Collection<DealCategory> dealCategories = dealBean
				.getAllDealCategories();
		model.addAttribute("dealCategories", dealCategories);
		model.addAttribute("currencies", Arrays.asList(Currency.values()));
		model.addAttribute("quantityUnits",
				Arrays.asList(QuantityUnit.values()));
		model.addAttribute("priceUnits", Arrays.asList(PriceUnit.values()));

		model.addAttribute("shopId", shopId);

		return getTemplatePath("new_deal");
	}

	@Secured("ROLE_SELLER")
	@RequestMapping(value = "/shops/{shop-id}/deals/new", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String createNewDeal(Model model,
			@AuthenticationPrincipal LMFUserDetails user,
			@ModelAttribute @Valid NewDealForm newDealForm,
			BindingResult bindingResult, @PathVariable("shop-id") Long shopId) {

		Shop shop = shopBean.getShopById(shopId);
		checkNewDealPreconditions(shop, user, "/shops/" + shopId);

		if (bindingResult.hasErrors()) {
			return showNewDealPage(model, user, newDealForm, shopId);
		}

		try {

			Deal deal = new Deal();
			deal.setDescription(newDealForm.getDescription());
			deal.setDiscountReason(newDealForm.getDiscountReason());
			deal.setExpireDate(newDealForm.getExpireDate());
			deal.setName(newDealForm.getName());
			deal.setPrice(newDealForm.getPrice());
			deal.setPriceUnit(newDealForm.getPriceUnit());
			deal.setQuantity(newDealForm.getQuantity());
			deal.setCurrency(newDealForm.getCurrency());
			deal.setQuantityUnit(newDealForm.getQuantityUnit());
			deal.setStartingPrice(newDealForm.getStartingPrice());
			deal.setImage(newDealForm.getPicture().getOriginalFilename());

			dealBean.createNewDeal(deal, user.getId(), newDealForm
					.getCategories(), shop, new ByteArrayInputStream(
					newDealForm.getPicture().getBytes()), newDealForm
					.getPicture().getContentType());

			return "redirect:/deals/" + deal.getId();
		} catch (Exception e) {
			model.addAttribute("errorMsg", "Unable to create the deal...");
			return showNewDealPage(model, user, newDealForm, shopId);
		}
	}

	/**
	 * Checks whether the deal creation request preconditions has been
	 * satisfied.
	 * 
	 * @param shop
	 * @param user
	 */
	private void checkNewDealPreconditions(Shop shop, LMFUserDetails user,
			String url) throws RedirectException {
		checkResourceExists(shop);

		if (shop.getOwner().getId() != user.getId())
			throw new RedirectException(url,
					"You're not the owner of this shop.", MessageType.ERROR);

		if (!shop.isAuthorized())
			throw new RedirectException(url,
					"This shop has not been authorized yet.", MessageType.ERROR);
	}
}
