package it.unito.lmf.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Configuration class for main application properties.
 * @author Biava Lorenzo
 *
 */
@Configuration
@EnableWebMvc
@EnableWebMvcSecurity //To add AuthPrincipalResolver & other Spring SecurityWebMVCConfigs.
@ComponentScan({ "it.unito.lmf.web.*" }) //Scan this tree for Spring website related classes.
@Import({ SecurityConfiguration.class }) //Include Spring Security Configs
public class AppConfiguration {

	public static final String WEB_APP_URL_PREFIX="";//"/web";
	
	// Working for bean creation, but it is a Spring Bean, causing error to nested @EJB injection.
	// Could fix trying to create an EJB or a Spring Bean referencing that EJB.
//	public @Bean(name="UserMgmtBean") UserMgmtBean getUserMgmtBean(Object o) throws NamingException{
//	        
//	      //Get the Initial Context for the JNDI lookup for a local EJB
//	        InitialContext ic = new InitialContext();
//	        //Retrieve the Home interface using JNDI lookup
//	        return (UserMgmtBean) ic.lookup(UserMgmtBean.JNDIName);
//	        
//	        //return null;
//	    }
	
}