package it.unito.lmf.web.forms.deals;

import it.unito.lmf.web.validation.file.PictureFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

public class NewDealForm implements Serializable {

	private static final long serialVersionUID = 263380637726626751L;

	@NotNull
	@Size(min = 3, max = 256)
	private String name;

	@NotNull
	@Size(min = 20, max = 5000)
	private String description;

	@NotNull
	@Min(value = 0, message = "Price cannot be negative.")
	private Double price;

	@NotNull
	@Size(min = 0, max = 10)
	private String priceUnit;
	
	@NotNull
	@Size(min = 1, max = 10)
	private String currency;

	@NotNull
	@Min(value = 0)
	private Double quantity;
	
	@NotNull
	@Size(min = 1, max = 10)
	private String quantityUnit;

	@NotNull
	@NotEmpty
	private String discountReason;

	@NotNull
	@Min(value = 0, message = "Price cannot be negative.")
	private Double startingPrice;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Future(message = "Expire date must be in the future.")
	private Date expireDate;

	@NotNull
	@PictureFile(maxSize=1024000, message="May not be empty and max size is 1 MB")
	private MultipartFile picture; 
	
	@NotNull
	@Size(min = 1, max = 3)
	private List<Long> categories = new ArrayList<Long>();
	
	public MultipartFile getPicture() {
		return picture;
	}

	public void setPicture(MultipartFile picture) {
		this.picture = picture;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getDiscountReason() {
		return discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public Double getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(Double startingPrice) {
		this.startingPrice = startingPrice;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public List<Long> getCategories() {
		return categories;
	}

	public void setCategories(List<Long> categories) {
		this.categories = categories;
	}

}
