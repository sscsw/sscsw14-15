package it.unito.lmf.web.utils.mapping;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.domain.dsl.deal.SellerRepresentation;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;
import it.unito.lmf.domain.dsl.notification.AdministratorNotificationRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdministratorNotificationMappingHelper
		extends
		MappingHelperBase<AdministratorNotification, AdministratorNotificationRepresentation> {

	@Autowired
	MappingHelperBase<Seller, SellerRepresentation> sellerMH;

	@Autowired
	MappingHelperBase<Shop, ShopRepresentation> shopMH;

	@Override
	public AdministratorNotification getEntity(
			AdministratorNotificationRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public AdministratorNotificationRepresentation getDSL(
			AdministratorNotification o) {
		if(o==null) return null;
		AdministratorNotificationRepresentation dsl = new AdministratorNotificationRepresentation();

		dsl.setCreatedAt(o.getCreatedAt().getTime());
		dsl.setDone(o.isDone());
		dsl.setId(o.getId());
		dsl.setMessage(o.getMessage());		
		dsl.setSeller(sellerMH.getDSL(o.getSeller()));
		dsl.setShop(shopMH.getDSL(o.getShop()));
		dsl.setType(o.getType());

		return dsl;
	}
}
