package it.unito.lmf.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Configuration class to start-up the Spring Security Web integration.
 * <br/>Spring Security configuration in placed in {@link SecurityConfiguration}.
 * 
 * @author Biava Lorenzo
 *
 */
public class SecurityWebApplicationInitializer extends
		AbstractSecurityWebApplicationInitializer {

}