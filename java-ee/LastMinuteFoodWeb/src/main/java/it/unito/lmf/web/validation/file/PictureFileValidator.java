package it.unito.lmf.web.validation.file;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.web.multipart.MultipartFile;

public class PictureFileValidator implements
		ConstraintValidator<PictureFile, MultipartFile> {
	private PictureFile annotation;

	@Override
	public void initialize(final PictureFile constraintAnnotation) {
		this.annotation = constraintAnnotation;
	}

	@Override
	public boolean isValid(final MultipartFile value,
			final ConstraintValidatorContext context) {

		// Null file
		if (value == null)
			return annotation.canBeEmpty();
		
		// Empty file
		if (value.getSize() < 1)
			return annotation.canBeEmpty();

		// File size
		if (value.getSize() > annotation.maxSize())
			return false;

		// Picture format
		for (String format : annotation.formats())
			if (value.getContentType().contains(format))
				return true;

		return false;
	}
}