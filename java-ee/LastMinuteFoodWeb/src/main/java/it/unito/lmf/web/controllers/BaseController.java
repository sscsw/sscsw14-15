package it.unito.lmf.web.controllers;

import it.unito.lmf.web.controllers.exceptions.RedirectException;
import it.unito.lmf.web.controllers.exceptions.ResourceNotFoundException;
import it.unito.lmf.web.misc.pagination.PageWrapper;
import it.unito.lmf.web.misc.pagination.SortableField;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.ui.Model;

public abstract class BaseController {
	protected final static String PAGES_PATH = "pages/";

	/**
	 * 
	 * @return the path to the base view folder (/pages).
	 */
	protected static String getTemplatesBasePath() {
		return PAGES_PATH;
	}

	/**
	 * 
	 * @param page
	 * @return the full path of the given <i>page</i> path, inside the base view
	 *         folder (/pages/<i>page</i>)
	 */
	protected static String getBasePath(String page) {
		return getTemplatesBasePath() + page;
	}

	/**
	 * 
	 * @return the path to the controller's view folder
	 *         (/pages/&lt;<i>controllerClassName</i>&gt;/).
	 */
	protected String getControllerTemplatesBasePath() {
		return PAGES_PATH
				+ this.getClass().getSimpleName().replace("Controller", "")
						.toLowerCase() + "/";
	}

	/**
	 * 
	 * @param page
	 * @return the full path of the given <i>page</i> path, inside the
	 *         controller's own folder
	 *         (/pages/&lt;<i>controllerClassName</i>&gt;/<i>page</i>) .
	 */
	protected String getTemplatePath(String page) {
		return getControllerTemplatesBasePath() + page;
	}

	/**
	 * Sets the error field in the <i>model</i>, using the given <i>error</i>
	 * message.
	 * 
	 * @param model
	 * @param error
	 */
	protected void setPageError(Model model, String error) {
		model.addAttribute("errorMsg", error);
	}

	/**
	 * @param model
	 * @return return the error message if present in the error field in the
	 *         <i>model</i>, <tt>null</tt> otherwise.
	 */
	protected String getPageError(Model model) {
		return (String) model.asMap().get("errorMsg");
	}

	/**
	 * Verifies whether the given resource exists (it's not <tt>null</tt>) and
	 * throws the related {@link ResourceNotFoundException} in case it does not.
	 * 
	 * @param resource
	 *            the resource to be checked.
	 * @throws ResourceNotFoundException
	 *             if the resource does not exist (it's <tt>null</tt>).
	 */
	protected <T> T checkResourceExists(T resource)
			throws ResourceNotFoundException {
		if (resource == null)
			throw new ResourceNotFoundException();

		return resource;
	}

	/**
	 * Returns the {@link PageWrapper} for the given {@link Page}, also checking
	 * if the requested page has exceeded page limit, redirecting in the case.
	 * 
	 * @param page
	 *            the page to put inside the PageWrapper.
	 * @param url
	 *            the URL of the current end point.
	 * @param sortableFields
	 *            a list of {@link SortableField} to display in the view.
	 * @return the newly created {@link PageWrapper}.
	 * @throws RedirectException
	 *             if the requested page has exceeded page limit.
	 */
	protected <T> PageWrapper<T> getPageWrapper(Page<T> page, String url,
			List<SortableField> sortableFields) throws RedirectException {
		PageWrapper<T> pageWrapper = new PageWrapper<T>(page, url,
				sortableFields);
		if (pageWrapper.getTotalPages() > 0
				&& pageWrapper.getNumber() >= pageWrapper.getTotalPages())
			throw new RedirectException(pageWrapper.lastPageUrl());

		return pageWrapper;
	}

	/**
	 * See {@link BaseController#getPageWrapper(Page, String, List)}, with no
	 * {@code sortableFields}.
	 * 
	 * @see BaseController#getPageWrapper(Page, String, List)
	 * @param page
	 * @param url
	 * @return
	 */
	protected <T> PageWrapper<T> getPageWrapper(Page<T> page, String url) {
		return getPageWrapper(page, url, null);
	}

	/**
	 * Check whether the {@link Pageable} sort params are legal, given a list of
	 * permitted {@link SortableField}.
	 * 
	 * @param pageable
	 *            the pagination settings.
	 * @param sortableFields
	 *            the list of permitted {@link SortableField}.
	 */
	protected void checkSortableFields(Pageable pageable,
			List<SortableField> sortableFields) {
		if (pageable.getSort() != null)
			for (Order order : pageable.getSort())
				if (!SortableField.containsSortableField(sortableFields,
						order.getProperty()))
					throw new IllegalArgumentException(
							"Bad sort field provided: " + order.getProperty()
									+ ".");
	}

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected HttpServletResponse response;

	protected HttpServletRequest getCurrentRequest() {
		return this.request;
	}

	protected HttpServletResponse getCurrentResponse() {
		return this.response;
	}
}
