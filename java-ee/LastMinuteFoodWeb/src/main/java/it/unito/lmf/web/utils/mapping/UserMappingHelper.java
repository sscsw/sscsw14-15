package it.unito.lmf.web.utils.mapping;

import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.domain.dsl.accounting.UserRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import org.springframework.stereotype.Component;

@Component
public class UserMappingHelper extends
		MappingHelperBase<User, UserRepresentation> {

	@Override
	public User getEntity(UserRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public UserRepresentation getDSL(User o) {
		if(o==null) return null;
		UserRepresentation dsl = new UserRepresentation();

		dsl.setId(o.getId());
		dsl.setUsername(o.getUsername());
		
		return dsl;
	}
}
