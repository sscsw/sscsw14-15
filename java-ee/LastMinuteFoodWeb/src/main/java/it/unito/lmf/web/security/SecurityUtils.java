package it.unito.lmf.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
	private static final Logger LOG = LoggerFactory
			.getLogger(SecurityUtils.class);

	public static void logInUser(LMFUserDetails userDetails) {
		// LOG.info("Logging in user: {}", user);

		// List<UserRole> roles = new ArrayList<UserRole>();
		// roles.add(UserRole.ROLE_USER);
		// if (user.isAdmin())
		// roles.add(UserRole.ROLE_ADMIN);
		// if (user.isSeller()&&user.getSeller().isAuthorized())
		// roles.add(UserRole.ROLE_SELLER);
		//
		// LMFUserDetails userDetails = LMFUserDetails.getBuilder()
		// .firstName(user.getFirstName())
		// .id(user.getId())
		// .lastName(user.getLastName())
		// .password(user.getPassword())
		// .roles(roles)
		// .socialSignInProvider(
		// (user.getSignInProvider() == null ? null
		// : SocialMediaService.valueOf(user.getSignInProvider())))
		// .username(user.getEmail())
		// .build();
		LOG.debug("Logging in principal: {}", userDetails);

		Authentication authentication = new UsernamePasswordAuthenticationToken(
				userDetails, null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(authentication);

		LOG.info("User: {} has been logged in.", userDetails);
	}
}
