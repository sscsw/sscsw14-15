package it.unito.lmf.web.controllers.admin;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.biz.notification.NotificationMgmtBean;
import it.unito.lmf.biz.shop.ShopMgmtBean;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.notification.AdministratorNotification.NotificationType;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.web.controllers.BaseController;

import javax.ejb.EJB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
@Secured("ROLE_ADMIN")
public class AdminController extends BaseController {

	private static final Logger LOG = LoggerFactory
			.getLogger(AdminController.class);

	@EJB(mappedName = NotificationMgmtBean.JNDIName)
	NotificationMgmtBean notificationBean;

	@EJB(mappedName = ShopMgmtBean.JNDIName)
	ShopMgmtBean shopBean;

	@EJB(mappedName = UserMgmtBean.JNDIName)
	UserMgmtBean userBean;
	
	@RequestMapping("")
	public String administrationPage(Model model) {
		model.addAttribute("notifications",
				notificationBean.getAllPendingAdminNotifications());
		
		model.addAttribute("shops",
				shopBean.getAllPendingShops());
		model.addAttribute("sellers",
				userBean.getAllPendingSellers());
		
		return getTemplatePath("administration");
	}
/*
	@RequestMapping("/notifications")
	public String showAdminNotificationsPage(Model model) {

		model.addAttribute("notifications",
				notificationBean.getAllPendingAdminNotifications());
		return getTemplatePath("notifications_list");
	}
*/
	@RequestMapping("/notifications/{notification-id}")
	public String showNotificationDetailsPage(Model model,
			@PathVariable("notification-id") Long notificationId) {
		AdministratorNotification notification = notificationBean
				.getAdministratorNotificationById(notificationId);
		checkResourceExists(notification); 
	
		model.addAttribute("notification", notification);
		return getTemplatePath("notification_details");
	}

	@RequestMapping(value = "/notifications/{notification-id}/accept", method = RequestMethod.GET)
	public String adminNotificationResultAccepted(Model model,
			@PathVariable("notification-id") Long notificationId) {
		return adminNotificationResult(model, notificationId, true);
	}

	@RequestMapping(value = "/notifications/{notification-id}/reject", method = RequestMethod.GET)
	public String adminNotificationResultRejected(Model model,
			@PathVariable("notification-id") Long notificationId) {
		return adminNotificationResult(model, notificationId, false);
	}

	private String adminNotificationResult(Model model, Long notificationId,
			boolean result) {
		try {
			AdministratorNotification notification = notificationBean
					.getAdministratorNotificationById(notificationId);
			checkResourceExists(notification); 
			
			// TODO: set done instead of delete -- Problem if request is reject and shop (or seller) was deleted
			// notificationBean.setNotificationDone(notificationId);
			notificationBean.deleteNotification(notification);
			
			if (notification
					.getType()
					.equals(AdministratorNotification.NotificationType.NEW_SHOP_REQUEST.toString())) {
				shopBean.shopAuthorizationRequestResult(notification.getShop()
						.getId(), result);
			} else {
				userBean.sellerAuthorizationRequestResult(notification
						.getSeller().getId(), result);
			}

			//TODO: Delete or disable the notification to avoid future reuse !
			//TODO: Display a success message

			return "redirect:/admin/notifications";
		} catch (Exception e) {
			// throw new RuntimeException("NOT FOUND",e);
			setPageError(model, "Error: " + e.getMessage());
			return showNotificationDetailsPage(model, notificationId);
		}
	}
	
	@RequestMapping(value = "/requests/sellers/{seller-id}/reject", method = RequestMethod.GET)
	public String sellerRequestRejected(Model model,
			@PathVariable("seller-id") Long sellerId) {
		
		disableNotificationByTypeAndId(NotificationType.SELLER_AUTHORIZATION_REQUEST, sellerId);
		userBean.sellerAuthorizationRequestResult(sellerId, false);
		
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/requests/sellers/{seller-id}/accept", method = RequestMethod.GET)
	public String sellerRequestAccepted(Model model,
			@PathVariable("seller-id") Long sellerId) {
		
		disableNotificationByTypeAndId(NotificationType.SELLER_AUTHORIZATION_REQUEST, sellerId);
		userBean.sellerAuthorizationRequestResult(sellerId, true);

		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/requests/shops/{shop-id}/reject", method = RequestMethod.GET)
	public String shopRequestRejected(Model model,
			@PathVariable("shop-id") Long shopId) {
		
		disableNotificationByTypeAndId(NotificationType.NEW_SHOP_REQUEST, shopId);
		shopBean.shopAuthorizationRequestResult(shopId, false);

		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/requests/shops/{shop-id}/accept", method = RequestMethod.GET)
	public String shopRequestAccepted(Model model,
			@PathVariable("shop-id") Long shopId) {

		disableNotificationByTypeAndId(NotificationType.NEW_SHOP_REQUEST, shopId);
		shopBean.shopAuthorizationRequestResult(shopId, true);

		return "redirect:/admin";
	}
	
	private void disableNotificationByTypeAndId(NotificationType type, Long id) {
		AdministratorNotification notification = notificationBean.getNotificationByTypeAndObjectId(type, id);
		checkResourceExists(notification); 
		
		// TODO: set done instead of delete -- Problem if request is reject and shop (or seller) was deleted
		// notificationBean.setNotificationDone(notification);
		notificationBean.deleteNotification(notification);
	}
	
	@RequestMapping(value = "/shops/{shop-id}", method = RequestMethod.GET)
	public String showShopRequestsPage(Model model,
			@PathVariable("shop-id") Long shopId) {
		Shop shop = null;
		try {
			shop = shopBean.getShopById(shopId);
		} catch (Exception e) {
			shop = null;
		}
		
		if (shop != null) {
			model.addAttribute("shop", shop);
			
			return getTemplatePath("admin_shop");
		}
		else {
			setPageError(model, "Unable to find shop with id " + shopId);
			return administrationPage(model);
		}
	}
	
	@RequestMapping(value = "/users/{user-id}", method = RequestMethod.GET)
	public String showUserPage(Model model,
			@PathVariable("user-id") Long userId) {
		
		User user = null;
		try {
			user = userBean.getUserById(userId);
		} catch (Exception e) {
			user = null;
		}
		
		if (user != null) {
			model.addAttribute("user", user);
			
			return getTemplatePath("admin_user");
		}
		else {
			setPageError(model, "Unable to find user with id " + userId);
			return administrationPage(model);
		}
	}

}
