package it.unito.lmf.web.controllers.notification;

import it.unito.lmf.biz.notification.NotificationMgmtBean;
import it.unito.lmf.dal.entities.notification.AdministratorNotification;
import it.unito.lmf.dal.entities.notification.DealNotification;
import it.unito.lmf.dal.entities.notification.DealNotification.VisibilityType;
import it.unito.lmf.domain.dsl.notification.AdministratorNotificationRepresentation;
import it.unito.lmf.domain.dsl.notification.DealNotificationRepresentation;
import it.unito.lmf.utils.mapping.MappingHelper;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.security.LMFUserDetails;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("notifications")
public class NotificationController extends BaseController {

	@EJB(mappedName = NotificationMgmtBean.JNDIName)
	NotificationMgmtBean notificationBean;

	@Autowired
	MappingHelper<AdministratorNotification, AdministratorNotificationRepresentation> adminNotificationMH;

	@Autowired
	MappingHelper<DealNotification, DealNotificationRepresentation> dealNotificationMH;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/my/admin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Collection<AdministratorNotificationRepresentation> getAdminNotificationList(
			Model model, @AuthenticationPrincipal LMFUserDetails user) {

		return adminNotificationMH.getDSL(notificationBean
				.getAllPendingAdminNotifications());
	}

	@Secured("ROLE_USER")
	@RequestMapping(value = "/my", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Collection<DealNotificationRepresentation> getUserNotificationList(
			@AuthenticationPrincipal LMFUserDetails user) {

		return dealNotificationMH.getDSL(notificationBean
				.getAllPendingUserNotifications(user.getId(),
						VisibilityType.WEB));
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/{notification-id}/done", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody ResponseEntity<String> setAdminNotificationDone(
			@PathVariable("notification-id") Long notificationId) {

		try {
			notificationBean.setNotificationDone(notificationId);
			return new ResponseEntity<String>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * Sets the deal notification to <code>read</code> status while redirecting
	 * to the related deal page for the user to view the notified deal.
	 * 
	 * @param notificationId
	 *            the Id of the deal notification
	 * @param user
	 *            the user making the request
	 * @return
	 */
	@Secured("ROLE_USER")
	@RequestMapping(value = "/{notification-id}/read", method = RequestMethod.GET)
	public String setDealNotificationRead(
			@PathVariable("notification-id") Long notificationId,
			@AuthenticationPrincipal LMFUserDetails user) {

		DealNotification notification = null;

		try {
			notification = notificationBean.findDealNotification(
					notificationId, user.getId());
			if (!notification.isRead())
				notificationBean
						.setNotificationRead(notification, user.getId());
		} catch (Exception e) {
			notification = null;
		}

		if (notification == null)
			return "redirect:/errors/404";

		return "redirect:/deals/" + notification.getDeal().getId();

	}

}