package it.unito.lmf.web.utils.mapping;

import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.deal.DealCategory;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DealMappingHelper extends
		MappingHelperBase<Deal, DealRepresentation> {

	@Autowired
	MappingHelperBase<Shop, ShopRepresentation> shopMH;

	@Override
	public Deal getEntity(DealRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public DealRepresentation getDSL(Deal o) {
		if(o==null) return null;
		DealRepresentation dsl = new DealRepresentation();

		//TODO: Restore dates once fixed Thymeleaf tranformation in Strings
		
		dsl.setId(o.getId());
		List<String> categories = new ArrayList<String>();
		for (DealCategory item : o.getCategories())
			categories.add(item.getName());
		dsl.setCategories(categories);
		//dsl.setCreatedAt(o.getCreatedAt());
		dsl.setDescription(o.getDescription());
		dsl.setDiscountPercentage(o.getDiscountPercentage());
		dsl.setDiscountReason(o.getDiscountReason());
		//dsl.setExpireDate(o.getExpireDate());
		dsl.setImage(o.getImage());
		//dsl.setModifiedAt(o.getModifiedAt());
		dsl.setName(o.getName());
		dsl.setPrice(o.getPrice());
		dsl.setPriceUnit(o.getPriceUnit());
		dsl.setQuantity(o.getQuantity());
		dsl.setQuantityUnit(o.getQuantityUnit());
		dsl.setShop(shopMH.getDSL(o.getShop()));
		dsl.setStartingPrice(o.getStartingPrice());

		return dsl;
	}
}
