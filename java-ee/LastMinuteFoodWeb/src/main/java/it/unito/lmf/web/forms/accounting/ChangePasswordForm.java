package it.unito.lmf.web.forms.accounting;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import it.unito.lmf.web.validation.password.PasswordsNotEmpty;
import it.unito.lmf.web.validation.password.PasswordsNotEqual;

@PasswordsNotEqual(
        passwordFieldName = "password",
        passwordVerificationFieldName = "passwordVerification",
        message = "Passwords must be equals"
)
public class ChangePasswordForm implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 5159890791653830276L;
	
	@NotEmpty
	private String password;
	
	@NotEmpty
	private String passwordVerification;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordVerification() {
		return passwordVerification;
	}
	public void setPasswordVerification(String passwordVerification) {
		this.passwordVerification = passwordVerification;
	}
}
