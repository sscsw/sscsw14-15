package it.unito.lmf.web.config;

import it.unito.lmf.biz.deal.DealMgmtBean;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.dal.entities.deal.DealCategory;
import it.unito.lmf.web.formatters.CalendarFormatter;
import it.unito.lmf.web.formatters.DateFormatter;
import it.unito.lmf.web.formatters.FAClassCurrencyFormatter;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 * Configuration class for all the MVC matters: view resolver & template engine
 * config, resource handling, message sources, interceptors, controllers'
 * argument resolver, etc.
 * 
 * @author Biava Lorenzo
 *
 */
@Configuration
// @PropertySource("classpath:thymeleaf.properties")
@EnableSpringDataWebSupport
public class MVCConfiguration extends WebMvcConfigurerAdapter {

	private static final String URL_STATIC_BASE = "/static";
	private static final String URL_ASSETS_BASE = URL_STATIC_BASE + "/assets";

	private static final String RES_STATIC_BASE = "/static";
	private static final String RES_ASSETS_BASE = RES_STATIC_BASE + "/assets";

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(URL_ASSETS_BASE + "/js/**")
				.addResourceLocations(RES_ASSETS_BASE + "/js/");
		registry.addResourceHandler(URL_ASSETS_BASE + "/css/**")
				.addResourceLocations(RES_ASSETS_BASE + "/css/");
		registry.addResourceHandler(URL_ASSETS_BASE + "/html/**")
				.addResourceLocations(RES_ASSETS_BASE + "/html/");
		registry.addResourceHandler(URL_ASSETS_BASE + "/img/**")
				.addResourceLocations(RES_ASSETS_BASE + "/img/");
		registry.addResourceHandler(URL_ASSETS_BASE + "/fonts/**")
				.addResourceLocations(RES_ASSETS_BASE + "/fonts/");
	}

	@Bean
	public TemplateResolver templateResolver() {
		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
		templateResolver.setPrefix("/WEB-INF/templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");

		return templateResolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver());
		templateEngine.addDialect(new LayoutDialect());
		templateEngine.addDialect(new SpringSecurityDialect());
		return templateEngine;
	}

	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine());
		viewResolver.setOrder(1);

		return viewResolver;
	}

	/**
	 * This function is used for apply i18n internationalization. With basenames
	 * is specified the location of languages resources.
	 * 
	 * @return messageSource
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames("WEB-INF/templates/languages/messages");
		return messageSource;
	}

	/**
	 * In this function is defined the parameter that permit to change languages
	 * from different country This function customize the request parameter that
	 * the interceptor check for call the setParamName method and then add it to
	 * the registry.
	 * 
	 * @return
	 */
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
		return localeChangeInterceptor;
	}

	/**
	 * In this function is defined the default local language because locale
	 * settings can be customized The default language used in this case is
	 * italian. LocaleResolver is responsible for determining how to retrieve
	 * the locale and how to set the locale
	 * 
	 * @return result
	 */
	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver result = new SessionLocaleResolver();
		result.setDefaultLocale(Locale.ENGLISH);
		return result;
	}

	/***
	 * addInterceptors function need in case user wants a different locale. It
	 * determines how to intercept requests handled in the DispatcherServlet
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	/**
	 * Produces a Spring default multipart-resolver.
	 * 
	 * @return
	 */
	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(10485760);
		return multipartResolver;
	}

	@Override
	public void addArgumentResolvers(
			List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(sortResolver());
		argumentResolvers.add(pageableResolver());
		// argumentResolvers.add(new AuthenticationPrincipalArgumentResolver());
	}

	@Bean
	public PageableHandlerMethodArgumentResolver pageableResolver() {
		PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver(
				sortResolver());
		resolver.setMaxPageSize(100);
		return resolver;
	}

	@Bean
	public SortHandlerMethodArgumentResolver sortResolver() {
		return new SortHandlerMethodArgumentResolver();
	}

	@Bean
	public DateFormatter dateFormatter() {
		return new DateFormatter();
	}

	@Bean
	public CalendarFormatter calendarFormatter() {
		return new CalendarFormatter();
	}

	@Bean
	public FAClassCurrencyFormatter faCurrencyFormatter() {
		return new FAClassCurrencyFormatter();
	}

	/*
	 * @Bean public FormattingConversionServiceFactoryBean conversionService() {
	 * FormattingConversionServiceFactoryBean
	 * formattingConversionServiceFactoryBean= new
	 * FormattingConversionServiceFactoryBean(); Set<Formatter<?>> formatters =
	 * new HashSet<Formatter<?>>(); formatters.add(dateFormatter());
	 * formattingConversionServiceFactoryBean.setFormatters(formatters);
	 * formattingConversionServiceFactoryBean.afterPropertiesSet(); return
	 * formattingConversionServiceFactoryBean; }
	 */

	@Override
	public void addFormatters(FormatterRegistry registry) {
		super.addFormatters(registry);
		registry.addFormatter(dateFormatter());
		registry.addFormatter(calendarFormatter());
		registry.addFormatter(faCurrencyFormatter());
	}

	@EJB(mappedName = DealMgmtBean.JNDIName)
	DealMgmtBean dealBean;

	@Bean
	public Collection<DealCategory> dealCategories() {
		return dealBean.getAllDealCategories();
	}

	public static class SocialShareConfig {
		private String facebookAppId;
		private String googleAppId;
		private String baseSitePath;

		public String getBaseSitePath() {
			return baseSitePath;
		}

		public void setBaseSitePath(String baseSitePath) {
			this.baseSitePath = baseSitePath;
		}

		public String getFacebookAppId() {
			return facebookAppId;
		}

		public void setFacebookAppId(String facebookAppId) {
			this.facebookAppId = facebookAppId;
		}

		public String getGoogleAppId() {
			return googleAppId;
		}

		public void setGoogleAppId(String googleAppId) {
			this.googleAppId = googleAppId;
		}

		public String getGooglePlusShareDeal(Deal deal)
				throws URISyntaxException {
			return getBaseSitePath() + "/deals/" + deal.getId();
		}

		public String getFacebookShareDeal(Deal deal) throws URISyntaxException {
			String href = getBaseSitePath() + "/deals/" + deal.getId();

			return new URIBuilder(
					"https://www.facebook.com/plugins/share_button.php")
					.addParameter("appId", getFacebookAppId())
					.addParameter("href", href)
					.addParameter("layout", "button_count").build().toString();
			// return new URIBuilder("https://www.facebook.com/dialog/feed")
			// .addParameter("appId", getFacebookAppId())
			// .addParameter("display", "popup")
			// .addParameter("caption", "Check out this deal: "+deal.getName())
			// .addParameter("link", href)
			// .addParameter("redirect_uri", href).build().toString();

		}
	}

	@Autowired
	private Environment env;

	@Bean
	public SocialShareConfig socialShareConfig() {
		SocialShareConfig socialShareConfig = new SocialShareConfig();

		socialShareConfig.setFacebookAppId(env.getProperty("facebook.app.id"));
		socialShareConfig.setBaseSitePath("http://localhost:8080/web");

		return socialShareConfig;
	}
}