package it.unito.lmf.web.controllers.accounting;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.web.controllers.BaseController;
import it.unito.lmf.web.security.LMFUserDetails;

import javax.ejb.EJB;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

@Controller
public class LoginController extends BaseController {

	private static final Logger LOG = LoggerFactory
			.getLogger(LoginController.class);

	@EJB(mappedName = UserMgmtBean.JNDIName)
	UserMgmtBean userBean;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage(Model model,
			@RequestParam(required = false) String error,
			NativeWebRequest webRequest) {
		LOG.info("Login page requested.");
		AuthenticationException authEx = (AuthenticationException) webRequest
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, 1);
		if (authEx != null) {
			// ExceptionUtils.getThrowableList(authEx)
			// if (authEx.getCause() instanceof BadCredentialsException) {
			if (authEx.getExtraInformation() instanceof LMFUserDetails) {
				LMFUserDetails user = (LMFUserDetails) authEx
						.getExtraInformation();
				if (user.getSocialSignInProvider() != null) {
					// User attemped to Log in with password, but he's
					// registered with a Social Network
					// TODO redirect automatico al login con social network
					// ?
					String snName = user.getSocialSignInProvider().name();
					snName = snName.substring(0, 1).toUpperCase()
							+ snName.substring(1).toLowerCase();
					setPageError(
							model,
							"You are registered with a Social Network. Try "
									+ snName
									+ " login instead ! <br/> You can also change the password from your account, after logging in, for future logins.");
				}
				// }
			}
		}

		if (error != null && getPageError(model) == null) {
			String errorMsg = "";
			switch (error) {
			case "invalid":
				errorMsg = "Invalid credentials.";
				break;
			case "required":
				errorMsg = "You must login to view this page !";
				break;
			default:
				errorMsg = "Error during login.";
			}
			setPageError(model, errorMsg);
		}

		return getTemplatePath("login");
	}

	// @RequestMapping(value = "/login", method = RequestMethod.POST)
	// public String doLogin(Model model, LoginForm loginForm) {
	// LOG.info("Login request: %", loginForm);
	//
	// model.addAttribute("users", userBean.getAllUsers());
	// return getTemplatePath("accounts");
	// }
}