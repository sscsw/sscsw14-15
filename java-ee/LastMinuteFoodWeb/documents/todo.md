- Denied Access Handler e avviso per Seller Request
- Expire date nel deal (conversione formato)
- Se fallisce geocoding shop, permettere inserimento manuale coordinate
- Ridimensionare immagine deal ? 1MB
- Migliorare Paginazione (sia interfaccia grafica, magari JS, ma soprattutto parametri di ordinamento malfunzionanti nella view)
- Sistemare le pagine d'errore custom

+ Upload immagine del deal (futuro multiple ?)