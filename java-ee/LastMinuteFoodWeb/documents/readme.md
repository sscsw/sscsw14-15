LastMinuteFood - Web Application
===

**NOTES:**

- Base url: `localhost:8080/lmf`.
- Servlets url: as defined in the `@WebServlet` annotation of each servlet.
- Spring MVC-Thymeleaf base url: as defined in `@RequestMapping` annotation of each controller.
- Rest WS: `/rest` +  as defined in `@Path` annotation in each WS.

---
**Additional Informations:**

1. Packages: .
  1.1. it.unito.lmf.web: .
  1.2. it.unito.lmf.web.config: contains all web app configurations, annotation-style.
  1.3. it.unito.lmf.web.controllers: contains Spring MVC controllers (auto-discovered).
  1.4. it.unito.lmf.web.servlets: contains common J2EE Servlets (auto-discovered) [deprecated].
  1.5. it.unito.lmf.web.restful: contains Restful Web Services.
  
 2. Web Resources paths: 
   2.1. Static contents: `src/main/webapp/static/`, then `img`, `css`, etc.
   2.2. Pages templates: `src/main/webapp/WEB-INF/templates`
     2.2.1. Pages:
     2.2.2. Layouts:
     2.2.3. Fragments:
     2.2.4. Languages: one properties file for each language in the form `messages_<i18n id>.properties`