<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="date" class="java.util.Date" />
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pagina non trovata :(</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<h1>Pagina non trovata :(</h1>
	<p>
		Purtroppo la pagina che stai cercando non esiste. <br /> <a href="">Torna
			alla home page</a> oppure <a href="mailto:a@a.it">segnala il problema
			ad un amministratore</a>.
	</p>


	<%--             <li>Status code: <c:out value="${requestScope['javax.servlet.error.status_code']}" /> --%>

</body>
</html>