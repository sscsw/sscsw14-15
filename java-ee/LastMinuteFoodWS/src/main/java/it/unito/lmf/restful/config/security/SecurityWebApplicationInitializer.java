package it.unito.lmf.restful.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Configuration class to start-up the Spring Security Web integration. <br/>
 * Spring Security configuration in placed in {@link SecurityConfiguration}.
 *
 */
public class SecurityWebApplicationInitializer extends
		AbstractSecurityWebApplicationInitializer {

}