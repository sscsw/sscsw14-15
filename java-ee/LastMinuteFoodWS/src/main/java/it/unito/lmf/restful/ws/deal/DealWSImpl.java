package it.unito.lmf.restful.ws.deal;

import it.unito.lmf.biz.deal.DealMgmtBean;
import it.unito.lmf.dal.entities.deal.Deal;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;
import it.unito.lmf.domain.dsl.deal.DealSearchData;
import it.unito.lmf.restful.exceptions.ResourceNotFoundException;
import it.unito.lmf.restful.misc.pagination.PageableParams;
import it.unito.lmf.restful.ws.misc.BaseWS;
import it.unito.lmf.utils.mapping.MappingHelper;
import it.unito.lmf.utils.mapping.RequestParamMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class DealWSImpl extends BaseWS implements DealWS {

	private static final Logger LOG = Logger.getLogger(DealWSImpl.class);
	
	@Inject
	MappingHelper<Deal, DealRepresentation> dealMH;

	@EJB
	DealMgmtBean dealBean;

	@Inject
	@RequestScoped
	@PageableParams
	Pageable pageable;

	// @Override
	// public Collection<DealRepresentation> getAllDeals() {
	// return dealMH.getDSL(dealBean.getAllDeals());
	// }

	@Context
	UriInfo uriInfo;

	@Override
	public Page<DealRepresentation> getAllDealsPaginated() {

		// Extract search parameters
		MultivaluedMap<String, String> queryParams = uriInfo
				.getQueryParameters();

		DealSearchData searchData=null;
		try {
			searchData = RequestParamMapper.mapToPOJO(queryParams,DealSearchData.class,false);
		} catch (Exception e) {
		}

		Page<Deal> page = dealBean.getAllDealsWithSearch(pageable, searchData);
		Page<DealRepresentation> deals = new PageImpl<DealRepresentation>(
				(List<DealRepresentation>) dealMH.getDSL(page.getContent(),
						new ArrayList<DealRepresentation>()), pageable,
				page.getTotalElements());

		return deals;
	}

	@Override
	public DealRepresentation getDeal(Long dealId)
			throws ResourceNotFoundException {
		return dealMH.getDSL(checkResourceExists(dealBean.getDealById(dealId)));
	}
}