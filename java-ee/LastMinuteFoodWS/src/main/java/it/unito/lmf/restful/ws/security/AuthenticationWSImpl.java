package it.unito.lmf.restful.ws.security;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.domain.dsl.accounting.TokenRepresentation;
import it.unito.lmf.domain.dsl.accounting.UserRepresentation;
import it.unito.lmf.restful.exceptions.AuthenticationException;
import it.unito.lmf.utils.mapping.MappingHelper;

import java.util.Date;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthenticationWSImpl implements AuthenticationWS {

	@Inject
	MappingHelper<User, UserRepresentation> userMH;

	@EJB
	UserMgmtBean userBean;

	@Inject
	PasswordEncoder passwordEncoder;

	@Override
	public TokenRepresentation getToken(String authHeader) {
		try {
			authHeader=new String(Base64.decode(StringUtils.replaceOnce(authHeader,"Basic ","").getBytes()));
			String username = authHeader.split(":")[0];
			String password = authHeader.split(":")[1];
			User user = userBean.getUserByEmail(username);
			if(user==null)
				throw new RuntimeException("Wrong credentials");
			if(user.getPassword()==null)
				throw new RuntimeException("Social Network authentication required");
			if (!passwordEncoder.matches(password, user.getPassword()))
				throw new RuntimeException("Wrong credentials");

			TokenRepresentation token = new TokenRepresentation();
			token.setId("DUMMY-TOKEN");
			token.setUser(user.getId());
			token.setCreatedAt(new Date());
			token.setExpiresAt(new Date());
			return token;
		} catch (Exception e) {
			throw new AuthenticationException("Authentication Failed: "+e.getMessage());
		}
	}
}
