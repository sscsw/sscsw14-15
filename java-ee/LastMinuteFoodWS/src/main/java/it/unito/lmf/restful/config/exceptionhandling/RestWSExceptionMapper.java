package it.unito.lmf.restful.config.exceptionhandling;

import it.unito.lmf.domain.dsl.misc.ErrorRepresentation;
import it.unito.lmf.domain.dsl.misc.GeneralErrorCodes;
import it.unito.lmf.restful.exceptions.AuthenticationException;
import it.unito.lmf.restful.exceptions.DuplicatedResourceException;
import it.unito.lmf.restful.exceptions.ResourceNotFoundException;
import it.unito.lmf.restful.exceptions.RestWSException;
import it.unito.lmf.utils.misc.ErrorHelper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RestWSExceptionMapper implements ExceptionMapper<RestWSException> {

	public Response toResponse(RestWSException exception) {

		boolean verbose = true;

		ErrorResponse errResp = findExceptionErrorMapping(exception, verbose);
		if (errResp == null) {
			ErrorRepresentation error = new ErrorRepresentation(
					GeneralErrorCodes.LMF_ERR_GENERIC);
			error.setVerbose(ErrorHelper.getStackTraceToString(exception));
			errResp = new ErrorResponse(error, Status.INTERNAL_SERVER_ERROR);
		}
		// errorsList.add(errResp.getError());

		return Response.status(errResp.getStatus()).entity(errResp.getError())
				.type(MediaType.APPLICATION_JSON).build();
	}

	protected ErrorResponse findExceptionErrorMapping(
			RuntimeException exception, boolean verbose) {
		ErrorResponse errResp = null;
		ErrorRepresentation error = null;
		
		if (exception instanceof DuplicatedResourceException) {
			error = new ErrorRepresentation(
					GeneralErrorCodes.LMF_ERR_RESOURCE_ALREADY_EXISTS);
			if (verbose)
				error.setVerbose(exception.getMessage());

			errResp = new ErrorResponse(error, Status.BAD_REQUEST);
		} else if (exception instanceof ResourceNotFoundException) {
			error = new ErrorRepresentation(
					GeneralErrorCodes.LMF_ERR_RESOURCE_NOT_FOUND);
			if (verbose)
				error.setVerbose(exception.getMessage());

			errResp = new ErrorResponse(error, Status.NOT_FOUND);
		
	} else if (exception instanceof AuthenticationException) {
		error = new ErrorRepresentation(
				GeneralErrorCodes.LMF_ERR_UNAUTHENTICATED);
		if (verbose)
			error.setVerbose(exception.getMessage());

		errResp = new ErrorResponse(error, Status.UNAUTHORIZED);
	}
		return errResp;
	}

	protected class ErrorResponse {
		private ErrorRepresentation error;
		private StatusType status;

		public ErrorResponse(ErrorRepresentation error, StatusType status) {
			super();
			this.error = error;
			this.status = status;
		}

		public ErrorRepresentation getError() {
			return error;
		}

		public void setError(ErrorRepresentation error) {
			this.error = error;
		}

		public StatusType getStatus() {
			return status;
		}

		public void setStatus(StatusType status) {
			this.status = status;
		}
	}

}