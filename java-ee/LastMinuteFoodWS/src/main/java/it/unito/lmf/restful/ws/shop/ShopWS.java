package it.unito.lmf.restful.ws.shop;

import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

@Path("/shops")
public interface ShopWS {

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Page<ShopRepresentation> getAllShopsPaginated();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{shopId}")
	public ShopRepresentation getShop(@PathParam("shopId") Long dealId);
}