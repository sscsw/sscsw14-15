package it.unito.lmf.restful.ws.misc;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/test")
public class TestWS {

	public class TestEntity {
		private String status;
		private Date currentTime;
		
		public String getStatus() {
			return status;
		}
		
		public void setStatus(String status) {
			this.status = status;			
		}
		
		public Date getCurrentTime() {
			return currentTime;
		}
		
		public void setCurrentTime(Date currentTime) {
			this.currentTime = currentTime;
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//@Path("/")
	public Response test() {
		TestEntity result = new TestEntity();
		result.setStatus("RUNNING");
		result.setCurrentTime(new Date());
		return Response.ok(result).build();
	}
}