package it.unito.lmf.restful.config.security;

import it.unito.lmf.restful.security.DBUserDetailsService;

import javax.enterprise.inject.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration class for all the security realms of Spring Security. <br/>
 * Note that there are two realms: one unauthenticated for RESTful WS (/rest/*)
 * and one for the website (/web/*).
 * 
 * @author bln
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
// , mode = AdviceMode.ASPECTJ, proxyTargetClass = true, prePostEnabled=true)
public class SecurityConfiguration {

	/**
	 * Configures the {@link AuthenticationManager} for using the custom
	 * {@link DBUserDetailsService} and related {@link PasswordEncoder}.
	 * 
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(
				passwordEncoder());
	}

	@Configuration
	@Order(1)
	public static class WebsiteWebSecurityConfigurerAdapter extends
			WebSecurityConfigurerAdapter {

		@Override
		public void configure(WebSecurity web) throws Exception {
		}

		/**
		 * Defines the web based security configuration.
		 * 
		 * @param http
		 *            It allows configuring web based security for specific http
		 *            requests.
		 * @throws Exception
		 */
		@Override
		protected void configure(HttpSecurity http) throws Exception {

			String PREFIX = "/ws/rest";
			// @formatter:off
			http
			// .exceptionHandling().authenticationEntryPoint(authenticationExceptionEntryPoint())
			// .and()
			.antMatcher(PREFIX + "/**")
					.authorizeRequests()
					// Routes privileges
					.antMatchers(PREFIX + "/auth/**")
					.permitAll()
					// TODO Restore to restrict access
					// .antMatchers(PREFIX+"/workgroups/*/paas/**").hasRole("WEBUI")
					// .anyRequest()
					// .authenticated()
					.and().sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

			// org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter
			// .and()
			// Errors pages overrides
			// .exceptionHandling()
			// .accessDeniedHandler(lmfAccessDeniedHandler())
			// .accessDeniedPage(PREFIX + "/errors/access_denied")
			// .and()
			// Logout
			// .logout()
			// .logoutUrl(PREFIX + "/logout")
			// .logoutRequestMatcher(
			// new AntPathRequestMatcher(PREFIX + "/logout", "GET"))
			// .logoutSuccessUrl(PREFIX + "/")
			// .and()
			// // Login
			// .formLogin()
			// .usernameParameter("username")
			// .passwordParameter("password")
			// .loginPage(PREFIX + "/login?error=required")
			// .failureUrl(PREFIX + "/login?error=invalid")
			// .loginProcessingUrl(PREFIX + "/login")
			// .defaultSuccessUrl(ROUTE_LOGIN_SUCCESFUL)
			// .permitAll()
			// .and()
			// @formatter:on
		}

		/**
		 * Produces a LMFAccessDeniedHandler.
		 * 
		 * @return
		 */
		// @Bean
		// public LMFAccessDeniedHandler lmfAccessDeniedHandler() {
		// return new LMFAccessDeniedHandler();
		// }

		// @Bean
		// public AuthenticationTokenProcessingFilter
		// authenticationTokenProcessingFilter() {
		// return new AuthenticationTokenProcessingFilter(
		// authenticationExceptionEntryPoint());
		// }
		//
		// @Bean
		// public AuthenticationExceptionEntryPoint
		// authenticationExceptionEntryPoint() {
		// return new AuthenticationExceptionEntryPoint();
		// }
	}

	/**
	 * Produces a password encoder for users password encryption.
	 * 
	 * @return
	 */
	@Bean
	@Produces
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(10);
	}

	// @Bean
	// public SocialUserDetailsService socialUserDetailsService() {
	// return new LMFSocialUserDetailsService();
	// }

	/**
	 * Produces a DB implementation of a {@link UserDetailsService}.
	 * 
	 * @return
	 */
	@Bean
	public DBUserDetailsService userDetailsService() {
		return new DBUserDetailsService();
	}

}