package it.unito.lmf.restful.config.filters;

import it.unito.lmf.domain.dsl.misc.Pagination;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

/**
 * This filter can be used to enrich RESTful responses with additional
 * pagination information in the headers. <br/>
 * The entity type MUST be {@link Page}.
 * 
 * @author l.biava
 *
 */
@Provider
public class PageableResponseFilter implements WriterInterceptor {

	private static final Logger LOG = LoggerFactory
			.getLogger(PageableResponseFilter.class);

	@Context
	protected HttpServletResponse response;

	@Context
	protected HttpServletRequest request;

	@Override
	public void aroundWriteTo(WriterInterceptorContext context)
			throws IOException, WebApplicationException {

		try {
			if (context.getEntity() instanceof Page) {
				Page<?> page = (Page<?>) context.getEntity();									

				response.setHeader(Pagination.HEADER_PAG_PAGE_SIZE,
						"" + page.getSize());
				response.setHeader(Pagination.HEADER_PAG_PAGE_NUMBER,
						"" + page.getNumber());
				response.setHeader(Pagination.HEADER_PAG_TOTAL_COUNT,
						"" + page.getTotalElements());
				response.setHeader(Pagination.HEADER_PAG_TOTAL_PAGES,
						"" + page.getTotalPages());
				// response.setHeader("Link",
				// getLinkHeader(request.getRequestURL().toString(),
				// request.getQueryString(),page));				
				
				// Update context entity data and type
				if(request.getHeader(Pagination.HEADER_PAG_OUTPUT_ENCODING)!=null && request.getHeader(Pagination.HEADER_PAG_OUTPUT_ENCODING).equals("envelope")) {
					//TODO: Envelope with spring hateoas ?
					context.setType(page.getContent().getClass());
					//context.setGenericType(page.getContent().getClass().);
					context.setEntity(page.getContent());
				}
				else
				{
				context.setType(page.getContent().getClass());
				//context.setGenericType(page.getContent().getClass().);
				context.setEntity(page.getContent());
				}
			}
		} catch (Exception ex) {
			LOG.error(
					"Unable to add pagination headers to the response. Error: "
							+ ex.getMessage(), ex);
		}

		context.proceed();
	}

	// private String getNextLink(String baseURL, String queryString,
	// Page<?> page) {
	// if (queryString == null)
	// queryString = "";
	//
	// String offsetPattern = "(" + RestWSPaginationParams.QUERY_PARAM_OFFSET
	// + "=)(\\d+)";
	//
	// String newOffset = RestWSPaginationParams.QUERY_PARAM_OFFSET + "="
	// + (pagination.getOffset() + pagination.getLimit());
	//
	// if (queryString.contains(RestWSPaginationParams.QUERY_PARAM_OFFSET
	// + "="))
	// queryString = queryString.replaceAll(offsetPattern, newOffset);
	// else
	// queryString += (queryString.equals("") ? "" : "&") + newOffset;
	//
	// return baseURL + "?" + queryString;
	// }
	//
	// private String getLinkHeader(String baseURL, String queryString,
	// Pagination pagination) {
	//
	// // Map of Rel - URL for links
	// Map<String,String> links = new HashMap<String,String>();
	//
	// links.put("next", getNextLink(baseURL, queryString, pagination));
	// links.put("last", getNextLink(baseURL, queryString, pagination));
	//
	// String linkHeader = "";
	// for(Map.Entry<String,String> link: links.entrySet()) {
	// linkHeader += "<"+link.getValue()+">; rel=\""+link.getKey()+"\",";
	// }
	//
	// if(links.size()>0)
	// linkHeader=linkHeader.substring(0, linkHeader.length()-1);
	//
	// return linkHeader;
	// }
}
