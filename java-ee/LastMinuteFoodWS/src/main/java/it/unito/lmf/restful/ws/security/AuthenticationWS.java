package it.unito.lmf.restful.ws.security;

import it.unito.lmf.domain.dsl.accounting.TokenRepresentation;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/auth")
public interface AuthenticationWS {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/token")
	public TokenRepresentation getToken(
			@HeaderParam("Authorization") String authHeader);
}