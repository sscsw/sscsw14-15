package it.unito.lmf.restful.misc.pagination;

import java.lang.reflect.Method;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * This class contains a single producer for {@link Pageable}, quailified with the {@link PageableParams} annotation.
 * 
 * @author l.biava
 *
 */
@Singleton
public class PageableParamsProducer {

	PageableHandlerMethodArgumentResolver resolver;
	MethodParameter methodParameter;

	@PostConstruct
	private void init() {
		this.resolver = getPageableHandlerMethodArgumentResolver();

		try {
			Method method = this.getClass().getDeclaredMethod("dummyMethod",
					Pageable.class);
			this.methodParameter = new MethodParameter(method, 0);
		} catch (Exception e) {
			throw new Error("PageableParamsProducer error: " + e.getMessage(),
					e);
		}
	}

	/**
	 * Override to customize the {@link PageableHandlerMethodArgumentResolver}.
	 * 
	 * @return
	 */
	protected PageableHandlerMethodArgumentResolver getPageableHandlerMethodArgumentResolver() {
		PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver(
				new SortHandlerMethodArgumentResolver());
		
		resolver.setFallbackPageable(new PageRequest(0, 20));
		resolver.setMaxPageSize(100);
		
		return resolver;
	}

	/**
	 * This method produces a {@link Pageable}, using data from the current
	 * {@link HttpServletRequest}.
	 * 
	 * 
	 * @param request
	 *            the current WS request.
	 * @return an instance of {@link Pageable}, populated with the data found in
	 *         the request.
	 */
	@RequestScoped
	@Produces
	@PageableParams
	public Pageable createParamsContainer(HttpServletRequest request) {

		/**
		 * Using
		 * {@link PageableHandlerMethodArgumentResolver#resolveArgument(MethodParameter, org.springframework.web.method.support.ModelAndViewContainer, NativeWebRequest, org.springframework.web.bind.support.WebDataBinderFactory)}
		 * to build the {@link Pageable}. Mocking data to recycle that
		 * implementation. NOT GOOD !
		 */

		return resolver.resolveArgument(methodParameter, null,
				new ServletWebRequest(request), null);
	}

	protected void dummyMethod(Pageable pageable) {

	}

}
