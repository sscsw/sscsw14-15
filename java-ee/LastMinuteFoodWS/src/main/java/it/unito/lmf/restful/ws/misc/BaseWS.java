package it.unito.lmf.restful.ws.misc;

import it.unito.lmf.restful.exceptions.ResourceNotFoundException;

import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

@RequestScoped
public abstract class BaseWS {

	@Context
	private HttpServletResponse servletResponse;

	@Context
	private HttpServletRequest servletRequest;

	// @Inject
	// @PrismaRestWSParams
	// private RestWSParamsContainer restWSParams;
	//
	// /**
	// * @return the {@link RestWSParamsContainer}, containing common RESTful
	// * WebService parameters (pagination, filtering, ordering, etc), for
	// * the current session.
	// */
	// protected RestWSParamsContainer getSessionWSParams() {
	// return restWSParams;
	// }

	/**
	 * @return the {@link HttpServletRequest} for the current session.
	 */
	protected HttpServletRequest getSessionServletRequest() {
		return servletRequest;
	}

	/**
	 * @return the {@link HttpServletResponse} for the current session.
	 */
	protected HttpServletResponse getSessionServletResponse() {
		return servletResponse;
	}

	// protected <ResultType> Response handleNotFoundException(String itemName)
	// {
	// return PrismaResponse
	// .status(Status.NOT_FOUND,
	// OrchestratorErrorCode.ORC_ITEM_NOT_FOUND, itemName)
	// .build().build();
	// }

	// protected <ResultType> Response handleNotFoundException(
	// ErrorCode errorCode, String verbose) {
	// return PrismaResponse.status(Status.NOT_FOUND, errorCode, verbose)
	// .build().build();
	// }
	//
	// protected <ResultType> Response handleError(StatusType status,
	// ErrorCode errorCode, String verbose) {
	// return PrismaResponse.status(status, errorCode, verbose).build()
	// .build();
	// }
	//
	// protected <ResultType> Response handleError(StatusType status,
	// ErrorCode errorCode, Exception e) {
	// return PrismaResponse
	// .status(status, errorCode, StackTrace.getStackTraceToString(e))
	// .build().build();
	// }
	//
	// protected <ResultType> Response handleResult(ResultType result) {
	// return PrismaResponse.status(Status.OK, result).build().build();
	// }

	// protected void handleGenericException(Exception e) {
	// e.printStackTrace();
	//
	// // Unexpected error occurred (probably server error)
	// return PrismaResponse
	// .status(Status.INTERNAL_SERVER_ERROR,
	// OrchestratorErrorCode.ORC_GENERIC_ERROR,
	// StackTrace.getStackTraceToString(e)).build().build();
	// // return Response
	// // .status(Status.OK)
	// // .entity(PrismaResponseWrapper.error(
	// // OrchestratorErrorCode.ORC_GENERIC_ERROR,
	// // e.getStackTrace().toString()).build()).build();
	// }
	
	/**
	 * Verifies whether the given resource exists (it's not <tt>null</tt>) and
	 * throws the related {@link ResourceNotFoundException} in case it does not.
	 * 
	 * @param resource
	 *            the resource to be checked.
	 * @throws ResourceNotFoundException
	 *             if the resource does not exist (it's <tt>null</tt>).
	 */
	protected <T> T checkResourceExists(T resource)
			throws ResourceNotFoundException{
		if (resource == null)
			throw new ResourceNotFoundException();
		
		return resource;
	}
}