package it.unito.lmf.restful.ws.accounting;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.dal.entities.accounting.User;
import it.unito.lmf.domain.dsl.accounting.UserRepresentation;
import it.unito.lmf.restful.exceptions.DuplicatedResourceException;
import it.unito.lmf.utils.mapping.MappingHelper;

import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;

public class AccountingWSImpl implements AccountingWS {

	@Inject
	MappingHelper<User, UserRepresentation> userMH;

	@EJB
	UserMgmtBean userBean;

	@Override
	public Collection<UserRepresentation> getAllUsers() {
		return userMH.getDSL(userBean.getAllUsers());
	}

	@Override
	public UserRepresentation addUser(String username,
			HttpServletResponse response) {
		try {
			response.setStatus(Status.CREATED.getStatusCode());
			// TODO: solo username?
			User u = new User();
			u.setUsername(username);
			return userMH.getDSL(userBean.createNewUser(u));
		} catch (Exception e) {
			throw new DuplicatedResourceException(UserRepresentation.class,
					"username");
		}
	}
}
