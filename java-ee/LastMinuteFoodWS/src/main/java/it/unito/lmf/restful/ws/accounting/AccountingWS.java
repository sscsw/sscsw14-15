package it.unito.lmf.restful.ws.accounting;

import it.unito.lmf.domain.dsl.accounting.UserRepresentation;

import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/accounting")
public interface AccountingWS {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users")
	public Collection<UserRepresentation> getAllUsers();

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/{username}")
	public UserRepresentation addUser(@PathParam("username") String username,
			@Context final HttpServletResponse response);
}