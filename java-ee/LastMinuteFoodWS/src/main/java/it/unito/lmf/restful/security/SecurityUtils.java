package it.unito.lmf.restful.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
	private static final Logger LOG = LoggerFactory
			.getLogger(SecurityUtils.class);

	public static void logInUser(LMFUserDetails userDetails) {
		LOG.debug("Logging in principal: {}", userDetails);

		Authentication authentication = new UsernamePasswordAuthenticationToken(
				userDetails, null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(authentication);

		LOG.info("User: {} has been logged in.", userDetails);
	}
}
