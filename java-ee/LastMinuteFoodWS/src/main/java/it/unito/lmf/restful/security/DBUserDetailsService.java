package it.unito.lmf.restful.security;

import it.unito.lmf.biz.accounting.UserMgmtBean;
import it.unito.lmf.dal.entities.accounting.User;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Used to load user-specific data from the DB. It's used by the security
 * framework to load user authentication & authorization data.
 * 
 * @author Biava Lorenzo
 *
 */
public class DBUserDetailsService implements UserDetailsService { // TODO:
																	// refactor
																	// della
																	// classe in
																	// LMFUserDetailsService

	@EJB(mappedName = UserMgmtBean.JNDIName)
	UserMgmtBean userBean;

	public LMFUserDetails loadUserById(Long id)
			throws UsernameNotFoundException {
		User user = userBean.getUserById(id);

		if (user == null){
			throw new UsernameNotFoundException("No user found with id: "
					+ id);
		}
		if (!user.isEnabled()) { //Should already be checked by Spring...
			throw new UsernameNotFoundException("The user "
					+ id + " is not enabled.");
		}

		return buildPrincipal(user);
	}
	
	@Override
	public LMFUserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = userBean.getUserByEmail(username);

		if (user == null){
			throw new UsernameNotFoundException("No user found with username: "
					+ username);
		}
		if (!user.isEnabled()) { //Should already be checked by Spring...
			throw new UsernameNotFoundException("The user "
					+ username + " is not enabled.");
		}

		return buildPrincipal(user);
	}
	
	protected LMFUserDetails buildPrincipal(User user) {
		List<UserRole> roles = new ArrayList<UserRole>();
		roles.add(UserRole.ROLE_USER);
		if (user.isAdmin())
			roles.add(UserRole.ROLE_ADMIN);
		if (user.isSeller() && user.getSeller().isAuthorized())
			roles.add(UserRole.ROLE_SELLER);

		LMFUserDetails principal = LMFUserDetails
				.getBuilder()
				.firstName(user.getFirstName())
				.id(user.getId())
				.lastName(user.getLastName())
				.password(user.getPassword())
				.roles(roles)
				.username(user.getUsername()).internalUser(user).build();

		return principal;
	}
}
