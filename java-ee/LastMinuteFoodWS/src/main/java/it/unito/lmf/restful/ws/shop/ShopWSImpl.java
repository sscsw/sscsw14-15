package it.unito.lmf.restful.ws.shop;

import it.unito.lmf.biz.shop.ShopMgmtBean;
import it.unito.lmf.dal.entities.shop.Shop;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;
import it.unito.lmf.restful.exceptions.ResourceNotFoundException;
import it.unito.lmf.restful.misc.pagination.PageableParams;
import it.unito.lmf.restful.ws.misc.BaseWS;
import it.unito.lmf.utils.mapping.MappingHelper;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class ShopWSImpl extends BaseWS implements ShopWS {

	private static final Logger LOG = Logger.getLogger(ShopWSImpl.class);

	@Inject
	MappingHelper<Shop, ShopRepresentation> shopMH;

	@EJB
	ShopMgmtBean shopBean;

	@Inject
	@RequestScoped
	@PageableParams
	Pageable pageable;

	@Context
	UriInfo uriInfo;

	@Override
	public Page<ShopRepresentation> getAllShopsPaginated() {

		Page<Shop> page = shopBean.getAllAuthorizedShops(pageable);
		Page<ShopRepresentation> deals = new PageImpl<ShopRepresentation>(
				(List<ShopRepresentation>) shopMH.getDSL(page.getContent(),
						new ArrayList<ShopRepresentation>()), pageable,
				page.getTotalElements());

		return deals;
	}

	@Override
	public ShopRepresentation getShop(Long dealId)
			throws ResourceNotFoundException {
		return shopMH.getDSL(checkResourceExists(shopBean.getShopById(dealId)));
	}
}