package it.unito.lmf.utils.mapping.accounting;

import it.unito.lmf.dal.entities.accounting.Seller;
import it.unito.lmf.domain.dsl.deal.SellerRepresentation;
import it.unito.lmf.utils.mapping.MappingHelperBase;

import javax.faces.bean.ApplicationScoped;

@ApplicationScoped
public class SellerMappingHelper extends
		MappingHelperBase<Seller, SellerRepresentation> {

	@Override
	public Seller getEntity(SellerRepresentation o) {
		throw new UnsupportedOperationException("TO IMPLEMENT");
	}

	@Override
	public SellerRepresentation getDSL(Seller o) {
		SellerRepresentation dsl = new SellerRepresentation();

		dsl.setId(o.getId());
		dsl.setCodiceFiscale(o.getCodiceFiscale());
		dsl.setEmail(o.getUser().getEmail());
		dsl.setFirstName(o.getUser().getFirstName());
		dsl.setLastName(o.getUser().getLastName());
		dsl.setPhoneNumber(o.getPhoneNumber());
		dsl.setRagioneSociale(o.getRagioneSociale());
		dsl.setUsername(o.getUser().getUsername());

		return dsl;
	}
}
