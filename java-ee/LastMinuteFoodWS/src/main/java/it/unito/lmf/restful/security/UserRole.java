package it.unito.lmf.restful.security;

/**
 * Represents the possible roles for a {@link LMFUserDetails}.
 * 
 * @author Biava Lorenzo
 *
 */
public enum UserRole {
	ROLE_USER, ROLE_SELLER, ROLE_ADMIN
}
