package it.unito.lmf.restful.exceptions;

import it.unito.lmf.domain.dsl.misc.ErrorCode;

import javax.ws.rs.core.Response.Status;

/**
 * Exception for returning error in Prisma WS. <br/>
 * Custom parameters can be specified: <tt>status</tt>, <tt>errorCode</tt>,
 * <tt>verbose</tt>.
 * 
 * @author l.biava
 *
 */
public class RestWSException extends RuntimeException {

	private static final long serialVersionUID = -5129288618082097967L;

	private Status status;
	private ErrorCode errorCode;
	private String verbose;

	public RestWSException() {
		super();
	}

	public RestWSException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RestWSException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestWSException(String message) {
		super(message);
	}

	public RestWSException(Throwable cause) {
		super(cause);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public String getVerbose() {
		return verbose;
	}

	public void setVerbose(String verbose) {
		this.verbose = verbose;
	}

	/**
	 * Pseudo-builder method.
	 * 
	 * @param status
	 * @return
	 */
	public RestWSException status(Status status) {
		this.status = status;
		return this;
	}

	/**
	 * Pseudo-builder method.
	 * 
	 * @param status
	 * @return
	 */
	public RestWSException errorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * Pseudo-builder method.
	 * 
	 * @param status
	 * @return
	 */
	public RestWSException verbose(String verbose) {
		this.verbose = verbose;
		return this;
	}

}
