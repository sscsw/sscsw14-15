package it.unito.lmf.restful.misc.pagination;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;

/**
 * Qualifies the injection of {@link Pageable} parameters from an {@link HttpServletRequest}.
 * @author Biava Lorenzo
 *
 */
@Target({ PARAMETER, TYPE, METHOD, FIELD })
@Retention(RUNTIME)
@Documented
public @interface PageableParams {
}