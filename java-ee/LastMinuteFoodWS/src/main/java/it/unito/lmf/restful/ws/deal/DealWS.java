package it.unito.lmf.restful.ws.deal;

import it.unito.lmf.domain.dsl.deal.DealRepresentation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

@Path("/deals")
public interface DealWS {

	// @GET
	// @Produces(MediaType.APPLICATION_JSON)
	// @Path("")
	// public Collection<DealRepresentation> getAllDeals();

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Page<DealRepresentation> getAllDealsPaginated();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dealId}")
	public DealRepresentation getDeal(@PathParam("dealId") Long dealId);
}