package it.unito.lmf.utils.mapping;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;

import com.google.common.base.CaseFormat;
import com.sun.xml.xsom.impl.scd.Iterators.Map;

/**
 * Contains utility methods for mapping parameters maps into POJOs.
 * 
 * @author Biava Lorenzo
 *
 */
public class RequestParamMapper {

	private static final Logger LOG = Logger
			.getLogger(RequestParamMapper.class);

	/**
	 * @see RequestParamMapper#mapToPOJO(MultivaluedMap, Object, boolean) This
	 *      method instantiate the given POJO type (<b>there MUST be an empty
	 *      constructor !)</b>.
	 * 
	 * @param pojoType
	 *            the type of the POJO to populate - will be instantiated.
	 * @return the populated POJO.
	 * @throws InstantiationException
	 *             if the POJO instantiation fails.
	 * @throws IllegalAccessException
	 *             if the POJO instantiation fails.
	 */
	public static <T> T mapToPOJO(MultivaluedMap<String, String> params,
			Class<T> pojoType, boolean signalError)
			throws InstantiationException, IllegalAccessException, Exception {

		T pojo = pojoType.newInstance();

		return mapToPOJO(params, pojo, signalError);
	}

	/**
	 * Populates the given POJO with parameters specified.<br/>
	 * The lookup is done searching for the POJO's fields inside the map and
	 * trying to assign the first value, if found.
	 * 
	 * @param params
	 *            the {@link Map} of parameter to map into the POJO.
	 * @param pojo
	 *            an instance of the POJO to populate.
	 * @param signalError
	 *            defines whether to throw an exception in case of error mapping
	 *            a field.
	 * @return the populated POJO.
	 */
	public static <T> T mapToPOJO(MultivaluedMap<String, String> params,
			T pojo, boolean signalError) throws Exception {

		for (Field field : pojo.getClass().getDeclaredFields()) {
			if (params.containsKey(field.getName())) {
				String param = params.getFirst(field.getName());

				try {
					String setterName = "set"
							+ CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL,
									field.getName());

					Method setter = pojo.getClass().getMethod(setterName,
							field.getType());

					Class<?> fieldType = field.getType();
					Object fieldValue = params.getFirst(field.getName());

					if (fieldType.equals(Integer.TYPE)
							|| fieldType.equals(Integer.class)) {
						fieldValue = Integer.valueOf(param);
					} else if (fieldType.equals(Double.TYPE)
							|| fieldType.equals(Double.class)) {
						fieldValue = Double.valueOf(param);
					} else if (fieldType.equals(Boolean.TYPE)
							|| fieldType.equals(Boolean.class)) {
						fieldValue = Boolean.valueOf(param);
					} else if (fieldType.equals(Long.TYPE)
							|| fieldType.equals(Long.class)) {
						fieldValue = Long.valueOf(param);
					}

					setter.invoke(pojo, fieldValue);
				} catch (Exception e) {
					LOG.warn("Failed to map parameter " + field
							+ " with value " + param + " to related field: "
							+ e.getMessage(), e);

					if (signalError)
						throw e;
				}
			}
		}
		return pojo;
	}
}
