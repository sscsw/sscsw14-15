<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<!-- The pom builds the web WAR artifact. -->

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>lmf</artifactId>
		<groupId>it.unito.lmf</groupId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<artifactId>lmf-ee-ws</artifactId>
	<packaging>war</packaging>

	<name>Last Minute Food - J2EE Application - Web Services module</name>
	<description>Last Minute Food - J2EE Application - Web Services module

This module implements the application WebServices (currently only REST), using a default context-root of /ws.</description>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-framework-bom</artifactId>
				<version>${version.spring}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>

		<!-- Import the EJB project -->
		<dependency>
			<artifactId>lmf-ee-ejb</artifactId>
			<groupId>it.unito.lmf</groupId>
			<type>ejb</type>
			<scope>provided</scope>
		</dependency>

		<!-- Import the DAL project -->
		<dependency>
			<artifactId>lmf-ee-dal</artifactId>
			<groupId>it.unito.lmf</groupId>
			<type>ejb</type>
			<scope>provided</scope>
		</dependency>

		<!-- Import the Domain project -->
		<dependency>
			<artifactId>lmf-ee-domain</artifactId>
			<groupId>it.unito.lmf</groupId>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>

		<!-- Import the Utility project -->
		<dependency>
			<groupId>it.unito.lmf</groupId>
			<artifactId>lmf-ee-utility</artifactId>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>6.0</version>
			<scope>provided</scope>
		</dependency>

		<!-- Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Spring Security -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${version.spring-security}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${version.spring-security}</version>
			<scope>provided</scope>
		</dependency>

		<!-- Spring for pagination facilities -->
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-commons</artifactId>
			<version>${version.spring-data}</version>
			<scope>provided</scope>
		</dependency>

		<!-- Validation provider -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>${version.hibernate-validator}</version>
		</dependency>

		<!-- Provider for JAX-RS JSON mapping -->
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<version>2.4.3</version>
		</dependency>

		<!-- Jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.4.3</version>
		</dependency>

		<!-- Utility Libraries -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.3.2</version>
			<scope>provided</scope>
		</dependency>

		<!-- Apache Commons FileUpload -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.1</version>
			<scope>provided</scope>
		</dependency>

		<!-- Apache Commons IO -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
			<scope>provided</scope>
		</dependency>

		<!-- Testing -->
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.8.4</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<!-- Set the name of the war, used as the context root when the app is 
			deployed -->
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<version>${version.war.plugin}</version>
				<configuration>
					<!-- Java EE 7 doesn't require web.xml, Maven needs to catch up! -->
					<failOnMissingWebXml>true</failOnMissingWebXml>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>