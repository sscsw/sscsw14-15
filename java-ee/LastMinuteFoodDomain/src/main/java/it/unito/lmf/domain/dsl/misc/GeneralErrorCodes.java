package it.unito.lmf.domain.dsl.misc;

/**
 * General error codes.
 * 
 * @author l.biava
 * 
 */
public enum GeneralErrorCodes implements ErrorCode {
	LMF_ERR_GENERIC(500L, "A generic error occurred."),
	LMF_ERR_CLIENT(400L, "A client error occurred."),
	LMF_ERR_WS_NOT_FOUND(404L, "The specified Web Service does not exist."),
	LMF_ERR_RESOURCE_NOT_FOUND(404L, "The specified resource does not exist."),
	LMF_ERR_RESOURCE_ALREADY_EXISTS(1405L, "The specified resource already exists."),
	LMF_ERR_UNAUTHENTICATED(1401L, "Authentication required."),
	LMF_ERR_UNAUTHORIZED(1403L, "Not authorized to access the requested resource.");

	private final Long code;
	private final String description;

	private GeneralErrorCodes(Long code, String description) {

		this.code = code;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public Long getCode() {
		return code;
	}

	public String getName() {
		return name();
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}

	public GeneralErrorCodes lookupFromCode(Long errorCode) {
		for (GeneralErrorCodes e : values()) {
			if (e.code == errorCode) {
				return e;
			}
		}
		return null;
	}

	public GeneralErrorCodes lookupFromName(String errorName) {
		for (GeneralErrorCodes e : values()) {
			if (errorName.equals(e.name())) {
				return e;
			}
		}
		return null;
	}
}