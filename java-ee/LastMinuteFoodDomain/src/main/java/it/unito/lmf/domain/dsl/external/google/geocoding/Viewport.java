package it.unito.lmf.domain.dsl.external.google.geocoding;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "northeast", "southwest" })
public class Viewport {

	@JsonProperty("northeast")
	private Northeast northeast;
	@JsonProperty("southwest")
	private Southwest southwest;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The northeast
	 */
	@JsonProperty("northeast")
	public Northeast getNortheast() {
		return northeast;
	}

	/**
	 * 
	 * @param northeast
	 *            The northeast
	 */
	@JsonProperty("northeast")
	public void setNortheast(Northeast northeast) {
		this.northeast = northeast;
	}

	/**
	 * 
	 * @return The southwest
	 */
	@JsonProperty("southwest")
	public Southwest getSouthwest() {
		return southwest;
	}

	/**
	 * 
	 * @param southwest
	 *            The southwest
	 */
	@JsonProperty("southwest")
	public void setSouthwest(Southwest southwest) {
		this.southwest = southwest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(northeast).append(southwest)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Viewport) == false) {
			return false;
		}
		Viewport rhs = ((Viewport) other);
		return new EqualsBuilder().append(northeast, rhs.northeast)
				.append(southwest, rhs.southwest)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}