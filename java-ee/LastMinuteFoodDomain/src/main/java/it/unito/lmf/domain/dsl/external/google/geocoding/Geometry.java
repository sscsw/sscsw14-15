package it.unito.lmf.domain.dsl.external.google.geocoding;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "location", "location_type", "viewport" })
public class Geometry {

	@JsonProperty("location")
	private Location location;
	@JsonProperty("location_type")
	private String locationType;
	@JsonProperty("viewport")
	private Viewport viewport;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The location
	 */
	@JsonProperty("location")
	public Location getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 *            The location
	 */
	@JsonProperty("location")
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * 
	 * @return The locationType
	 */
	@JsonProperty("location_type")
	public String getLocationType() {
		return locationType;
	}

	/**
	 * 
	 * @param locationType
	 *            The location_type
	 */
	@JsonProperty("location_type")
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	/**
	 * 
	 * @return The viewport
	 */
	@JsonProperty("viewport")
	public Viewport getViewport() {
		return viewport;
	}

	/**
	 * 
	 * @param viewport
	 *            The viewport
	 */
	@JsonProperty("viewport")
	public void setViewport(Viewport viewport) {
		this.viewport = viewport;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(location).append(locationType)
				.append(viewport).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Geometry) == false) {
			return false;
		}
		Geometry rhs = ((Geometry) other);
		return new EqualsBuilder().append(location, rhs.location)
				.append(locationType, rhs.locationType)
				.append(viewport, rhs.viewport)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}