package it.unito.lmf.domain.dsl.values;

public enum Currency {
	EURO,
	DOLLAR
}
