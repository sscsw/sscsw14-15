package it.unito.lmf.domain.dsl.misc;

public class Pagination {
	public static final String HEADER_PAG_PAGE_SIZE="X-Page-Size";
	public static final String HEADER_PAG_PAGE_NUMBER="X-Page-Number";
	public static final String HEADER_PAG_TOTAL_COUNT="X-Total-Count";
	public static final String HEADER_PAG_TOTAL_PAGES="X-Total-Pages";
	public static final String HEADER_PAG_OUTPUT_ENCODING="X-Pagination-Output";
	
	public static final String QUERY_PAG_PAGE_SIZE="size";
	public static final String QUERY_PAG_PAGE_NUMBER="page";
}
