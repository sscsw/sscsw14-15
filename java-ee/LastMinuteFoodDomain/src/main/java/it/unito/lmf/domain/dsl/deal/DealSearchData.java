package it.unito.lmf.domain.dsl.deal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DealSearchData implements Serializable {

	private static final long serialVersionUID = 263380637726626751L;
	
	private List<Long> categories = new ArrayList<Long>();
		
	private String name;

	//@Min(value = 0, message = "Price cannot be negative.")
	private Double minPrice;
	
	//@Min(value = 0, message = "Price cannot be negative.")
	private Double maxPrice;

	/**
	 * In days.
	 */
	//@Size(min = 0, max = 100)
	private Integer maxAge;
	
	private boolean expired;

	//@Min(value = 0)
	private Double minQuantity;
	
	//@Size(min = 0, max = 5)
	private String minShopFeedback;

	private String discountReason;
	
	private String distanceFromLatLng;
	
	private Integer distance;
	
	public String getDistanceFromLatLng() {
		return distanceFromLatLng;
	}

	public void setDistanceFromLatLng(String distanceFromLatLng) {
		this.distanceFromLatLng = distanceFromLatLng;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public List<Long> getCategories() {
		return categories;
	}

	public void setCategories(List<Long> categories) {
		this.categories = categories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Double getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Double minQuantity) {
		this.minQuantity = minQuantity;
	}

	public String getMinShopFeedback() {
		return minShopFeedback;
	}

	public void setMinShopFeedback(String minShopFeedback) {
		this.minShopFeedback = minShopFeedback;
	}

	public String getDiscountReason() {
		return discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}
}
