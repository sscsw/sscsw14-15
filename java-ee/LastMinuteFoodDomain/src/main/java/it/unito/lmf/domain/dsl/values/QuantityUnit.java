package it.unito.lmf.domain.dsl.values;

public enum QuantityUnit {
	G, // GRAMS
	HG, // 100 GRAMS
	KG, // KGS
	PIECE // PIECES
}
