package it.unito.lmf.domain.dsl.deal;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Seller
 * <p>
 * A seller from Last Minute Food's catalog
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
//@formatter:off
@JsonPropertyOrder({
"id",
"username",
"firstName",
"lastName",
"email",
"ragioneSociale",
"codiceFiscale",
"phoneNumber"
})
//@formatter:on
public class SellerRepresentation {

	@JsonProperty("id")
	private Long id;
	@JsonProperty("username")
	private String username;
	@JsonProperty("firstName")
	private String firstName;
	@JsonProperty("lastName")
	private String lastName;
	@JsonProperty("email")
	private String email;
	@JsonProperty("ragioneSociale")
	private String ragioneSociale;
	@JsonProperty("codiceFiscale")
	private String codiceFiscale;
	@JsonProperty("phoneNumber")
	private String phoneNumber;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	public SellerRepresentation withId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The username
	 */
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @param username
	 *            The username
	 */
	@JsonProperty("username")
	public void setUsername(String username) {
		this.username = username;
	}

	public SellerRepresentation withUsername(String username) {
		this.username = username;
		return this;
	}

	/**
	 * 
	 * @return The firstName
	 */
	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName
	 *            The firstName
	 */
	@JsonProperty("firstName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public SellerRepresentation withFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	/**
	 * 
	 * @return The lastName
	 */
	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName
	 *            The lastName
	 */
	@JsonProperty("lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public SellerRepresentation withLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	/**
	 * 
	 * @return The email
	 */
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 *            The email
	 */
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	public SellerRepresentation withEmail(String email) {
		this.email = email;
		return this;
	}

	/**
	 * 
	 * @return The ragioneSociale
	 */
	@JsonProperty("ragioneSociale")
	public String getRagioneSociale() {
		return ragioneSociale;
	}

	/**
	 * 
	 * @param ragioneSociale
	 *            The ragioneSociale
	 */
	@JsonProperty("ragioneSociale")
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public SellerRepresentation withRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
		return this;
	}

	/**
	 * 
	 * @return The codiceFiscale
	 */
	@JsonProperty("codiceFiscale")
	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	/**
	 * 
	 * @param codiceFiscale
	 *            The codiceFiscale
	 */
	@JsonProperty("codiceFiscale")
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public SellerRepresentation withCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
		return this;
	}

	/**
	 * 
	 * @return The phoneNumber
	 */
	@JsonProperty("phoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * 
	 * @param phoneNumber
	 *            The phoneNumber
	 */
	@JsonProperty("phoneNumber")
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public SellerRepresentation withPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public SellerRepresentation withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(username)
				.append(firstName).append(lastName).append(email)
				.append(ragioneSociale).append(codiceFiscale)
				.append(phoneNumber).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof SellerRepresentation) == false) {
			return false;
		}
		SellerRepresentation rhs = ((SellerRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id)
				.append(username, rhs.username)
				.append(firstName, rhs.firstName)
				.append(lastName, rhs.lastName).append(email, rhs.email)
				.append(ragioneSociale, rhs.ragioneSociale)
				.append(codiceFiscale, rhs.codiceFiscale)
				.append(phoneNumber, rhs.phoneNumber)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}
