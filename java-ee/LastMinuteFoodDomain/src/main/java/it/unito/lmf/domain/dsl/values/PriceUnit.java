package it.unito.lmf.domain.dsl.values;

public enum PriceUnit {
	G, // GRAMS
	HG, // 100 GRAMS
	KG, // KGS
	PIECE // PIECES
}
