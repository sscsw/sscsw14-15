package it.unito.lmf.domain.dsl.notification;

import it.unito.lmf.domain.dsl.deal.SellerRepresentation;
import it.unito.lmf.domain.dsl.deal.ShopRepresentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "id", "type", "message", "done", "seller", "shop",
		"createdAt" })
public class AdministratorNotificationRepresentation implements Serializable {

	private static final long serialVersionUID = 5489770441889934238L;

	public enum NotificationType {
		SELLER_AUTHORIZATION_REQUEST, NEW_SHOP_REQUEST
	}

	@JsonProperty("id")
	private Long id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("message")
	private String message;
	@JsonProperty("done")
	private Boolean done;
	@JsonProperty("seller")
	private SellerRepresentation seller;
	@JsonProperty("shop")
	private ShopRepresentation shop;
	@JsonProperty("createdAt")
	private Long createdAt;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	public AdministratorNotificationRepresentation withId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The type
	 */
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *            The type
	 */
	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	public AdministratorNotificationRepresentation withType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * 
	 * @return The message
	 */
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 *            The message
	 */
	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	public AdministratorNotificationRepresentation withMessage(String message) {
		this.message = message;
		return this;
	}

	/**
	 * 
	 * @return The done
	 */
	@JsonProperty("done")
	public Boolean getDone() {
		return done;
	}

	/**
	 * 
	 * @param done
	 *            The done
	 */
	@JsonProperty("done")
	public void setDone(Boolean done) {
		this.done = done;
	}

	public AdministratorNotificationRepresentation withDone(Boolean done) {
		this.done = done;
		return this;
	}

	/**
	 * 
	 * @return The seller
	 */
	@JsonProperty("seller")
	public SellerRepresentation getSeller() {
		return seller;
	}

	/**
	 * 
	 * @param seller
	 *            The seller
	 */
	@JsonProperty("seller")
	public void setSeller(SellerRepresentation seller) {
		this.seller = seller;
	}

	public AdministratorNotificationRepresentation withSeller(SellerRepresentation seller) {
		this.seller = seller;
		return this;
	}

	/**
	 * 
	 * @return The shop
	 */
	@JsonProperty("shop")
	public ShopRepresentation getShop() {
		return shop;
	}

	/**
	 * 
	 * @param shop
	 *            The shop
	 */
	@JsonProperty("shop")
	public void setShop(ShopRepresentation shop) {
		this.shop = shop;
	}

	public AdministratorNotificationRepresentation withShop(ShopRepresentation shop) {
		this.shop = shop;
		return this;
	}

	/**
	 * 
	 * @return The createdAt
	 */
	@JsonProperty("createdAt")
	public Long getCreatedAt() {
		return createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 *            The createdAt
	 */
	@JsonProperty("createdAt")
	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public AdministratorNotificationRepresentation withCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public AdministratorNotificationRepresentation withAdditionalProperty(
			String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(type).append(message)
				.append(done).append(seller).append(shop).append(createdAt)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof AdministratorNotificationRepresentation) == false) {
			return false;
		}
		AdministratorNotificationRepresentation rhs = ((AdministratorNotificationRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id).append(type, rhs.type)
				.append(message, rhs.message).append(done, rhs.done)
				.append(seller, rhs.seller).append(shop, rhs.shop)
				.append(createdAt, rhs.createdAt)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}