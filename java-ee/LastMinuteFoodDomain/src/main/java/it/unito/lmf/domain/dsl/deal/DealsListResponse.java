package it.unito.lmf.domain.dsl.deal;

import java.util.ArrayList;
import java.util.List;

public class DealsListResponse {
	
	private List<DealRepresentation> deals = new ArrayList<DealRepresentation>();

	public List<DealRepresentation> getDeals() {
		return deals;
	}

	public void setDeals(List<DealRepresentation> deals) {
		this.deals = deals;
	}
	
}
