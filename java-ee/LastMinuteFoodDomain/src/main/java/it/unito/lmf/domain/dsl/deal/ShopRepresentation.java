package it.unito.lmf.domain.dsl.deal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * ShopRepresentation
 * <p>
 * A ShopRepresentation from Last Minute Food's catalog
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
//@formatter:off
@JsonPropertyOrder({
"id",
"name",
"description",
"image",
"categories",
"partitaIVA",
"phoneNumber",
"address",
"latitude",
"longitude",
"owner",
"createdAt"
})
//@formatter:on
public class ShopRepresentation {

	@JsonProperty("id")
	private Long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("image")
	private String image;
	@JsonProperty("categories")
	private List<String> categories = new ArrayList<String>();
	@JsonProperty("partitaIVA")
	private String partitaIVA;
	@JsonProperty("phoneNumber")
	private String phoneNumber;
	@JsonProperty("address")
	private String address;
	@JsonProperty("latitude")
	private Double latitude;
	@JsonProperty("longitude")
	private Double longitude;
	@JsonProperty("owner")
	private SellerRepresentation owner;
	@JsonProperty("createdAt")
	private Date createdAt;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	public ShopRepresentation withId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public ShopRepresentation withName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * 
	 * @return The description
	 */
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *            The description
	 */
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public ShopRepresentation withDescription(String description) {
		this.description = description;
		return this;
	}

	
	/**
	 * 
	 * @return The image
	 */
	@JsonProperty("image")
	public String getImage() {
		return image;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("image")
	public void setImage(String image) {
		this.image = image;
	}

	public ShopRepresentation withImage(String image) {
		this.image = image;
		return this;
	}
	/**
	 * 
	 * @return The categories
	 */
	@JsonProperty("categories")
	public List<String> getCategories() {
		return categories;
	}

	/**
	 * 
	 * @param categories
	 *            The categories
	 */
	@JsonProperty("categories")
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public ShopRepresentation withCategories(List<String> categories) {
		this.categories = categories;
		return this;
	}

	/**
	 * 
	 * @return The partitaIVA
	 */
	@JsonProperty("partitaIVA")
	public String getPartitaIVA() {
		return partitaIVA;
	}

	/**
	 * 
	 * @param partitaIVA
	 *            The partitaIVA
	 */
	@JsonProperty("partitaIVA")
	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}

	public ShopRepresentation withPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
		return this;
	}

	/**
	 * 
	 * @return The phoneNumber
	 */
	@JsonProperty("phoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * 
	 * @param phoneNumber
	 *            The phoneNumber
	 */
	@JsonProperty("phoneNumber")
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ShopRepresentation withPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
		return this;
	}

	/**
	 * 
	 * @return The address
	 */
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	/**
	 * 
	 * @param address
	 *            The address
	 */
	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	public ShopRepresentation withAddress(String address) {
		this.address = address;
		return this;
	}

	/**
	 * 
	 * @return The latitude
	 */
	@JsonProperty("latitude")
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * 
	 * @param latitude
	 *            The latitude
	 */
	@JsonProperty("latitude")
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public ShopRepresentation withLatitude(Double latitude) {
		this.latitude = latitude;
		return this;
	}

	/**
	 * 
	 * @return The longitude
	 */
	@JsonProperty("longitude")
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * 
	 * @param longitude
	 *            The longitude
	 */
	@JsonProperty("longitude")
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public ShopRepresentation withLongitude(Double longitude) {
		this.longitude = longitude;
		return this;
	}

	/**
	 * 
	 * @return The owner
	 */
	@JsonProperty("owner")
	public SellerRepresentation getOwner() {
		return owner;
	}

	/**
	 * 
	 * @param owner
	 *            The owner
	 */
	@JsonProperty("owner")
	public void setOwner(SellerRepresentation owner) {
		this.owner = owner;
	}

	public ShopRepresentation withOwner(SellerRepresentation owner) {
		this.owner = owner;
		return this;
	}

	/**
	 * 
	 * @return The createdAt
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 *            The createdAt
	 */
	@JsonProperty("createdAt")
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public ShopRepresentation withCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public ShopRepresentation withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name)
				.append(description).append(image).append(categories).append(partitaIVA)
				.append(phoneNumber).append(address).append(latitude)
				.append(longitude).append(owner).append(createdAt)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ShopRepresentation) == false) {
			return false;
		}
		ShopRepresentation rhs = ((ShopRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id).append(name, rhs.name)
				.append(description, rhs.description)
				.append(image, rhs.image)
				.append(categories, rhs.categories)
				.append(partitaIVA, rhs.partitaIVA)
				.append(phoneNumber, rhs.phoneNumber)
				.append(address, rhs.address).append(latitude, rhs.latitude)
				.append(longitude, rhs.longitude).append(owner, rhs.owner)
				.append(createdAt, rhs.createdAt)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}