package it.unito.lmf.domain.dsl.accounting;

import java.util.Date;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "id", "user", "expiresAt", "createdAt" })
public class TokenRepresentation {

	@JsonProperty("id")
	private String id;
	@JsonProperty("user")
	private Long user;
	@JsonProperty("expiresAt")
	private Date expiresAt;
	@JsonProperty("createdAt")
	private Date createdAt;

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public TokenRepresentation withId(String id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The user
	 */
	@JsonProperty("user")
	public Long getUser() {
		return user;
	}

	/**
	 * 
	 * @param user
	 *            The user
	 */
	@JsonProperty("user")
	public void setUser(Long user) {
		this.user = user;
	}

	public TokenRepresentation withUser(Long user) {
		this.user = user;
		return this;
	}

	/**
	 * 
	 * @return The expiresAt
	 */
	@JsonProperty("expiresAt")
	public Date getExpiresAt() {
		return expiresAt;
	}

	/**
	 * 
	 * @param expiresAt
	 *            The expiresAt
	 */
	@JsonProperty("expiresAt")
	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	public TokenRepresentation withExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
		return this;
	}

	/**
	 * 
	 * @return The createdAt
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 *            The createdAt
	 */
	@JsonProperty("createdAt")
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public TokenRepresentation withCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(user).append(expiresAt)
				.append(createdAt).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof TokenRepresentation) == false) {
			return false;
		}
		TokenRepresentation rhs = ((TokenRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id).append(user, rhs.user)
				.append(expiresAt, rhs.expiresAt)
				.append(createdAt, rhs.createdAt).isEquals();
	}

}