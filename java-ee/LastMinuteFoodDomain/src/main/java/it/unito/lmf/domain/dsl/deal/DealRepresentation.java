package it.unito.lmf.domain.dsl.deal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Deal
 * <p>
 * A deal from Last Minute Food's catalog
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
// @formatter:off
@JsonPropertyOrder({ "id", "name", "description", "price", "startingPrice",
		"currency", "priceUnit", "image", "quantity", "quantityUnit",
		"discountReason", "discountPercentage", "categories", "shop",
		"expireDate", "createdAt", "modifiedAt" })
// @formatter:on
public class DealRepresentation {

	public enum Fields {
		id, name, description, price, startingPrice,
		currency, priceUnit, image, quantity, quantityUnit,
		discountReason, discountPercentage, categories, shop,
		expireDate, createdAt, modifiedAt;
		
		public String getName() {
			return this.name();
		}
	}
	
	public enum CurrencySymbol {
		EURO("€"), DOLLAR("$");

		private String symbol;

		private CurrencySymbol(String symbol) {
			this.symbol = symbol;
		}

		public String getSymbol() {
			return symbol;
		}

	}

	@JsonProperty("id")
	private Long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("price")
	private Double price;
	@JsonProperty("startingPrice")
	private Double startingPrice;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("priceUnit")
	private String priceUnit;
	@JsonProperty("image")
	private String image;
	@JsonProperty("quantity")
	private Double quantity;
	@JsonProperty("quantityUnit")
	private String quantityUnit;
	@JsonProperty("discountReason")
	private String discountReason;
	@JsonProperty("discountPercentage")
	private Double discountPercentage;
	@JsonProperty("categories")
	private List<String> categories = new ArrayList<String>();
	@JsonProperty("shop")
	private ShopRepresentation shop;
	@JsonProperty("expireDate")
	private Date expireDate;
	@JsonProperty("createdAt")
	private Date createdAt;
	@JsonProperty("modifiedAt")
	private Date modifiedAt;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	public DealRepresentation withId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public DealRepresentation withName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * 
	 * @return The description
	 */
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *            The description
	 */
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public DealRepresentation withDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * 
	 * @return The price
	 */
	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 *            The price
	 */
	@JsonProperty("price")
	public void setPrice(Double price) {
		this.price = price;
	}

	public DealRepresentation withPrice(Double price) {
		this.price = price;
		return this;
	}

	/**
	 * 
	 * @return The startingPrice
	 */
	@JsonProperty("startingPrice")
	public Double getStartingPrice() {
		return startingPrice;
	}

	/**
	 * 
	 * @param startingPrice
	 *            The startingPrice
	 */
	@JsonProperty("startingPrice")
	public void setStartingPrice(Double startingPrice) {
		this.startingPrice = startingPrice;
	}

	public DealRepresentation withStartingPrice(Double startingPrice) {
		this.startingPrice = startingPrice;
		return this;
	}

	/**
	 * 
	 * @return The currency
	 */
	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * 
	 * @return The currency symbol
	 */
	public String getCurrencySymbol() {
		try {
			return CurrencySymbol.valueOf(currency).getSymbol();
		} catch (Exception e) {		
		}
		return currency;
	}

	/**
	 * 
	 * @param priceUnit
	 *            The priceUnit
	 */
	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public DealRepresentation withCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	/**
	 * 
	 * @return The priceUnit
	 */
	@JsonProperty("priceUnit")
	public String getPriceUnit() {
		return priceUnit;
	}

	/**
	 * 
	 * @param priceUnit
	 *            The priceUnit
	 */
	@JsonProperty("priceUnit")
	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public DealRepresentation withPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
		return this;
	}

	/**
	 * 
	 * @return The image
	 */
	@JsonProperty("image")
	public String getImage() {
		return image;
	}

	/**
	 * 
	 * @param image
	 *            The image
	 */
	@JsonProperty("image")
	public void setImage(String image) {
		this.image = image;
	}

	public DealRepresentation withImage(String image) {
		this.image = image;
		return this;
	}

	/**
	 * 
	 * @return The quantity
	 */
	@JsonProperty("quantity")
	public Double getQuantity() {
		return quantity;
	}

	/**
	 * 
	 * @param quantity
	 *            The quantity
	 */
	@JsonProperty("quantity")
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public DealRepresentation withQuantity(Double quantity) {
		this.quantity = quantity;
		return this;
	}

	/**
	 * 
	 * @return The quantityUnit
	 */
	@JsonProperty("quantityUnit")
	public String getQuantityUnit() {
		return quantityUnit;
	}

	/**
	 * 
	 * @param quantityUnit
	 *            The quantityUnit
	 */
	@JsonProperty("quantityUnit")
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public DealRepresentation withQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
		return this;
	}

	/**
	 * 
	 * @return The discountReason
	 */
	@JsonProperty("discountReason")
	public String getDiscountReason() {
		return discountReason;
	}

	/**
	 * 
	 * @param discountReason
	 *            The discountReason
	 */
	@JsonProperty("discountReason")
	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public DealRepresentation withDiscountReason(String discountReason) {
		this.discountReason = discountReason;
		return this;
	}

	/**
	 * 
	 * @return The discountPercentage
	 */
	@JsonProperty("discountPercentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	/**
	 * 
	 * @param discountPercentage
	 *            The discountPercentage
	 */
	@JsonProperty("discountPercentage")
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public DealRepresentation withDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
		return this;
	}

	/**
	 * 
	 * @return The categories
	 */
	@JsonProperty("categories")
	public List<String> getCategories() {
		return categories;
	}

	/**
	 * 
	 * @param categories
	 *            The categories
	 */
	@JsonProperty("categories")
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public DealRepresentation withCategories(List<String> categories) {
		this.categories = categories;
		return this;
	}

	/**
	 * 
	 * @return The shop
	 */
	@JsonProperty("shop")
	public ShopRepresentation getShop() {
		return shop;
	}

	/**
	 * 
	 * @param shop
	 *            The shop
	 */
	@JsonProperty("shop")
	public void setShop(ShopRepresentation shop) {
		this.shop = shop;
	}

	public DealRepresentation withShop(ShopRepresentation shop) {
		this.shop = shop;
		return this;
	}

	/**
	 * 
	 * @return The expireDate
	 */
	@JsonProperty("expireDate")
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * 
	 * @param expireDate
	 *            The expireDate
	 */
	@JsonProperty("expireDate")
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public DealRepresentation withExpireDate(Date expireDate) {
		this.expireDate = expireDate;
		return this;
	}

	/**
	 * 
	 * @return The createdAt
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 *            The createdAt
	 */
	@JsonProperty("createdAt")
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public DealRepresentation withCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	/**
	 * 
	 * @return The modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public Date getModifiedAt() {
		return modifiedAt;
	}

	/**
	 * 
	 * @param modifiedAt
	 *            The modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public DealRepresentation withModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public DealRepresentation withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name)
				.append(description).append(price).append(startingPrice)
				.append(currency).append(priceUnit).append(image)
				.append(quantity).append(quantityUnit).append(discountReason)
				.append(discountPercentage).append(categories).append(shop)
				.append(expireDate).append(createdAt).append(modifiedAt)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof DealRepresentation) == false) {
			return false;
		}
		DealRepresentation rhs = ((DealRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id).append(name, rhs.name)
				.append(description, rhs.description).append(price, rhs.price)
				.append(startingPrice, rhs.startingPrice)
				.append(currency, rhs.currency)
				.append(priceUnit, rhs.priceUnit).append(image, rhs.image)
				.append(quantity, rhs.quantity)
				.append(quantityUnit, rhs.quantityUnit)
				.append(discountReason, rhs.discountReason)
				.append(discountPercentage, rhs.discountPercentage)
				.append(categories, rhs.categories).append(shop, rhs.shop)
				.append(expireDate, rhs.expireDate)
				.append(createdAt, rhs.createdAt)
				.append(modifiedAt, rhs.modifiedAt)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}
