package it.unito.lmf.domain.dsl.misc;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "code", "name", "description", "verbose" })
public class ErrorRepresentation {

	@JsonProperty("code")
	private Long code;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("verbose")
	private String verbose;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public ErrorRepresentation() {
		
	}
	
	/**
	 * Instantiates an error with the given {@link ErrorCode}.
	 * @param ec
	 */
	public ErrorRepresentation(ErrorCode ec) {
		this.code = ec.getCode();
		this.description = ec.getDescription();
		this.name = ec.getName();
	}
	
	/**
	 * 
	 * @return The code
	 */
	@JsonProperty("code")
	public Long getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 *            The code
	 */
	@JsonProperty("code")
	public void setCode(Long code) {
		this.code = code;
	}

	public ErrorRepresentation withCode(Long code) {
		this.code = code;
		return this;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public ErrorRepresentation withName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * 
	 * @return The description
	 */
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *            The description
	 */
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public ErrorRepresentation withDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * 
	 * @return The verbose
	 */
	@JsonProperty("verbose")
	public String getVerbose() {
		return verbose;
	}

	/**
	 * 
	 * @param verbose
	 *            The verbose
	 */
	@JsonProperty("verbose")
	public void setVerbose(String verbose) {
		this.verbose = verbose;
	}

	public ErrorRepresentation withVerbose(String verbose) {
		this.verbose = verbose;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public ErrorRepresentation withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(code).append(name)
				.append(description).append(verbose)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ErrorRepresentation) == false) {
			return false;
		}
		ErrorRepresentation rhs = ((ErrorRepresentation) other);
		return new EqualsBuilder().append(code, rhs.code)
				.append(name, rhs.name).append(description, rhs.description)
				.append(verbose, rhs.verbose)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}