package it.unito.lmf.domain.dsl.notification;

import it.unito.lmf.domain.dsl.accounting.UserRepresentation;
import it.unito.lmf.domain.dsl.deal.DealRepresentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "id", "event", "visibility", "read", "deal", "user",
		"createdAt" })
public class DealNotificationRepresentation implements Serializable {

	private static final long serialVersionUID = -1865339105223285467L;

	public enum VisibilityType {
		WEB, MOBILE, EMAIL
	}

	@JsonProperty("id")
	private Long id;
	@JsonProperty("event")
	private String event;
	@JsonProperty("visibility")
	private String visibility;
	@JsonProperty("read")
	private Boolean read;
	@JsonProperty("deal")
	private DealRepresentation deal;
	@JsonProperty("user")
	private UserRepresentation user;
	@JsonProperty("createdAt")
	private Long createdAt;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	public DealNotificationRepresentation withId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * 
	 * @return The event
	 */
	@JsonProperty("event")
	public String getEvent() {
		return event;
	}

	/**
	 * 
	 * @param event
	 *            The event
	 */
	@JsonProperty("event")
	public void setEvent(String event) {
		this.event = event;
	}

	public DealNotificationRepresentation withEvent(String event) {
		this.event = event;
		return this;
	}

	/**
	 * 
	 * @return The visibility
	 */
	@JsonProperty("visibility")
	public String getVisibility() {
		return visibility;
	}

	/**
	 * 
	 * @param visibility
	 *            The visibility
	 */
	@JsonProperty("visibility")
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public DealNotificationRepresentation withVisibility(String visibility) {
		this.visibility = visibility;
		return this;
	}

	/**
	 * 
	 * @return The read
	 */
	@JsonProperty("read")
	public Boolean getRead() {
		return read;
	}

	/**
	 * 
	 * @param read
	 *            The read
	 */
	@JsonProperty("read")
	public void setRead(Boolean read) {
		this.read = read;
	}

	public DealNotificationRepresentation withRead(Boolean read) {
		this.read = read;
		return this;
	}

	/**
	 * 
	 * @return The deal
	 */
	@JsonProperty("deal")
	public DealRepresentation getDeal() {
		return deal;
	}

	/**
	 * 
	 * @param deal
	 *            The deal
	 */
	@JsonProperty("deal")
	public void setDeal(DealRepresentation deal) {
		this.deal = deal;
	}

	public DealNotificationRepresentation withDeal(DealRepresentation deal) {
		this.deal = deal;
		return this;
	}

	/**
	 * 
	 * @return The user
	 */
	@JsonProperty("user")
	public UserRepresentation getUser() {
		return user;
	}

	/**
	 * 
	 * @param user
	 *            The user
	 */
	@JsonProperty("user")
	public void setUser(UserRepresentation user) {
		this.user = user;
	}

	public DealNotificationRepresentation withUser(UserRepresentation user) {
		this.user = user;
		return this;
	}

	/**
	 * 
	 * @return The createdAt
	 */
	@JsonProperty("createdAt")
	public Long getCreatedAt() {
		return createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 *            The createdAt
	 */
	@JsonProperty("createdAt")
	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public DealNotificationRepresentation withCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public DealNotificationRepresentation withAdditionalProperty(String name,
			Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(event)
				.append(visibility).append(read).append(deal).append(user)
				.append(createdAt).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof DealNotificationRepresentation) == false) {
			return false;
		}
		DealNotificationRepresentation rhs = ((DealNotificationRepresentation) other);
		return new EqualsBuilder().append(id, rhs.id).append(event, rhs.event)
				.append(visibility, rhs.visibility).append(read, rhs.read)
				.append(deal, rhs.deal).append(user, rhs.user)
				.append(createdAt, rhs.createdAt)
				.append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}
