package it.unito.lmf.gae.repo;

import it.unito.lmf.gae.domain.Picture;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class PictureDAO {
	/**
	 * Queries the datastore for the picture object with the passed-in key. If
	 * found, returns the picture object; otherwise, returns null.
	 *
	 * @param key
	 *            picture key to look up
	 */
	@SuppressWarnings("unchecked")
	public static Picture getPicture(String key) {
		PersistenceManager pm = PMF.get().getPersistenceManager();

		// Search for any picture object with the passed-in key; limit the
		// number of results returned to 1 since there should be at most one
		// picture with a given key
		Query query = pm.newQuery(Picture.class, "key == keyParam");
		query.declareParameters("String keyParam");
		query.setRange(0, 1);

		try {
			List<Picture> results = (List<Picture>) query.execute(key);
			if (results.iterator().hasNext()) {
				// If the results list is non-empty, return the first (and only)
				// result
				return results.get(0);
			}
		} finally {
			query.closeAll();
			pm.close();
		}

		return null;
	}

	/**
	 * Queries the datastore for the picture object with the passed-in key. If
	 * found, returns true; otherwise, returns false.
	 *
	 * @param key
	 *            picture key to look up
	 */
	public static boolean pictureExists(String key) {
		return getPicture(key) != null;
	}

	/**
	 * Queries the datastore deleting the picture object with the passed-in key.
	 * If found, returns true; otherwise, returns false.
	 *
	 * @param key
	 *            picture key to delete
	 */
	public static boolean deletePicture(String key) {
		PersistenceManager pm = PMF.get().getPersistenceManager();

		// Search for any picture object with the passed-in key; limit the
		// number of results returned to 1 since there should be at most one
		// picture with a given key
		Query query = pm.newQuery(Picture.class, "key == keyParam");
		query.declareParameters("String keyParam");
		try {
			return query.deletePersistentAll(key) > 0;
		} finally {
			query.closeAll();
			pm.close();
		}
	}
}
