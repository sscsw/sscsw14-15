package it.unito.lmf.gae.web;

import it.unito.lmf.gae.domain.Picture;
import it.unito.lmf.gae.repo.PMF;
import it.unito.lmf.gae.repo.PictureDAO;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

/**
 * Picture repository service.
 * 
 * @author Biava Lorenzo
 *
 */
public class UploadPictureServlet extends HttpServlet {

	private static final long serialVersionUID = 6155366409430674226L;

	private static final String TOKEN = "f3065f20-8944-11e4-b4a9-0800200c9a66";

	/**
	 * PUT requests persist this uploaded image (with multipart form) along with
	 * the key specified by the key query string parameter as a new Picture
	 * object in App Engine's datastore. MUST be authenticated.
	 */
	@Override
	public void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// Check the authentication token to avoid uncontrolled upload
		// operations
		String token = req.getParameter("token");
		if (!checkAuthorization(resp, token))
			return;

		try {
			// if(!ServletFileUpload.isMultipartContent(req))
			// {
			// resp.sendError(HttpStatus.SC_BAD_REQUEST,
			// "body must be multipart/form-data");
			// return;
			// }

			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iterator = upload.getItemIterator(req);
			FileItemStream imageItem = null;

			while (iterator.hasNext()) {
				FileItemStream item = iterator.next();

				if (!item.isFormField()
						&& item.getFieldName().equals("picture")) {
					imageItem = item;
					break;
				}
			}

			if (imageItem == null) {
				resp.sendError(400, "picture parameter cannot be null");
				return;
			}

			InputStream stream = imageItem.openStream();

			String key = req.getParameter("key");
			if (key == null) {
				// resp.setStatus(HttpStatus.SC_BAD_REQUEST);
				resp.sendError(400, "key parameter cannot be null");
				return;
			}

			if (PictureDAO.pictureExists(key)) {
				// resp.setStatus(HttpStatus.SC_CONFLICT);
				resp.sendError(409, "picture with same key already exists");
				return;
			}

			// Create a new Picture instance
			Picture picture = new Picture();
			picture.setKey(key);
			picture.setImageType(imageItem.getContentType());

			// Set the picture's promotional image by passing in the bytes
			// pulled
			// from the image fetched via the URL Fetch service
			picture.setImage(IOUtils.toByteArray(stream));

			PersistenceManager pm = PMF.get().getPersistenceManager();
			try {
				// Store the image in App Engine's datastore
				pm.makePersistent(picture);
			} finally {
				pm.close();
			}

		} catch (FileUploadException e) {
			throw new IOException(e);
		}
	}

	/**
	 * GET requests return the picture associated with the picture key specified
	 * by the key query string parameter.
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String key = req.getParameter("key");

		if (key == null)
			// Try to extract as a path param
			try {
				key = req.getPathInfo().substring(1);
			} catch (Exception e) {
			}
		if (key == null) {
			// resp.setStatus(HttpStatus.SC_BAD_REQUEST);
			resp.sendError(400, "key parameter cannot be null");
			return;
		}

		Picture picture = PictureDAO.getPicture(key);

		if (picture != null && picture.getImageType() != null
				&& picture.getImage() != null) {

			// Set the appropriate Content-Type header and write the raw bytes
			// to the response's output stream
			resp.setContentType(picture.getImageType());
			resp.getOutputStream().write(picture.getImage());

			int cacheAge = 90000;
			resp.addHeader("Cache-Control", "public, max-age="
					+ (cacheAge));
			resp.addHeader("ETag", key);
			long expiry = new Date().getTime() + cacheAge * 1000;

			resp.setDateHeader("Expires", expiry);
		} else {
			// If no image is found
			resp.setStatus(404);
		}
	}

	/**
	 * DELETE requests removes the picture associated with the picture key
	 * specified by the key query string parameter.
	 */
	@Override
	public void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		// Check the authentication token to avoid uncontrolled delete
		// operations
		String token = req.getParameter("token");
		if (!checkAuthorization(resp, token))
			return;

		String key = req.getParameter("key");
		if (key == null) {
			// resp.setStatus(HttpStatus.SC_BAD_REQUEST);
			resp.sendError(400, "key parameter cannot be null");
			return;
		}

		if (PictureDAO.deletePicture(key))
			resp.setStatus(204);
		else
			resp.sendError(410, "the picture does not exist");
	}

	private boolean checkAuthorization(HttpServletResponse response,
			String token) {
		if (token == null) {
			response.setStatus(401);
			return false;
		}
		if (!TOKEN.equals(token)) {
			response.setStatus(403);
			return false;
		}

		return true;
	}
}