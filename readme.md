Last Minute Food
======================
**A project for Component-based Software Development and Web Services [course](http://wwwold.educ.di.unito.it/VisualizzaCorsi/corso.php?cod=MFN0941&codA=&year=2015&orienta=F)**

The goal of the course was to realize a web application using a component-based approach, also featuring web services, while trying to follow an Agile methodology.

The idea we chose for the web app was of reducing food waste by letting merchants sell their perishable goods directly to the final customers. So we created a marketplace where they could advertise theirs deals.
We adopted Extreme Programming as the Agile methodology and several tools to speed up the development process of the web application, the web site, the basic Android app and a trivial cloud-based (GCE) utility.
The main tools we used are the following: J2EE, Hibernate, Spring Framework, Thymeleaf, Bootstrap, Google APIs (Maps, Google +), Facebook APIs, Apache Maven, MySQL, Glassfish, Google App Engine, Agilefant.


In the [docs](docs) folder you can find some screenshots and more informations.

## Screenshots

##### Home page
![Home page](/docs/screenshots/home-page.png)

##### Deals
![Deals](/docs/screenshots/deals.png)

##### New deal
![New deal](/docs/screenshots/new-deal.png)

##### Shops
![Shops](/docs/screenshots/shops.png)

##### Shop details
![Shop details](/docs/screenshots/shop.png)

Copyright and license
----------------------
*Copyright (c) 2015 Lorenzo Biava, Federico Angaramo*

Licensed under the Apache License, Version 2.0 (the "License");<br/>
you may not use this file except in compliance with the License.<br/>
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.